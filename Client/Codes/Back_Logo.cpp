#include "stdafx.h"
#include "..\Headers\Back_Logo.h"
#include "Management.h"

_USING(Client)

CBack_Logo::CBack_Logo(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CGameObject(pGraphic_Device)
{
}

CBack_Logo::CBack_Logo(const CBack_Logo & rhs)
	: CGameObject(rhs),
	m_byDrawID(rhs.m_byDrawID),
	m_byKey(rhs.m_byKey)
{

}


// 원형객체 생성될 때 호출.
HRESULT CBack_Logo::Ready_Prototype()
{
	// 파일 입출력 등, 초기화에 시간이 걸리는 ㄱ데이터들 셋.



	return NOERROR;
}

// 복사본객체 생성될 때 호출.
HRESULT CBack_Logo::Ready_GameObject()
{
	// 실제 사용되기위한 객체들 만의 추가적인 데이터를 셋.

	// 현재 객체에게 필요한 컴포넌트들을 복제해서 온다.


	if (FAILED(Ready_Component()))
		return E_FAIL;

	switch (m_byDrawID)
	{
	case 1:
		m_pTransformCom->Scaling(2.f, 2.f, 2.f);
		m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &_vec3(0.0f, 0.f, 0.f));
		break;
	case 2:
		m_pTransformCom->Scaling(0.6f, 1.5f, 1.f);
		m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &_vec3(-0.4f, 0.f, 0.f));
		break;
	case 3:
		m_pTransformCom->Scaling(1.f, 0.5f, 1.f);
		m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &_vec3(0.4f, 0.5f, 0.f));
		break;
	case 4:
		m_pTransformCom->Scaling(0.7f, 0.25f, 1.f);
		m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &_vec3(0.4f, 0.52f, 0.f));
		break;
	case 5:
		m_pTransformCom->Scaling(1.f, 1.0f, 1.f);
		m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &_vec3(0.25f, -0.15f, 0.f));
		break;
	case 6:
		m_pTransformCom->Scaling(1.f, 0.18f, 1.f);
		m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &_vec3(0.25f, 0.2f, 0.f));
		break;
	case 7:
		m_pTransformCom->Scaling(1.f, 0.18f, 1.f);
		m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &_vec3(0.25f, 0.0f, 0.f));
		break;
	case 8:
		m_pTransformCom->Scaling(1.f, 0.18f, 1.f);
		m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &_vec3(0.25f, -0.2f, 0.f));
		break;

	default:
		break;
	}



	return NOERROR;
}

_int CBack_Logo::Update_GameObject(const _float & fTimeDelta)
{

	return _int();
}

_int CBack_Logo::LastUpdate_GameObject(const _float & fTimeDelta)
{
	if (nullptr == m_pRendererCom)
		return -1;

	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_NONEALPHA, this)))
		return -1;


	m_fTime += fTimeDelta;

	if (m_fTime > 0.1f)
	{

		if (GetAsyncKeyState(VK_UP) & 0x8000)
		{
			if (m_byKey == 0)
				m_byKey = 4;

			m_byKey--;
		}
		if (GetAsyncKeyState(VK_DOWN) & 0x8000)
		{
			if (m_byKey >= 3)
				m_byKey = -1;

			m_byKey++;
		}

		m_fTime = 0.f;

	}

	return _int();
}

void CBack_Logo::Render_GameObject()
{
	if (nullptr == m_pBufferCom)
		return;

	m_pTransformCom->SetUp_OnGraphicDev();

	
	switch (m_byDrawID)
	{
	case 1:
		if (FAILED(m_pTextureCom->SetUp_OnGraphicDev(0)))
			return;
		break;
	case 2:
		if (FAILED(m_pTextureCom->SetUp_OnGraphicDev(2)))
			return;
		break;
	case 3:
		if (FAILED(m_pTextureCom->SetUp_OnGraphicDev(5)))
			return;
		break;
	case 4:
		if (FAILED(m_pTextureCom->SetUp_OnGraphicDev(4)))
			return;
		break;
	case 5:
		if (FAILED(m_pTextureCom->SetUp_OnGraphicDev(0)))
			return;
		break;
	case 6:
		if (1 == m_byKey)
			m_pTextureCom->SetUp_OnGraphicDev(0);
		else
			m_pTextureCom->SetUp_OnGraphicDev(1);
		break;
	case 7:
		if (2 == m_byKey)
			m_pTextureCom->SetUp_OnGraphicDev(2);
		else
			m_pTextureCom->SetUp_OnGraphicDev(3);
		break;
	case 8:
		if (3 == m_byKey)
			m_pTextureCom->SetUp_OnGraphicDev(4);
		else
			m_pTextureCom->SetUp_OnGraphicDev(5);
		break;
	default:
		if (FAILED(m_pTextureCom->SetUp_OnGraphicDev(2)))
			return;
		break;
	}

	m_pGraphic_Device->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	m_pBufferCom->Render_VIBuffer();
	m_pGraphic_Device->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
}

HRESULT CBack_Logo::Ready_Component()
{
	CManagement*		pManagement = CManagement::GetInstance();
	if (nullptr == pManagement)
		return E_FAIL;
	pManagement->AddRef();

	// For.Com_Transform
	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;

	// For.Com_Renderer
	m_pRendererCom = (CRenderer*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Renderer");
	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
		return E_FAIL;

	// For.Com_Buffer
	m_pBufferCom = (CBuffer_RcTex*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Buffer_RcTex");
	if (FAILED(Add_Component(L"Com_Buffer", m_pBufferCom)))
		return E_FAIL;

	if (1 == m_byDrawID)
	{
		m_pTextureCom = (CTexture*)pManagement->Clone_Component(SCENE_LOGO, L"Component_Texture_Logo_Background");
		if (FAILED(Add_Component(L"Com_Texture", m_pTextureCom)))
			return E_FAIL;
	}
	else if (6 <= m_byDrawID)
	{
		m_pTextureCom = (CTexture*)pManagement->Clone_Component(SCENE_LOGO, L"Component_Texture_Logo_Menu");
		if (FAILED(Add_Component(L"Com_Texture", m_pTextureCom)))
			return E_FAIL;
	}
	else
	{
		m_pTextureCom = (CTexture*)pManagement->Clone_Component(SCENE_LOGO, L"Component_Texture_Logo_Part");
		if (FAILED(Add_Component(L"Com_Texture", m_pTextureCom)))
			return E_FAIL;
	}




	//switch (m_byDrawID)
	//{
	//case 1:

	//	break;
	//case 2:
	//	m_pTextureCom = (CTexture*)pManagement->Clone_Component(SCENE_LOGO, L"Component_Texture_Logo_Part");
	//	if (FAILED(Add_Component(L"Com_Texture", m_pTextureCom)))
	//		return E_FAIL;
	//	break;
	//case 3:
	//	m_pTextureCom = (CTexture*)pManagement->Clone_Component(SCENE_LOGO, L"Component_Texture_Logo_Part");
	//	if (FAILED(Add_Component(L"Com_Texture", m_pTextureCom)))
	//		return E_FAIL;
	//	break;
	//case 4:

	//default:
	//	break;
	//}


	Safe_Release(pManagement);


	return NOERROR;
}

// 원형객체를 생성하기위해 만들어진 함수.
CBack_Logo * CBack_Logo::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CBack_Logo*	pInstance = new CBack_Logo(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		MessageBox(0, L"CBack_Logo Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

// 원형객체가 호출해주는 함수(원형주소->Clone_GameObject())
// 복사본객첼르 생성하기위해.
CGameObject * CBack_Logo::Clone_GameObject()
{
	this->AddClone();

	CBack_Logo*	pInstance = new CBack_Logo(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		MessageBox(0, L"CBack_Logo Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

void CBack_Logo::Free()
{
	Safe_Release(m_pTextureCom);
	Safe_Release(m_pBufferCom);
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pTransformCom);

	CGameObject::Free();
}

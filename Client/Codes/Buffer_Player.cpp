#include "stdafx.h"
#include "..\Headers\Buffer_Player.h"

_USING(Client)


CBuffer_Player::CBuffer_Player(LPDIRECT3DDEVICE9 pGraphic_Device)
	:CVIBuffer(pGraphic_Device)
{
}

CBuffer_Player::CBuffer_Player(const CBuffer_Player & rhs)
	: CVIBuffer(rhs)
{
	
}

HRESULT CBuffer_Player::Ready_VIBuffer()
{

	m_iNumVertices = 9;
	m_iStride = sizeof(VTXNMTEX);
	m_dwFVF = D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1;


	m_iNumPolygons = 8;
	m_iPolygonSize = sizeof(POLYGON16);
	m_eFormat = D3DFMT_INDEX16;

	
	//m_pIndex = new POLYGON16[m_iNumPolygons];

	if (FAILED(CVIBuffer::Ready_VIBuffer()))
		return E_FAIL;


	VTXNMTEX*		pVertices = nullptr;
	m_pVB->Lock(0, 0, (void**)&pVertices, 0);
	for (size_t i = 0; i < m_iNumVertices; ++i)
	{
		pVertices[i].vNormal = _vec3(0.5f, 0.5f, -0.5f);
	}
	m_pVB->Unlock();

	Ready_Index();

	return NOERROR;
}

void CBuffer_Player::Render_VIBuffer()
{
	
	UpdateVertex();
	

	
	m_pGraphic_Device->SetStreamSource(0, m_pVB, 0, m_iStride);
	m_pGraphic_Device->SetFVF(m_dwFVF);
	m_pGraphic_Device->SetIndices(m_pIB);
	m_pGraphic_Device->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, m_iNumVertices, 0, m_iNumPolygons);
}

CBuffer_Player * CBuffer_Player::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CBuffer_Player* pInstance = new CBuffer_Player(pGraphic_Device);
	if (FAILED(pInstance->Ready_VIBuffer()))
	{
		_MSG_BOX("CBuffer_Player Created Failed")
			Safe_Release(pInstance);
	}
	return pInstance;
}

CComponent * CBuffer_Player::Clone_Component()
{
	return new CBuffer_Player(*this);
}

void CBuffer_Player::Free()
{
	CVIBuffer::Free();
}

void CBuffer_Player::SetUp_BufferPlayer(PLANEINFO* _pPlane,  Direction& _touchDir, Direction& _climbDir, _float& _offX, _float& _offY)
{
	
	m_pPlane = _pPlane;
	touchDir = _touchDir;
	climbDir = _climbDir;
	offX = _offX;
	offY = _offY;

	
}

void CBuffer_Player::UpdateVertex()
{
	UpdateVertex1();
	
	if (touchDir != DIR_END)
	{
		calNext();
		UpdateVertex2();
	}

}

void CBuffer_Player::UpdateVertex1()
{

	VTXNMTEX*		pVertices = nullptr;

	m_pVB->Lock(0, 0, (void**)&pVertices, 0);


	pVertices[0].vPosition = _vec3(-0.5f,-0.5f,0.f);
	pVertices[0].vTexUV = _vec2(0.f, 1.f);

	pVertices[1].vPosition = _vec3(offX-0.5f, -0.5f, 0.f);//
	pVertices[1].vTexUV = _vec2(offX, 1.f);

	pVertices[2].vPosition = _vec3(0.5f, -0.5f, 0.f);//
	pVertices[2].vTexUV = _vec2(1.f, 1.f);

	pVertices[3].vPosition = _vec3( - 0.5f, 0.5f-offY, 0.f);//
	pVertices[3].vTexUV = _vec2(0.f, offY);

	pVertices[4].vPosition = _vec3(offX - 0.5f, 0.5f-offY, 0.f);//
	pVertices[4].vTexUV = _vec2(offX, offY);

	pVertices[5].vPosition = _vec3(0.5f, 0.5f - offY, 0.f);//
	pVertices[5].vTexUV = _vec2(1.f, offY);

	pVertices[6].vPosition = _vec3(-0.5f, 0.5f, 0.f);//
	pVertices[6].vTexUV = _vec2(0.f, 0.f);

	pVertices[7].vPosition = _vec3(offX - 0.5f, 0.5f, 0.f);//
	pVertices[7].vTexUV = _vec2(offX, 0.f);

	pVertices[8].vPosition = _vec3(0.5f, 0.5f, 0.f);//
	pVertices[8].vTexUV = _vec2(1.f, 0.f);


		

	m_pVB->Unlock();
}

void CBuffer_Player::UpdateVertex2()
{
	_int dir1 = 0;
	if (m_pPlane->iPlaneNum % 5 == 4)
		dir1 = (touchDir - climbDir + 4) % 4;
	else
		dir1 = touchDir;



	VTXNMTEX*		pVertices = nullptr;
	m_pVB->Lock(0, 0, (void**)&pVertices, 0);
	switch (dir1)
	{
	case DOWN:
		pVertices[0].vPosition = _vec3(-0.5f, 0.5f - offY, dir[touchDir]*(1.f - offY));
		pVertices[1].vPosition = _vec3(0.f, 0.5f - offY, dir[touchDir] * (1.f - offY));
		pVertices[2].vPosition = _vec3(0.5f, 0.5f - offY, dir[touchDir] * (1.f - offY));
		break;
	case LEFT:
		pVertices[0].vPosition = _vec3(offX - 0.5f, -0.5f, dir[touchDir] * (offX));
		pVertices[3].vPosition = _vec3(offX - 0.5f, 0.f, dir[touchDir] * (offX));
		pVertices[6].vPosition = _vec3(offX - 0.5f, 0.5f, dir[touchDir] * (offX));
		break;
	case UP:
		pVertices[6].vPosition = _vec3(-0.5f, 0.5f - offY, dir[touchDir] * (offY));
		pVertices[7].vPosition = _vec3(0.f, 0.5f - offY, dir[touchDir] * (offY));
		pVertices[8].vPosition = _vec3(0.5f, 0.5f - offY, dir[touchDir] * (offY));
		break;
	case RIGHT:
		pVertices[2].vPosition = _vec3(offX - 0.5f, -0.5f, dir[touchDir] * (1.f - offX));
		pVertices[5].vPosition = _vec3(offX - 0.5f, 0.f, dir[touchDir] * (1.f - offX));
		pVertices[8].vPosition = _vec3(offX - 0.5f, 0.5f, dir[touchDir] * (1.f - offX));
		break;
	}



	m_pVB->Unlock();

	
	
}

void CBuffer_Player::Ready_Index()
{

	POLYGON16* pIndices = nullptr;
	m_pIB->Lock(0, 0, (void**)&pIndices, 0);
	

	pIndices[0]._0 = 3;
	pIndices[0]._1 = 4;
	pIndices[0]._2 = 1;

	pIndices[1]._0 = 3;
	pIndices[1]._1 = 1;
	pIndices[1]._2 = 0;

	pIndices[2]._0 = 4;
	pIndices[2]._1 = 5;
	pIndices[2]._2 = 2;

	pIndices[3]._0 = 4;
	pIndices[3]._1 = 2;
	pIndices[3]._2 = 1;

	pIndices[4]._0 = 6;
	pIndices[4]._1 = 7;
	pIndices[4]._2 = 4;

	pIndices[5]._0 =  6;
	pIndices[5]._1 =  4;
	pIndices[5]._2 =  3;

	pIndices[6]._0 =  7;
	pIndices[6]._1 =  8;
	pIndices[6]._2 =  5;

	pIndices[7]._0 =  7;
	pIndices[7]._1 =  5;
	pIndices[7]._2 =  4;


	m_pIB->Unlock();
}

void CBuffer_Player::calNext()
{
	_int curDir = m_pPlane->iPlaneNum % 5;
	_int nextDir = m_pPlane->arrMoveablePlane[touchDir]%5;


	//���ٿ����
	if (curDir == 4)//����
	{
		if (touchDir == UP)
		{
			if (nextDir == 0)
				dir[touchDir] = -1;
			else if(nextDir==2)
				dir[touchDir] = 1;
		}
		else if (touchDir == DOWN)
		{
			if (nextDir == 0)
				dir[touchDir] = 1;
			else if (nextDir == 2)
				dir[touchDir] = -1;
		}

	}
	else//�׿�
	{
		dir[UP] = 1;
		dir[DOWN] = -1;
	}




	//�¿�
	if (touchDir == LEFT)
	{
		switch (curDir)
		{
		case 0:
			if (nextDir == 1)
				dir[touchDir] = 1;
			else if(nextDir==3)
				dir[touchDir] = -1;
			break;
		case 1:
			if (nextDir == 2)
				dir[touchDir] = 1;
			else if (nextDir == 0)
				dir[touchDir] = -1;
			break;
		case 2:
			if (nextDir == 3)
				dir[touchDir] = 1;
			else if (nextDir == 1)
				dir[touchDir] = -1;
			break;
		case 3:
			if (nextDir == 0)
				dir[touchDir] = 1;
			else if (nextDir == 2)
				dir[touchDir] = -1;
			break;
		case 4:
			if (nextDir == 1)
				dir[touchDir] = 1;
			else if (nextDir == 3)
				dir[touchDir] = -1;
			break;
		}
	}
	else if (touchDir == RIGHT)
	{
		switch (curDir)
		{
		case 0:
			if (nextDir == 1)
				dir[touchDir] = -1;
			else if (nextDir == 3)
				dir[touchDir] = 1;
			break;
		case 1:
			if (nextDir == 2)
				dir[touchDir] = -1;
			else if (nextDir == 0)
				dir[touchDir] = 1;
			break;
		case 2:
			if (nextDir == 3)
				dir[touchDir] = -1;
			else if (nextDir == 1)
				dir[touchDir] = 1;
			break;
		case 3:
			if (nextDir == 0)
				dir[touchDir] = -1;
			else if (nextDir == 2)
				dir[touchDir] = 1;
			break;
		case 4:
			if (nextDir == 1)
				dir[touchDir] = -1;
			else if (nextDir == 3)
				dir[touchDir] = 1;
			break;
		}
	}


}

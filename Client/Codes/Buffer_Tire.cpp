#include "stdafx.h"
#include "..\Headers\Buffer_Tire.h"

_USING(Client)

CBuffer_Tire::CBuffer_Tire(LPDIRECT3DDEVICE9 pGraphic_Device)
	:CVIBuffer(pGraphic_Device)
{
}

CBuffer_Tire::CBuffer_Tire(const CBuffer_Tire & rhs)
	: CVIBuffer(rhs)
{
}

HRESULT CBuffer_Tire::Ready_VIBuffer()
{
	m_iNumVertices = 140;//36���� 
	m_iStride = sizeof(VTXCOL);
	m_dwFVF = D3DFVF_XYZ | D3DFVF_DIFFUSE;

	m_iNumPolygons = 280;
	m_iPolygonSize = sizeof(POLYGON16);
	m_eFormat = D3DFMT_INDEX16;

	if (FAILED(CVIBuffer::Ready_VIBuffer()))
		return E_FAIL;

	VTXCOL*		pVertices = nullptr;
	m_pVB->Lock(0, 0, (void**)&pVertices, 0);

	for(size_t i=0;i<m_iNumVertices;++i)
		pVertices[i].dwColor = D3DXCOLOR(0.3f, 0.3f, 0.3f, 1.f);

	m_pVB->Unlock();

	Ready_Index();
	
	

	return NOERROR;
}


CBuffer_Tire * CBuffer_Tire::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CBuffer_Tire * pInstance = new CBuffer_Tire(pGraphic_Device);
	if (FAILED(pInstance->Ready_VIBuffer()))
	{
		_MSG_BOX("CBuffer_Tire Created Failed")
			Safe_Release(pInstance);
	}
	return pInstance;
}

CComponent * CBuffer_Tire::Clone_Component()
{
	return new CBuffer_Tire(*this);
}

void CBuffer_Tire::Free()
{
	CVIBuffer::Free();
}


void CBuffer_Tire::SetUp_BufferPlayer(_float & _fX, _float & _fY)
{
	fX = _fX;
	fY = _fY;
}



void CBuffer_Tire::UpdateVertex()
{
	VTXCOL*		pVertices = nullptr;
	m_pVB->Lock(0, 0, (void**)&pVertices, 0);

	_int index = 0;
	_vec3 vVtx;
	
	_matrix	matRot;
	D3DXMatrixRotationY(&matRot, D3DXToRadian(-36.f));

	vVtx = _vec3(0.4f*fX, 1.f*fY - 0.5f, 0.f);///////0
	for (size_t i = 0; i < 10; ++i)
	{
		pVertices[index].dwColor = D3DXCOLOR(1.f, 1.f, 0.3f, 1.f);
		pVertices[index++].vPosition = vVtx;
		D3DXVec3TransformNormal(&vVtx, &vVtx, &matRot);
	}

	vVtx = _vec3(0.5f*fX, 0.9f*fY - 0.5f, 0.f);///////1
	for (size_t i = 0; i < 10; ++i)
	{
 		pVertices[index++].vPosition = vVtx;
		D3DXVec3TransformNormal(&vVtx, &vVtx, &matRot);
	}

	vVtx = _vec3(0.5f*fX, 0.76f*fY - 0.5f, 0.f);///////2
	for (size_t i = 0; i < 10; ++i)
	{
		pVertices[index++].vPosition = vVtx;
		D3DXVec3TransformNormal(&vVtx, &vVtx, &matRot);
	}

	vVtx = _vec3(0.4f*fX, 0.56f*fY - 0.5f, 0.f);///////3
	for (size_t i = 0; i < 10; ++i)
	{
		pVertices[index].dwColor = D3DXCOLOR(1.f, 1.f, 0.3f, 1.f);
		
		pVertices[index++].vPosition = vVtx;
		D3DXVec3TransformNormal(&vVtx, &vVtx, &matRot);
	}

	vVtx = _vec3(0.5f*fX, 0.56f*fY - 0.5f, 0.f);///////4
	for (size_t i = 0; i < 10; ++i)
	{
		
		pVertices[index++].vPosition = vVtx;
		D3DXVec3TransformNormal(&vVtx, &vVtx, &matRot);
	}

	vVtx = _vec3(0.5f*fX, 0.42f*fY - 0.5f, 0.f);///////5
	for (size_t i = 0; i < 10; ++i)
	{
		pVertices[index++].vPosition = vVtx;
		D3DXVec3TransformNormal(&vVtx, &vVtx, &matRot);
	}

	vVtx = _vec3(0.4f*fX, 0.32f*fY - 0.5f, 0.f);///////6
	for (size_t i = 0; i < 10; ++i)
	{
		pVertices[index].dwColor = D3DXCOLOR(1.f, 1.f, 0.3f, 1.f);
		pVertices[index++].vPosition = vVtx;
		D3DXVec3TransformNormal(&vVtx, &vVtx, &matRot);
	}

	vVtx = _vec3(0.5f*fX, 0.22f*fY - 0.5f, 0.f);///////7
	for (size_t i = 0; i < 10; ++i)
	{
		pVertices[index++].vPosition = vVtx;
		D3DXVec3TransformNormal(&vVtx, &vVtx, &matRot);
	}

	vVtx = _vec3(0.5f*fX, 0.09f*fY - 0.5f, 0.f);///////8
	for (size_t i = 0; i < 10; ++i)
	{
		pVertices[index++].vPosition = vVtx;
		D3DXVec3TransformNormal(&vVtx, &vVtx, &matRot);
	}

	vVtx = _vec3(0.4f*fX, 0.f*fY - 0.5f, 0.f);///////9
	for (size_t i = 0; i < 10; ++i)
	{
		pVertices[index].dwColor = D3DXCOLOR(1.f, 1.f, 0.3f, 1.f);
		
		pVertices[index++].vPosition = vVtx;
		D3DXVec3TransformNormal(&vVtx, &vVtx, &matRot);
	}

	vVtx = _vec3(0.3f*fX, 0.f*fY - 0.5f, 0.f);///////10
	for (size_t i = 0; i < 10; ++i)
	{
		
		pVertices[index++].vPosition = vVtx;
		D3DXVec3TransformNormal(&vVtx, &vVtx, &matRot);
	}

	vVtx = _vec3(0.25f*fX, 0.09f*fY - 0.5f, 0.f);///////11
	for (size_t i = 0; i < 10; ++i)
	{
		
		pVertices[index++].vPosition = vVtx;
		D3DXVec3TransformNormal(&vVtx, &vVtx, &matRot);
	}

	vVtx = _vec3(0.25f*fX, 0.9f*fY-0.5f, 0.f);///////12
	for (size_t i = 0; i < 10; ++i)
	{
		
		pVertices[index++].vPosition = vVtx;
		D3DXVec3TransformNormal(&vVtx, &vVtx, &matRot);
	}

	vVtx = _vec3(0.3f*fX, 1.f*fY - 0.5f, 0.f);///////13
	for (size_t i = 0; i < 10; ++i)
	{
		
		pVertices[index++].vPosition = vVtx;
		D3DXVec3TransformNormal(&vVtx, &vVtx, &matRot);
	}




	m_pVB->Unlock();

}

void CBuffer_Tire::Ready_Index()
{

	POLYGON16*	pIndices = nullptr;
	m_pIB->Lock(0, 0, (void**)&pIndices, 0);

	_int index = 0;

	//������ 280��
	for (size_t i = 0; i < 13; ++i)
	{
		for (size_t j = 0; j < 9; ++j)
		{
			//���
			pIndices[index]._0 = i * 10 + j;
			pIndices[index]._1 = i * 10 + j+1;
			pIndices[index]._2 = (i+1) * 10 + j+1;
			index++;

			//����
			pIndices[index]._0 = i * 10 + j;
			pIndices[index]._1 = (i+1) * 10 + j + 1;
			pIndices[index]._2 = (i+1) * 10 + j ;
			index++;
		}


		//���
		pIndices[index]._0 = i * 10 + 9;
		pIndices[index]._1 = i * 10;
		pIndices[index]._2 = (i+1) * 10;
		index++;

		//����
		pIndices[index]._0 = i * 10 + 9;
		pIndices[index]._1 = i * 10 + 10;
		pIndices[index]._2 = (i+1) * 10 + 9;
		index++;
	}

	//�������� �������ٸ�
	for (size_t j = 0; j < 9; ++j)
	{
		//���
		pIndices[index]._0 = j+1;
		pIndices[index]._1 = j;
		pIndices[index]._2 = j+130;
		index++;

		//����
		pIndices[index]._0 = j + 1;
		pIndices[index]._1 = j + 130;
		pIndices[index]._2 = j + 131;
		index++;
	}

	//���
	pIndices[index]._0 = 0;
	pIndices[index]._1 = 9;
	pIndices[index]._2 = 139;
	index++;


	//����
	pIndices[index]._0 = 0;
	pIndices[index]._1 = 139;
	pIndices[index]._2 = 130;
	index++;

	
	m_pIB->Unlock();
}

void CBuffer_Tire::Render_VIBuffer()
{
	m_pGraphic_Device->SetStreamSource(0, m_pVB, 0, m_iStride);
	m_pGraphic_Device->SetFVF(m_dwFVF);
	m_pGraphic_Device->SetIndices(m_pIB);
	m_pGraphic_Device->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, m_iNumVertices, 0, m_iNumPolygons);
}

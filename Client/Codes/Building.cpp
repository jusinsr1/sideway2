#include "stdafx.h"
#include "..\Headers\Building.h"
#include "Management.h"
#include "Buffer_CubeLine.h"


_USING(Client)

CBuilding::CBuilding(LPDIRECT3DDEVICE9 pGraphic_Device)
	:CGameObject(pGraphic_Device)
{
}

CBuilding::CBuilding(const CBuilding & rhs)
	: CGameObject(rhs)
{
}

HRESULT CBuilding::Ready_Prototype()
{
	return NOERROR;
}

HRESULT CBuilding::Ready_GameObject()
{
	if (FAILED(Ready_Component()))
		return E_FAIL;

	return NOERROR;
}

_int CBuilding::Update_GameObject(const _float & fTImeDelta)
{

	return _int();
}

_int CBuilding::LastUpdate_GameObject(const _float & fTImeDelta)
{
	if (m_pRendererCom == nullptr)
		return -1;
	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_NONEALPHA, this)))
		return -1;

	return _int();
}

void CBuilding::Render_GameObject()
{
	if (m_pBufferCom == nullptr)
		return;

//	m_pGraphic_Device->SetRenderState(D3DRS_ZENABLE, true);

	m_pTransformCom->SetUp_OnGraphicDev();

	if (FAILED(m_pTextureCom->SetUp_OnGraphicDev()))
		return;

	m_pBufferCom->Render_VIBuffer();

//	m_pGraphic_Device->SetRenderState(D3DRS_ZENABLE, false);

}

void CBuilding::SetData(_vec3 vecScale,_vec3 vecPos)
{
	m_pTransformCom->Scaling(vecScale.x, vecScale.y, vecScale.z);
	m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &vecPos);
}

CBuilding * CBuilding::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CBuilding* pInstance = new CBuilding(pGraphic_Device);
	if (FAILED(pInstance->Ready_Prototype()))
	{
		_MSG_BOX("CBuilding Created Failed");
		Safe_Release(pInstance);
	}
	return pInstance;
}

CGameObject * CBuilding::Clone_GameObject()
{
	CBuilding* pInstance = new CBuilding(*this);
	if (FAILED(pInstance->Ready_GameObject()))
	{
		_MSG_BOX("CBuilding Created Failed");
		Safe_Release(pInstance);
	}
	return pInstance;
}

HRESULT CBuilding::Ready_Component()
{
	CManagement* pManagement = GET_INSTANCE(CManagement);
	if (pManagement == nullptr)
		return E_FAIL;
	pManagement->AddRef();

	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (m_pTransformCom == nullptr)
		return E_FAIL;
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;

	m_pRendererCom = (CRenderer*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Renderer");
	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
		return E_FAIL;

	m_pTextureCom = (CTexture*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Texture_Player_Idle_Left");
	if (FAILED(Add_Component(L"Com_Texture", m_pTextureCom)))
		return E_FAIL;

	m_pBufferCom = (CBuffer_CubeLine*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Buffer_CubeLine");
	if (FAILED(Add_Component(L"Com_Buffer", m_pBufferCom)))
		return E_FAIL;

	Safe_Release(pManagement);

	return NOERROR;
}

void CBuilding::Free()
{
	Safe_Release(m_pTextureCom);
	Safe_Release(m_pBufferCom);
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pTransformCom);


	CGameObject::Free();
}

#include "stdafx.h"
#include "..\Headers\Camera_Debug.h"

_USING(Client)

CCamera_Debug::CCamera_Debug(LPDIRECT3DDEVICE9 pGraphic_Device)
	:CCamera(pGraphic_Device)
{
}

CCamera_Debug::CCamera_Debug(const CCamera_Debug & rhs)
	:CCamera(rhs)
{
}

HRESULT CCamera_Debug::Ready_Prototype()
{
	if (FAILED(CCamera::Ready_Prototype()))
		return E_FAIL;

	return NOERROR;
}

HRESULT CCamera_Debug::Ready_GameObject()
{
	if (FAILED(CCamera::Ready_GameObject()))
		return E_FAIL;

	m_pTransform->SetUp_Speed(5.f, D3DXToRadian(90.f));


	return NOERROR;
}

_int CCamera_Debug::Update_GameObject(const _float & fTimeDelta)
{
	if (m_pInput_Device->IsKeyPressing(DIK_D))
		m_pTransform->Go_Right(fTimeDelta);
	if (m_pInput_Device->IsKeyPressing(DIK_A))
		m_pTransform->Go_Left(fTimeDelta);
	if (m_pInput_Device->IsKeyPressing(DIK_W))
		m_pTransform->Go_Straight(fTimeDelta);
	if (m_pInput_Device->IsKeyPressing(DIK_S))
		m_pTransform->BackWard(fTimeDelta);

	_long MouseMove = 0;
	if (MouseMove = m_pInput_Device->Get_DIMouseMove(CInput_Device::DIM_X))
		m_pTransform->Rotation_Y(0.1f*MouseMove*fTimeDelta);
	if (MouseMove = m_pInput_Device->Get_DIMouseMove(CInput_Device::DIM_Y))
		m_pTransform->Rotation_AxisRight(0.1f*MouseMove*fTimeDelta);



	return _int();
}

_int CCamera_Debug::LastUpdate_GameObject(const _float & fTimeDelta)
{
	Invalidate_ViewProjMatrix();

	return _int();
}

void CCamera_Debug::Render_GameObject()
{
}

CCamera_Debug * CCamera_Debug::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CCamera_Debug* pInstance = new CCamera_Debug(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		_MSG_BOX("CCamera_Debug Created Failed")
			Safe_Release(pInstance);
	}
	return pInstance;
}

CGameObject * CCamera_Debug::Clone_GameObject()
{
	CCamera_Debug* pInstance = new CCamera_Debug(*this);
	
	if (FAILED(pInstance->Ready_GameObject()))
	{
		_MSG_BOX("CCamera_Debug Clone Failed")
		Safe_Release(pInstance);
	}
	return pInstance;
}

void CCamera_Debug::Free()
{
	CCamera::Free();
}

#include "stdafx.h"
#include "..\Headers\ObjMove.h"
#include "Transform.h"

_USING(Client)

CObjMove::CObjMove(LPDIRECT3DDEVICE9 pGraphic_Device)
	:CComponent(pGraphic_Device)

{
	
}

CObjMove::CObjMove(const CObjMove & rhs)
	: CComponent(rhs)
{
}

HRESULT CObjMove::Ready_Obj()
{

	return NOERROR;
}

void CObjMove::Setup_Obj(PLANEINFO* _pPlane, CTransform* pTrans)
{
	pPlane = _pPlane;
	m_pTransform = pTrans;

	 vPlayerSize = 
	 { D3DXVec3Length(pTrans->Get_StateInfo(CTransform::STATE_RIGHT))
		,D3DXVec3Length(pTrans->Get_StateInfo(CTransform::STATE_UP)) };


}

void CObjMove::Update()
{
	iCurDir = pPlane->iPlaneNum % 5;

	CalRUL();
	CalPosWorld();
	CalPosInPlane();

	iPreDir = iCurDir;
}


void CObjMove::CalRUL()
{
	if (iCurDir == iPreDir)
		return;

	_float	fScaleR = D3DXVec3Length(m_pTransform->Get_StateInfo(CTransform::STATE_RIGHT));
	_float	fScaleU = D3DXVec3Length(m_pTransform->Get_StateInfo(CTransform::STATE_UP));
	_float	fScaleL = D3DXVec3Length(m_pTransform->Get_StateInfo(CTransform::STATE_LOOK));


	switch (iCurDir)
	{
	case 0:
		m_pTransform->Set_StateInfo(CTransform::STATE_RIGHT, &_vec3(fScaleR,0.f,0.f));
		m_pTransform->Set_StateInfo(CTransform::STATE_UP, &_vec3(0.f, fScaleU, 0.f));
		m_pTransform->Set_StateInfo(CTransform::STATE_LOOK, &_vec3(0.f, 0.f, fScaleL));
		break;
	case 1:
		m_pTransform->Set_StateInfo(CTransform::STATE_RIGHT, &_vec3(0.f, 0.f, -fScaleR));
		m_pTransform->Set_StateInfo(CTransform::STATE_UP, &_vec3(0.f, fScaleU, 0.f));
		m_pTransform->Set_StateInfo(CTransform::STATE_LOOK, &_vec3(fScaleL, 0.f, 0.f));
		break;
	case 2:
		m_pTransform->Set_StateInfo(CTransform::STATE_RIGHT, &_vec3(-fScaleR, 0.f, 0.f));
		m_pTransform->Set_StateInfo(CTransform::STATE_UP, &_vec3(0.f, fScaleU, 0.f));
		m_pTransform->Set_StateInfo(CTransform::STATE_LOOK, &_vec3(0.f, 0.f, -fScaleL));
		break;
	case 3:
		m_pTransform->Set_StateInfo(CTransform::STATE_RIGHT, &_vec3(0.f, 0.f, fScaleR));
		m_pTransform->Set_StateInfo(CTransform::STATE_UP, &_vec3(0.f, fScaleU, 0.f));
		m_pTransform->Set_StateInfo(CTransform::STATE_LOOK, &_vec3(-fScaleL, 0.f, 0.f));
		break;
	case 4:
		m_pTransform->SetUp_RotationRight(D3DXToRadian(90.f));
		break;
	}


}


void CObjMove::CalPosWorld()
{
	if (iCurDir == iPreDir)
		return;

	_vec3 posWorld = *(m_pTransform->Get_StateInfo(CTransform::STATE_POSITION));

	switch (iCurDir)
	{
	case 0:
		posWorld.z = pPlane->Pos.z;
		m_pTransform->Set_StateInfo(CTransform::STATE_POSITION,&posWorld);
		break;
	case 1:
		posWorld.x = pPlane->Pos.x;
		m_pTransform->Set_StateInfo(CTransform::STATE_POSITION, &posWorld);
		break;
	case 2:
		posWorld.z = pPlane->Pos.z;
		m_pTransform->Set_StateInfo(CTransform::STATE_POSITION, &posWorld);
		break;
	case 3:
		posWorld.x = pPlane->Pos.x;
		m_pTransform->Set_StateInfo(CTransform::STATE_POSITION, &posWorld);
		break;
	case 4:
		posWorld.y = pPlane->Pos.y;
		m_pTransform->Set_StateInfo(CTransform::STATE_POSITION, &posWorld);
		break;
	}
}

void CObjMove::CalPosInPlane()
{

	_vec3 posWorld = *(m_pTransform->Get_StateInfo(CTransform::STATE_POSITION));
	_vec3 posInPlane = posWorld - pPlane->Pos;

	switch (iCurDir)
	{
	case 0:
		vPosInPlane = _vec2(posInPlane.x, posInPlane.y);
		break;
	case 1:
		vPosInPlane = _vec2(-posInPlane.z, posInPlane.y);
		break;
	case 2:
		vPosInPlane = _vec2(-posInPlane.x, posInPlane.y);
		break;
	case 3:
		vPosInPlane = _vec2(posInPlane.z, posInPlane.y);
		break;
	case 4:
		vPosInPlane = _vec2(posInPlane.x, posInPlane.z);
		break;
	}
}


_bool CObjMove::CheckIn(_int& _curPlane, _int& _prePlaneOut)
{
	_prePlaneOut = _curPlane;

	//평면이바뀌었는지체크
	if (vPosInPlane.x < -pPlane->fWidth*0.5f)
	{
		_curPlane = pPlane->arrMoveablePlane[LEFT];
		return true;
	}
	else if (vPosInPlane.x > pPlane->fWidth*0.5f)
	{
		_curPlane = pPlane->arrMoveablePlane[RIGHT];
		return true;
	}
	else if (vPosInPlane.y > pPlane->fHeight*0.5f)
	{
		_curPlane = pPlane->arrMoveablePlane[UP];
		return true;
	}
	else if (vPosInPlane.y <-pPlane->fHeight*0.5f)
	{
		_curPlane = pPlane->arrMoveablePlane[DOWN];
		return true;
	}

	return false;
}

void CObjMove::Get_ClimbDir(Direction & _climbDirOut)
{
	//올라온 벽전해주기
	if (iCurDir != 4)
		_climbDirOut = climbDir = Direction(iCurDir);
}

void CObjMove::Get_TouchDir(Direction & _touchDir, _float & offX, _float & offY)
{
	if (iCurDir != 4)
		_touchDir= TouchDir(offX, offY);
	else
		_touchDir= TouchDirInRoof(offX, offY);
}


Direction CObjMove::TouchDir(_float& offX, _float& offY)
{
	offX = 0.5f;
	offY = 0.5f;
	
	_float	left = vPosInPlane.x - 0.5f*vPlayerSize.x;
	_float	right = vPosInPlane.x + 0.5f*vPlayerSize.x;
	_float	up = vPosInPlane.y + 0.5f*vPlayerSize.y;
	_float	down = vPosInPlane.y - 0.5f* vPlayerSize.y;

	_float width = 0.f;
	_float height = 0.f;


	if (0 < (height = -0.5f*(pPlane->fHeight) - down))
	{
		if (iCurDir==pPlane->arrMoveablePlane[DOWN]%5)
			return DIR_END;
		offY = 1.f - height / vPlayerSize.y;
		return  DOWN;
	}
	else if (0 < (height = up - 0.5f*(pPlane->fHeight)))
	{
		if (iCurDir == pPlane->arrMoveablePlane[UP]%5)
			return DIR_END;
		offY = height / vPlayerSize.y;
		return  UP;
	}
	else if (0 <(width = -0.5f*(pPlane->fWidth) - left))
	{
		if (iCurDir == pPlane->arrMoveablePlane[LEFT] % 5)
			return DIR_END;
		offX = width / vPlayerSize.x;
		return  LEFT;
	}
	else if (0 < (width = right - 0.5f*(pPlane->fWidth)))
	{
		if (iCurDir == pPlane->arrMoveablePlane[RIGHT] % 5)
			return DIR_END;
		offX = 1.f - width / vPlayerSize.x;
		return  RIGHT;
	}


	return  DIR_END;
}

Direction CObjMove::TouchDirInRoof(_float& offX, _float& offY)
{
	offX = 0.5f;
	offY = 0.5f;

	_float	left = 0.f;
	_float	right = 0.f;
	_float	up = 0.f;
	_float	down = 0.f;

	_float	planeL = 0.f;
	_float	planeR = 0.f;
	_float	planeU = 0.f;
	_float	planeD = 0.f;

	switch (climbDir)
	{
	case DOWN:
		left = vPosInPlane.x - 0.5f*vPlayerSize.x;
		right = vPosInPlane.x + 0.5f*vPlayerSize.x;
		up = vPosInPlane.y + 0.5f*vPlayerSize.y;
		down = vPosInPlane.y - 0.5f* vPlayerSize.y;
		planeL = -0.5f*pPlane->fWidth;
		planeR = 0.5f*pPlane->fWidth;
		planeU = 0.5f*pPlane->fHeight;
		planeD = -0.5f*pPlane->fHeight;
		break;
	case LEFT:
		left = vPosInPlane.y + 0.5f*vPlayerSize.x;
		right = vPosInPlane.y - 0.5f*vPlayerSize.x;
		up = vPosInPlane.x + 0.5f*vPlayerSize.y;
		down = vPosInPlane.x - 0.5f* vPlayerSize.y;
		planeL = 0.5f*pPlane->fHeight;
		planeR = -0.5f*pPlane->fHeight;
		planeU = 0.5f*pPlane->fWidth;
		planeD = -0.5f*pPlane->fWidth;
		break;
	case UP:
		left = vPosInPlane.x + 0.5f*vPlayerSize.x;
		right = vPosInPlane.x - 0.5f*vPlayerSize.x;
		up = vPosInPlane.y - 0.5f*vPlayerSize.y;
		down = vPosInPlane.y + 0.5f* vPlayerSize.y;
		planeL = 0.5f*pPlane->fWidth;
		planeR = -0.5f*pPlane->fWidth;
		planeU = -0.5f*pPlane->fHeight;
		planeD = 0.5f*pPlane->fHeight;
		break;
	case RIGHT:
		left = vPosInPlane.y - 0.5f*vPlayerSize.x;
		right = vPosInPlane.y + 0.5f*vPlayerSize.x;
		up = vPosInPlane.x - 0.5f*vPlayerSize.y;
		down = vPosInPlane.x + 0.5f* vPlayerSize.y;
		planeL = -0.5f*pPlane->fHeight;
		planeR = 0.5f*pPlane->fHeight;
		planeU = -0.5f*pPlane->fWidth;
		planeD = 0.5f*pPlane->fWidth;
		break;
	}


	_float gap = 0.f;

	if (0<(gap = abs(left) - abs(planeL)))//캐릭터왼쪽으로삐져나왓다
	{
		if (iCurDir == pPlane->arrMoveablePlane[(LEFT + climbDir) % 4] % 5)
			return DIR_END;
		offX = gap / vPlayerSize.x;
		return  Direction((LEFT+ climbDir)%4);
	}
	else if (0<(gap = abs(right) - abs(planeR)))//오른쪽
	{
		if (iCurDir == pPlane->arrMoveablePlane[(RIGHT + climbDir) % 4] % 5)
			return DIR_END;
		offX = 1.f - gap / vPlayerSize.x;
		return  Direction((RIGHT + climbDir) % 4);
	}
	else if (0 < (gap = abs(up) - abs(planeU)))//캐릭터머리쪽
	{
		if (iCurDir == pPlane->arrMoveablePlane[(UP + climbDir) % 4] % 5)
			return DIR_END;
		offY = gap / vPlayerSize.y;
		return  Direction((UP + climbDir) % 4);
	}
	else if (0< (gap = abs(down) - abs(planeD)))//캐릭터발쪽
	{
		if (iCurDir == pPlane->arrMoveablePlane[(DOWN + climbDir) % 4] % 5)
			return DIR_END;
		offY = 1.f - gap / vPlayerSize.y;
		return  Direction((DOWN + climbDir) % 4);
	}

	return  DIR_END;


}



CObjMove * CObjMove::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CObjMove* pInstance = new CObjMove(pGraphic_Device);

	if (FAILED(pInstance->Ready_Obj()))
	{
		_MSG_BOX("CTouch Created Failed")
			Safe_Release(pInstance);
	}
	return pInstance;
}

CComponent * CObjMove::Clone_Component()
{
	return new CObjMove(*this);
}

void CObjMove::Free()
{
	CComponent::Free();
}


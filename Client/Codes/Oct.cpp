#include "stdafx.h"
#include "Oct.h"
#include "Management.h"

_USING(Client)

COct::COct(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CGameObject(pGraphic_Device)
{
}

COct::COct(const COct & rhs)
	: CGameObject(rhs),
	m_byDrawID(rhs.m_byDrawID),
	m_byKey(rhs.m_byKey)
{
	
}


// 원형객체 생성될 때 호출.
HRESULT COct::Ready_Prototype()
{
	// 파일 입출력 등, 초기화에 시간이 걸리는 데이터들 셋.


	return NOERROR;
}

// 복사본객체 생성될 때 호출.
HRESULT COct::Ready_GameObject()
{
	// 실제 사용되기위한 객체들 만의 추가적인 데이터를 셋.

	// 현재 객체에게 필요한 컴포넌트들을 복제해서 온다.
	
	
	if (FAILED(Ready_Component()))
		return E_FAIL;

	m_pTransformCom->Scaling(1.f, 1.f, 1.f);
	m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &_vec3(0.0f, 0.f, 0.f));
	
	return NOERROR;
}

_int COct::Update_GameObject(const _float & fTimeDelta)
{	

	return _int();
}

_int COct::LastUpdate_GameObject(const _float & fTimeDelta)
{
	if (nullptr == m_pRendererCom)
		return -1;

	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_NONEALPHA, this)))
		return -1;

	return 0;
}

void COct::Render_GameObject()
{
	if (nullptr == m_pBufferCom)
		return;

	m_pTransformCom->SetUp_OnGraphicDev();
				
	//if (FAILED(m_pTextureCom->SetUp_OnGraphicDev(m_iTexIdx)))
	//	return;

	
	m_pBufferCom->Render_VIBuffer();
	
}

HRESULT COct::Ready_Component()
{
	CManagement*		pManagement = CManagement::GetInstance();
	if (nullptr == pManagement)
		return E_FAIL;
	pManagement->AddRef();

	// For.Com_Transform
	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;	

	// For.Com_Renderer
	m_pRendererCom = (CRenderer*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Renderer");
	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
		return E_FAIL;

	 //For.Com_Buffer
 	m_pBufferCom = (CBuffer_RcTex*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Buffer_RcTex");
		if (FAILED(Add_Component(L"Com_Buffer", m_pBufferCom)))
			return E_FAIL;


	//m_pTextureCom = (CTexture*)pManagement->Clone_Component(SCENE_LOADING, L"Component_Texture_Loading_Background");
	//	if (FAILED(Add_Component(L"Com_Texture", m_pTextureCom)))
	//		return E_FAIL;


	Safe_Release(pManagement);


	return NOERROR;
}

// 원형객체를 생성하기위해 만들어진 함수.
COct * COct::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	COct*	pInstance = new COct(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		MessageBox(0, L"COct Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

// 원형객체가 호출해주는 함수(원형주소->Clone_GameObject())
// 복사본객첼르 생성하기위해.
CGameObject * COct::Clone_GameObject()
{
	this->AddClone();

	COct*	pInstance = new COct(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		MessageBox(0, L"COct Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

void COct::Free()
{
	Safe_Release(m_pTextureCom);
	Safe_Release(m_pBufferCom);
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pTransformCom);

	CGameObject::Free();
}

#include "stdafx.h"
#include "..\Headers\Scene_Logo.h"
#include "Management.h"
#include "Back_Logo.h"
#include "Scene_Stage.h"

_USING(Client)

CScene_Logo::CScene_Logo(LPDIRECT3DDEVICE9 pGraphic_Device)
	:CScene(pGraphic_Device)
{
}

HRESULT CScene_Logo::Ready_Scene()
{
	if (FAILED(Ready_Prototype_GameObject()))
		return E_FAIL;
	if (FAILED(Ready_Prototype_Component()))
		return E_FAIL;
	if (FAILED(Ready_Layer_BackGround(L"Layer_BackGround")))
		return E_FAIL;
	return NOERROR;
}

_int CScene_Logo::Update_Scene(const _float & fTimeDelta)
{
	return CScene::Update_Scene(fTimeDelta);
}

_int CScene_Logo::LastUpdate_Scene(const _float & fTimeDelta)
{
	if (GetKeyState(VK_SPACE) & 0x8000)
	{
		CManagement*	pManagement = GET_INSTANCE(CManagement);
		if (pManagement == nullptr)
			return -1;
		pManagement->AddRef();

		CScene_Stage* pNewScene = CScene_Stage::Create(m_pGraphic_Device);
		if (pNewScene == nullptr)
			return -1;
		if (FAILED(pManagement->SetUp_ScenePointer(pNewScene)))
			return -1;
		Safe_Release(pNewScene);
		Safe_Release(pManagement);
		return 0;
	}

	return CScene::LastUpdate_Scene(fTimeDelta);
}

void CScene_Logo::Render_Scene()
{
}

HRESULT CScene_Logo::Ready_Prototype_GameObject()
{
	CManagement*		pManagement = GET_INSTANCE(CManagement);

	if (pManagement == nullptr)
		return E_FAIL;

	pManagement->AddRef();

	if (FAILED(pManagement->Add_Prototype_GameObject(L"GameObject_Back_Logo", CBack_Logo::Create(m_pGraphic_Device))))
		return E_FAIL;

	Safe_Release(pManagement);
	return NOERROR;
}

HRESULT CScene_Logo::Ready_Prototype_Component()
{
	CManagement* pManagement = GET_INSTANCE(CManagement);
	if (pManagement == nullptr)
		return E_FAIL;

	pManagement->AddRef();

	// For.Component_Texture_Logo_Background
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_LOGO,
		L"Component_Texture_Logo_Background", CTexture::Create(m_pGraphic_Device, CTexture::TYPE_GENERAL,
			L"../../Resources/Textures/Others/Logo/Logo_Background/%d.png", 1))))
		return E_FAIL;

	// For.Component_Texture_Logo_Part

	if (FAILED(pManagement->Add_Prototype_Component(SCENE_LOGO,
		L"Component_Texture_Logo_Part", CTexture::Create(m_pGraphic_Device, CTexture::TYPE_GENERAL,
			L"../../Resources/Textures/Others/Logo/Logo_Part/%d.dds", 6))))
		return E_FAIL;

	// For.Component_Texture_Logo_Menu
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_LOGO,
		L"Component_Texture_Logo_Menu", CTexture::Create(m_pGraphic_Device, CTexture::TYPE_GENERAL,
			L"../../Resources/Textures/Others/Logo/Logo_Menu/%d.png", 6))))
		return E_FAIL;

	Safe_Release(pManagement);

	return NOERROR;
}

HRESULT CScene_Logo::Ready_Layer_BackGround(const _tchar * pLayerTag)
{
	CManagement*		pManagement = GET_INSTANCE(CManagement);

	if (pManagement == nullptr)
		return E_FAIL;

	pManagement->AddRef();

	for (size_t i = 0; i < 8; i++)
	{
		if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_Back_Logo", SCENE_LOGO, pLayerTag)))
			return E_FAIL;
	}

	Safe_Release(pManagement);

	return NOERROR;
}

CScene_Logo * CScene_Logo::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CScene_Logo*	pInstance = new CScene_Logo(pGraphic_Device);

	if (FAILED(pInstance->Ready_Scene()))
	{
		_MSG_BOX("CScene_Logo Created Failed")
		Safe_Release(pInstance);
	}
	return pInstance;
}

void CScene_Logo::Free()
{
	CManagement* pManagement = GET_INSTANCE(CManagement);
	
	if (pManagement == nullptr)
		return;

	pManagement->AddRef();
	pManagement->Clear_Layers(SCENE_LOGO);
	Safe_Release(pManagement);

	CScene::Free();
}

#include "stdafx.h"
#include "..\Headers\Scene_Stage.h"
#include "Back_Logo.h"
#include "Management.h"
#include "Terrain.h"
#include "Camera_Debug.h"
#include "Player.h"
#include "Buffer_Player.h"
#include "ObjMove.h"
#include "Building.h"
#include "Buffer_Cube.h"
#include "Tire.h"
#include "Buffer_Tire.h"
#include "Buffer_CubeLine.h"
_USING(Client)

CScene_Stage::CScene_Stage(LPDIRECT3DDEVICE9 pGraphic_Device)
	:CScene(pGraphic_Device)
{

}

D3DMATERIAL9 CScene_Stage::InitMtrl(D3DXCOLOR a, D3DXCOLOR d, D3DXCOLOR s, D3DXCOLOR e, float p)
{
	D3DMATERIAL9 mtrl;
	mtrl.Ambient = a;
	mtrl.Diffuse = d;
	mtrl.Specular = s;
	mtrl.Emissive = e;
	mtrl.Power = p;
	return mtrl;
}

HRESULT CScene_Stage::Ready_Scene()
{
	if (FAILED(Ready_Prototype_Component()))
		return E_FAIL;
	if (FAILED(Ready_Prototype_GameObject()))
		return E_FAIL;
	
	if (FAILED(Ready_Layer_Camera(L"Layer_Camera")))
		return E_FAIL;


	if (FAILED(Ready_Layer_BackGround(L"Layer_BackGround")))
		return E_FAIL;

	if (FAILED(Ready_Layer_Tire(L"Layer_Tire")))
		return E_FAIL;
	if (FAILED(Ready_Layer_Building(L"Layer_Building")))
		return E_FAIL;

	

	if (FAILED(Ready_Layer_Player(L"Layer_Player")))
		return E_FAIL;
	

	//D3DXVECTOR3 dir(-0.5f, -0.5f, 0.5f);
	//D3DXCOLOR   color = WHITE;
	//InitLight(&dir, &color);

	//D3DMATERIAL9 mtrl = InitMtrl(WHITE, WHITE, WHITE, BLACK, 2.0f);
	//m_pGraphic_Device->SetMaterial(&mtrl);

	//m_pGraphic_Device->SetLight(0, &light);
	//m_pGraphic_Device->LightEnable(0, true);
	//m_pGraphic_Device->SetRenderState(D3DRS_NORMALIZENORMALS, true);
	//m_pGraphic_Device->SetRenderState(D3DRS_SPECULARENABLE, false);

	//m_pGraphic_Device->SetRenderState(D3DRS_LIGHTING, TRUE);
	

	return NOERROR;
}

_int CScene_Stage::Update_Scene(const _float & fTimeDelta)
{
	return CScene::Update_Scene(fTimeDelta);
}

_int CScene_Stage::LastUpdate_Scene(const _float & fTimeDelta)
{
	return CScene::LastUpdate_Scene(fTimeDelta);
}

void CScene_Stage::Render_Scene()
{
}

HRESULT CScene_Stage::Ready_Prototype_GameObject()
{
	CManagement* pManagement = GET_INSTANCE(CManagement);

	if (pManagement == nullptr)
		return E_FAIL;

	pManagement->AddRef();
	
	if (FAILED(pManagement->Add_Prototype_GameObject(L"GameObject_Terrain", CTerrain::Create(m_pGraphic_Device))))
		return E_FAIL;

	if (FAILED(pManagement->Add_Prototype_GameObject(L"GameObject_Player", CPlayer::Create(m_pGraphic_Device))))
		return E_FAIL;

	if (FAILED(pManagement->Add_Prototype_GameObject(L"GameObject_Building", CBuilding::Create(m_pGraphic_Device))))
		return E_FAIL; 

	if (FAILED(pManagement->Add_Prototype_GameObject(L"GameObject_Tire", CTire::Create(m_pGraphic_Device))))
		return E_FAIL;

	Safe_Release(pManagement);
	return NOERROR;
}

HRESULT CScene_Stage::Ready_Prototype_Component()
{
	CManagement* pManagement = GET_INSTANCE(CManagement);

	if (pManagement == nullptr)
		return E_FAIL;

	pManagement->AddRef();

	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Component_Buffer_Terrain", CBuffer_Terrain::Create(m_pGraphic_Device, 100, 100))))
		return E_FAIL;

	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Component_Buffer_Player", CBuffer_Player::Create(m_pGraphic_Device))))
		return E_FAIL;

	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Component_ObjMove_Player", CObjMove::Create(m_pGraphic_Device))))
		return E_FAIL;

	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Component_Buffer_Cube", CBuffer_Cube::Create(m_pGraphic_Device))))
		return E_FAIL;

	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Component_Buffer_CubeLine", CBuffer_CubeLine::Create(m_pGraphic_Device))))
		return E_FAIL;

	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Component_Buffer_Tire", CBuffer_Tire::Create(m_pGraphic_Device))))
		return E_FAIL;

	// For.Component_Texture_Terrain
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Component_Texture_Terrain",
		CTexture::Create(m_pGraphic_Device, CTexture::TYPE_GENERAL,
			L"../../Resources/Textures/Others/Terrain/%d.tga", 1))))
		return E_FAIL;

	// For.Component_Texture_Test
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Component_Texture_Test",
		CTexture::Create(m_pGraphic_Device, CTexture::TYPE_CUBE,
			L"../../Resources/Textures/Others/z.Sample/SkyBox/Burger%d.dds", 4))))
		return E_FAIL;

	// For.Component_Texture_Player_Idle_Left
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE,
		L"Component_Texture_Player_Idle_Left", CTexture::Create(m_pGraphic_Device, CTexture::TYPE_GENERAL,
			L"../../Resources/Textures/Others/Player/Main/Player_Idle_Left/%d.png", 16))))
		return E_FAIL;

	// For.Component_Texture_Player_Idle_Right
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE,
		L"Component_Texture_Player_Idle_Right", CTexture::Create(m_pGraphic_Device, CTexture::TYPE_GENERAL,
			L"../../Resources/Textures/Others/Player/Main/Player_Idle_Right/%d.png", 16))))
		return E_FAIL;

	// For.Component_Texture_Player_Run_Left
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE,
		L"Component_Texture_Player_Run_Left", CTexture::Create(m_pGraphic_Device, CTexture::TYPE_GENERAL,
			L"../../Resources/Textures/Others/Player/Main/Player_Run_Left/%d.png", 6))))
		return E_FAIL;

	// For.Component_Texture_Player_Run_Right
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE,
		L"Component_Texture_Player_Run_Right", CTexture::Create(m_pGraphic_Device, CTexture::TYPE_GENERAL,
			L"../../Resources/Textures/Others/Player/Main/Player_Run_Right/%d.png", 6))))
		return E_FAIL;

	// For.Component_Texture_UI
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE,
		L"Component_Texture_UI", CTexture::Create(m_pGraphic_Device, CTexture::TYPE_GENERAL,
			L"../../Resources/Textures/Others/UI/Char_InGame/%d.dds", 2))))
		return E_FAIL;

	// For.Component_Texture_Score
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE,
		L"Component_Texture_Score", CTexture::Create(m_pGraphic_Device, CTexture::TYPE_GENERAL,
			L"../../Resources/Textures/Others/UI/Score_Board/%d.png", 1))))
		return E_FAIL;

	Safe_Release(pManagement);
	return NOERROR;
}

HRESULT CScene_Stage::Ready_Layer_Camera(const _tchar * pLayerTag)
{
	CManagement*	pManagement = GET_INSTANCE(CManagement);

	if (pManagement == nullptr)
		return E_FAIL;

	pManagement->AddRef();

	CCamera_Debug*	pCameraObject = nullptr;

	if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_Camera_Debug", SCENE_STAGE, pLayerTag, (CGameObject**)&pCameraObject)))
		return E_FAIL;

	CAMERADESC	CameraDesc;

	ZeroMemory(&CameraDesc, sizeof(CAMERADESC));
	CameraDesc.vEye = _vec3(0.f, 20.f, -10.f);
	CameraDesc.vAt = _vec3(0.f, 0.f, 0.f);
	CameraDesc.vAxisY = _vec3(0.f, 1.f, 0.f);

	PROJDESC	ProjDesc;
	ZeroMemory(&ProjDesc, sizeof(PROJDESC));
	ProjDesc.fFovY = D3DXToRadian(60.f);
	ProjDesc.fAspect = _float(g_iBackCX) / g_iBackCY;
	ProjDesc.fNear = 0.2f;
	ProjDesc.fFar = 500.f;

	if (FAILED(pCameraObject->SetUp_CameraProjDesc(CameraDesc, ProjDesc)))
		return E_FAIL;

	Safe_Release(pManagement);
	return NOERROR;
}

HRESULT CScene_Stage::Ready_Layer_BackGround(const _tchar * pLayerTag)
{
	CManagement*	pManagement = GET_INSTANCE(CManagement);

	if (pManagement == nullptr)
		return E_FAIL;

	pManagement->AddRef();



	if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_Terrain", SCENE_STAGE, pLayerTag)))
		return E_FAIL;

	Safe_Release(pManagement);

	return NOERROR;
}

HRESULT CScene_Stage::Ready_Layer_Player(const _tchar * pLayerTag)
{
	CManagement*	pManagement = GET_INSTANCE(CManagement);

	if (pManagement == nullptr)
		return E_FAIL;

	pManagement->AddRef();

	if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_Player", SCENE_STAGE, pLayerTag)))
		return E_FAIL;

	Safe_Release(pManagement);

	return NOERROR;
}

HRESULT CScene_Stage::Ready_Layer_Building(const _tchar * pLayerTag)
{
	CManagement*	pManagement = GET_INSTANCE(CManagement);

	if (pManagement == nullptr)
		return E_FAIL;

	pManagement->AddRef();

	CBuilding*	pBuilding1 = nullptr;
	if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_Building", SCENE_STAGE, pLayerTag, (CGameObject**)&pBuilding1)))
		return E_FAIL;
	pBuilding1->SetData(_vec3(10.f,10.f,10.f), _vec3(5.f, 5.f, 5.f));

	CBuilding*	pBuilding2 = nullptr;
	if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_Building", SCENE_STAGE, pLayerTag, (CGameObject**)&pBuilding2)))
		return E_FAIL;
	pBuilding2->SetData(_vec3(10.f, 10.f, 10.f), _vec3(15.f, 5.f, 15.f));

	CBuilding*	pBuilding3 = nullptr;
	if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_Building", SCENE_STAGE, pLayerTag, (CGameObject**)&pBuilding3)))
		return E_FAIL;
	pBuilding3->SetData(_vec3(10.f, 20.f, 10.f), _vec3(15.f, 10.f, 25.f));

	Safe_Release(pManagement);

	return NOERROR;
}

HRESULT CScene_Stage::Ready_Layer_Tire(const _tchar * pLayerTag)
{
	CManagement*	pManagement = GET_INSTANCE(CManagement);

	if (pManagement == nullptr)
		return E_FAIL;

	pManagement->AddRef();

	if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_Tire", SCENE_STAGE, pLayerTag)))
		return E_FAIL;

	Safe_Release(pManagement);

	return NOERROR;
}

void CScene_Stage::InitLight(D3DXVECTOR3* direction, D3DXCOLOR* color)
{
	light.Type      = D3DLIGHT_DIRECTIONAL;
	light.Ambient   = *color * 0.6f;
	light.Diffuse   = *color;
	light.Specular  = *color * 0.6f;
	light.Direction = *direction;
}


CScene_Stage * CScene_Stage::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CScene_Stage* pInstance = new CScene_Stage(pGraphic_Device);

	if (FAILED(pInstance->Ready_Scene()))
	{
		_MSG_BOX("CScene_Stage Created Failed")
		Safe_Release(pInstance);
	}
	return pInstance;
}

void CScene_Stage::Free()
{
	CManagement* pManagement = GET_INSTANCE(CManagement);

	if (pManagement == nullptr)
		return;

	pManagement->AddRef();
	pManagement->Clear_Layers(SCENE_STAGE);
	Safe_Release(pManagement);
	
	CScene::Free();
}

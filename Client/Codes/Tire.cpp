#include "stdafx.h"
#include "..\Headers\Tire.h"
#include "Management.h"
#include "Buffer_Tire.h"
#include "Collision.h"

_USING(Client)

CTire::CTire(LPDIRECT3DDEVICE9 pGraphic_Device)
	:CGameObject(pGraphic_Device)
{
}

CTire::CTire(const CTire & rhs)
	: CGameObject(rhs)
{
}

HRESULT CTire::Ready_Prototype()
{
	return NOERROR;
}

HRESULT CTire::Ready_GameObject()
{
	if (FAILED(Ready_Component()))
		return E_FAIL;
	///////////////////////////////////////////////////
	if (m_pCollCom == nullptr)
		return -1;
	if (FAILED(m_pCollCom->Add_CollGroup(CCollision::COLL_TIRE, this)))
		return -1;


	////////////////////////////////////////////////////////
	m_pTransformCom->Scaling(2.f, 2.f, 2.f);
	m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &_vec3(9.f, 1.f, -1.f));

	fX = 1.f;
	fY = 1.f;
	////////////////////////////////////////////////////////
	return NOERROR;
}

_int CTire::Update_GameObject(const _float & fTImeDelta)
{
	bounce(fTImeDelta);

	return _int();
}

_int CTire::LastUpdate_GameObject(const _float & fTImeDelta)
{
	if (m_pRendererCom == nullptr)
		return -1;
	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_NONEALPHA, this)))
		return -1;


	return _int();
}

void CTire::Render_GameObject()
{
	if (m_pBufferCom == nullptr)
		return;

//	m_pGraphic_Device->SetRenderState(D3DRS_ALPHATESTENABLE, FALSE);


	m_pTransformCom->SetUp_OnGraphicDev();



	m_pBufferCom->SetUp_BufferPlayer(fX,fY);
	m_pBufferCom->UpdateVertex();

	m_pBufferCom->Render_VIBuffer();

//	m_pGraphic_Device->SetRenderState(D3DRS_ALPHATESTENABLE, TRUE);
}

void CTire::Coll_GameObject()
{
	m_bTime = true;
}

void CTire::bounce(const _float & fTImeDelta)
{
	if (m_bTime)
	{
		fAngle += fTImeDelta*300.f;
		
		fX = 1+sin(D3DXToRadian(fAngle))*0.5f;
		fY = 1 - sin(D3DXToRadian(fAngle))*0.5f;

		//cout << fAngle << endl;
		if (fAngle > 180)
		{
			fAngle = 0.f;
			m_bTime = false;
		}
	}
}

CTire * CTire::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CTire* pInstance = new CTire(pGraphic_Device);
	if (FAILED(pInstance->Ready_Prototype()))
	{
		_MSG_BOX("CTire Created Failed");
		Safe_Release(pInstance);
	}
	return pInstance;
}

CGameObject * CTire::Clone_GameObject()
{
	CTire* pInstance = new CTire(*this);
	if (FAILED(pInstance->Ready_GameObject()))
	{
		_MSG_BOX("CTire Created Failed");
		Safe_Release(pInstance);
	}
	return pInstance;
}

HRESULT CTire::Ready_Component()
{
	CManagement* pManagement = GET_INSTANCE(CManagement);
	if (pManagement == nullptr)
		return E_FAIL;
	pManagement->AddRef();

	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (m_pTransformCom == nullptr)
		return E_FAIL;
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;

	m_pRendererCom = (CRenderer*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Renderer");
	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
		return E_FAIL;

	m_pCollCom = (CCollision*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Collision");
	if (FAILED(Add_Component(L"Com_Collision", m_pCollCom)))
		return E_FAIL;


	m_pBufferCom = (CBuffer_Tire*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Buffer_Tire");
	if (FAILED(Add_Component(L"Com_Buffer", m_pBufferCom)))
		return E_FAIL;


	Safe_Release(pManagement);

	return NOERROR;
}

void CTire::Free()
{
	Safe_Release(m_pBufferCom);
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pTransformCom);

	Safe_Release(m_pCollCom);

	CGameObject::Free();
}

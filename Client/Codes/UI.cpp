#include "stdafx.h"
#include "..\Headers\UI.h"
#include "Management.h"

_USING(Client)

CUI::CUI(LPDIRECT3DDEVICE9 pGraphic_Device)
	:CGameObject(pGraphic_Device)
{
}

CUI::CUI(const CUI & rhs)
	:CGameObject(rhs), 
	m_byDrawID(rhs.m_byDrawID)
{
}

HRESULT CUI::Ready_Prototype()
{

	return NOERROR;
}

HRESULT CUI::Ready_GameObject()
{
		if (FAILED(Ready_Component()))
		return E_FAIL;

		switch (m_byDrawID)
		{
		case 1:
			m_fX = 100.f;		m_fY = g_iBackCY * 0.9f;
			m_fSizeX = 50.0f;	m_fSizeY = 50.0f;
			break;
		case 2:
			m_fX = 175.f;		m_fY = g_iBackCY * 0.1f;
			m_fSizeX = 200.0f;	m_fSizeY = 50.0f;
			break;
			break;
		default:
			break;
		}

	m_pTransformCom->Scaling(m_fSizeX, m_fSizeY, 1.f);
	m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &_vec3(m_fX, m_fY, 0.f));

	m_pTransformCom->SetUp_Speed(5.0f, D3DXToRadian(90.0f));


	return NOERROR;
}

_int CUI::Update_GameObject(const _float & fTimeDelta)
{
	return _int();
}

_int CUI::LastUpdate_GameObject(const _float & fTimeDelta)
{
	if (nullptr == m_pRendererCom)
		return -1;

	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_UI, this)))
		return -1;

	return _int();
}

void CUI::Render_GameObject()
{
	m_pGraphic_Device->SetRenderState(D3DRS_ZENABLE, FALSE);

	if (FAILED(m_pTextureCom->SetUp_OnGraphicDev(0)))
		return;

	m_pBufferCom->Render_VIBuffer(&m_pTransformCom->Get_Matrix());

	m_pGraphic_Device->SetRenderState(D3DRS_ZENABLE, TRUE);
}

HRESULT CUI::Ready_Component()
{
	CManagement*		pManagement = CManagement::GetInstance();
	if (nullptr == pManagement)
		return E_FAIL;
	pManagement->AddRef();

	// For.Com_Transform
	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;

	// For.Com_Renderer
	m_pRendererCom = (CRenderer*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Renderer");
	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
		return E_FAIL;

	//For.Com_Buffer
	m_pBufferCom = (CBuffer_ViewPort*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Buffer_ViewPort");
	if (FAILED(Add_Component(L"Com_Buffer", m_pBufferCom)))
		return E_FAIL;

	//For.Com_Texture
	switch (m_byDrawID)
	{
	case 1:
		m_pTextureCom = (CTexture*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Texture_UI");
		if (FAILED(Add_Component(L"Com_Texture", m_pTextureCom)))
			return E_FAIL;
		break;
	case 2:
		m_pTextureCom = (CTexture*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Texture_Score");
		if (FAILED(Add_Component(L"Com_Texture", m_pTextureCom)))
			return E_FAIL;
		break;
	default:
		break;
	}


	Safe_Release(pManagement);


	return NOERROR;
}

CUI * CUI::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CUI* pInstance = new CUI(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		_MSG_BOX("UI Create Failed")
		Safe_Release(pInstance);
	}

	return pInstance;
}

CGameObject * CUI::Clone_GameObject()
{
	this->AddClone();

	CUI*	pInstance = new CUI(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		_MSG_BOX("UI_Clone Create Failed");
		Safe_Release(pInstance);
	}
	return pInstance;
}

void CUI::Free()
{
	Safe_Release(m_pTextureCom);
	Safe_Release(m_pBufferCom);
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pTransformCom);

	CGameObject::Free();
}

#pragma once
#include "VIBuffer.h"

_BEGIN(Client)

class CBuffer_Player final : public CVIBuffer
{
private:
	enum {R,U,L,END};
private:
	explicit CBuffer_Player(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CBuffer_Player(const CBuffer_Player& rhs);
	virtual ~CBuffer_Player() = default;
public:
	HRESULT Ready_VIBuffer();
	void Render_VIBuffer();
public:
	static CBuffer_Player* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CComponent* Clone_Component();

private:
	virtual void Free();

private:
	void UpdateVertex();

	void UpdateVertex1();
	void UpdateVertex2();

	void Ready_Index();

	void calNext();
	//_vec3 nextPlaneRight();
	
public:
	void SetUp_BufferPlayer(PLANEINFO* _pPlane, Direction& _touchDir, Direction& _climbDir, _float& _offX,_float& _offY);
private:
	PLANEINFO* m_pPlane = nullptr;

	Direction touchDir = DIR_END;
	Direction climbDir = DIR_END;

	_int dir[DIR_END];
	

	_float offX=0.f;
	_float offY=0.f;



};

_END

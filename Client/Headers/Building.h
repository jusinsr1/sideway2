#pragma once
#include "GameObject.h"

_BEGIN(Engine)
class CTransform;
class CRenderer;
class CTexture;
class CBuffer_CubeLine;
_END

_BEGIN(Client)
class CBuilding final : public CGameObject
{
private:
	explicit CBuilding(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CBuilding(const CBuilding& rhs);
	virtual ~CBuilding() = default;

public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& fTImeDelta);
	virtual _int LastUpdate_GameObject(const _float& fTImeDelta);
	virtual void Render_GameObject();

	void SetData(_vec3 vecScale, _vec3 vecPos);

public:
	static CBuilding* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject* Clone_GameObject();
private:
	HRESULT Ready_Component();

private:
	virtual void Free();

private:
	CTransform*			m_pTransformCom = nullptr;
	CRenderer*			m_pRendererCom = nullptr;
	CBuffer_CubeLine*	m_pBufferCom = nullptr;
	CTexture*			m_pTextureCom = nullptr;
};

_END
#pragma once

#include "Game_Defines.h"
#include "Base.h"
#include "Graphic_Device.h"
#include "Renderer.h"
#include "Collision.h"

_BEGIN(Engine)
class CManagement;
_END


_BEGIN(Client)

class CMainApp final : public CBase
{
public:
	explicit CMainApp();
	virtual ~CMainApp() = default;
public:
	HRESULT Ready_MainApp();
	_int Update_MainApp(const _float& fTimeDelta);
	void Render_MainApp();
	void Coll_MainApp();
private:
	LPDIRECT3DDEVICE9	m_pGraphic_Device = nullptr;
	CManagement*		m_pManagement = nullptr;
	CRenderer*			m_pRenderer = nullptr;
	CCollision*			m_pColl = nullptr;
private:
	_tchar	m_szFPS[MAX_PATH] = L"";
	_ulong	m_dwRenderCnt = 0;
	_float	m_fTimeAcc = 0.f;
private:
	HRESULT Ready_Default_Setting(CGraphic_Device::WINMODE eMode, const _uint& iSizeX, const _uint& iSizeY);
	HRESULT Ready_Prototype_Component();
	HRESULT Ready_Prototype_GameObject();
	HRESULT Ready_Start_Scene(SCENEID eSceneID);
	HRESULT Ready_Render_State();
	HRESULT Ready_Sampler_State();
public:
	static CMainApp* Create();
protected:
	virtual void Free();
};

_END

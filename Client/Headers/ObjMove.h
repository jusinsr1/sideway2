#pragma once
#include "Component.h"

_BEGIN(Engine)
class CTransform;
_END

_BEGIN(Client)

class _ENGINE_DLL CObjMove final : public CComponent
{
private:
	explicit CObjMove(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CObjMove(const CObjMove& rhs);
	virtual ~CObjMove() = default;

public:
	HRESULT Ready_Obj();

	void Setup_Obj(PLANEINFO* _pPlane, CTransform* pTrans);
	void Update();



	Direction TouchDir(_float& offX, _float& offY);
	Direction TouchDirInRoof(_float& offX, _float& offY);

	
	_bool CheckIn(_int& _curPlaneOut, _int& _prePlaneOut);
	void Get_ClimbDir(Direction & _climbDirOut);
	void Get_TouchDir(Direction& _touchDirOut,_float& offXOut, _float& offYOut);
private:
	void CalRUL();
	void CalPosWorld();
	void CalPosInPlane();


public:
	static CObjMove* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CComponent* Clone_Component();
private:
	virtual void Free() override;

private:
	PLANEINFO*		pPlane = nullptr;
	CTransform*		m_pTransform=nullptr;
	
	_int			iPreDir = 0;
	_int			iCurDir = 0;

	Direction		climbDir = Direction(0);

	_vec2			vPlayerSize = {};
	_vec2			vPosInPlane = {};
};

_END
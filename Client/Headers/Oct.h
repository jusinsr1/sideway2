#pragma once

#include "GameObject.h"

_BEGIN(Engine)
class CTransform;
class CRenderer;
class CTexture;
class CBuffer_RcTex;
_END

_BEGIN(Client)

class COct final : public CGameObject
{
private:
	explicit COct(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit COct(const COct& rhs);
	virtual ~COct() = default;
public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& fTimeDelta);
	virtual _int LastUpdate_GameObject(const _float& fTimeDelta);
	virtual void Render_GameObject();
public:
	void	AddClone() { m_byDrawID++; }
private:
	CTransform*			m_pTransformCom = nullptr;
	CRenderer*			m_pRendererCom = nullptr;
	CTexture*			m_pTextureCom = nullptr;
	CBuffer_RcTex*		m_pBufferCom = nullptr;
protected:
	_byte				m_byDrawID = 0 , m_byKey = 0;
	_float				m_fTime = 0.f;
	_uint				m_iTexIdx = 0;
private:
	HRESULT Ready_Component(); // 이 객체안에서 사용할 컴포넌트들을 원형객체로부터 복사(공유) 해서 온다.
public:
	static COct* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject* Clone_GameObject() override;
protected:
	virtual void Free();

};

_END
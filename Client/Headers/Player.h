#pragma once
#include "GameObject.h"
#include "Input_Device.h"

_BEGIN(Engine)
class CTransform;
class CRenderer;
class CTexture;
class CCollision;
_END

_BEGIN(Client)
class CBuffer_Player;
class CObjMove;
class CPlayer final : public CGameObject
{
public:
	enum PLAYER { PLAYER_IDLE_L, PLAYER_IDLE_R, PLAYER_RUN_L, PLAYER_RUN_R, PLAYER_END };

private:
	explicit CPlayer(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CPlayer(const CPlayer& rhs);
	virtual ~CPlayer() = default;
public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& fTImeDelta);
	virtual _int LastUpdate_GameObject(const _float& fTImeDelta);
	virtual void Render_GameObject();
	virtual void Coll_GameObject();
public:
	static CPlayer* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject* Clone_GameObject();
private:
	HRESULT Ready_Component();

private:
	void KeyInput(const _float & fTImeDelta);
	void AsGravity(const _float & fTImeDelta);
	void moveCheck();
private:
	virtual void Free();
private:
	_float	m_fTime = 0.f;	_uint	m_iTexIdx = 0;
	_float	m_fSeta = 0.f;
	PLAYER  m_eCurState = PLAYER_IDLE_R;
	PLAYER  m_ePreState = PLAYER_END;

private:
	CInput_Device* m_pInput_Device = nullptr;
	CRenderer*		m_pRendererCom = nullptr;
	CCollision*		m_pCollCom = nullptr;
	CTransform*		m_pTransformCom = nullptr;
	CObjMove*		m_pObjMove = nullptr;
	CBuffer_Player*	m_pBufferCom = nullptr;

private:
	CTexture*		m_pTextureCom[PLAYER_END] = {};

private:

	PLANEINFO plane[15];
	_int curPlane = 0;
	_int prePlane = 0;

	Direction climbDir = Direction(0);//옥상으로 올라올때  올라온 면 판정때문에 필요
	Direction touchDir = Direction(0);

	_float offX = 0.f;
	_float offY = 0.f;

	_bool bCheck = false;



	_vec2 vSpd = {0.f,0.f};
};

_END
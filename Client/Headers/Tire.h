#pragma once
#include "GameObject.h"

_BEGIN(Engine)
class CTransform;
class CRenderer;
class CCollision;
_END

_BEGIN(Client)
class CBuffer_Tire;
class CTire final : public CGameObject
{
private:
	explicit CTire(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CTire(const CTire& rhs);
	virtual ~CTire() = default;

public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& fTImeDelta);
	virtual _int LastUpdate_GameObject(const _float& fTImeDelta);
	virtual void Render_GameObject();

	virtual void Coll_GameObject();

private:
	void bounce(const _float& fTImeDelta);
public:
	static CTire* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject* Clone_GameObject();
private:
	HRESULT Ready_Component();
private:
	virtual void Free();


private:
	CRenderer*		m_pRendererCom = nullptr;
	CTransform*		m_pTransformCom = nullptr;
	CBuffer_Tire*	m_pBufferCom = nullptr;
	CCollision*		m_pCollCom = nullptr;

	_float	fX = 0.f;
	_float	fY = 0.f;

	_float fAngle = 0.f;
//	_float	m_fTimeAcc = 0.f;
	_bool	m_bTime = false;
};

_END
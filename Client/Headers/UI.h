#pragma once
#include "GameObject.h"


_BEGIN(Engine)
	class CBuffer_ViewPort;
	class CTransform;
	class CRenderer;
	class CTexture;
_END


_BEGIN(Client)

class CUI :
	public CGameObject
{


private:
	explicit CUI(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CUI(const CUI& rhs);
	virtual ~CUI() = default;
public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& fTimeDelta);
	virtual _int LastUpdate_GameObject(const _float& fTimeDelta);
	virtual void Render_GameObject();

public:
	void	AddClone() { 
		m_byDrawID++; }
private:
	CBuffer_ViewPort*	m_pBufferCom = nullptr;
	CTransform*			m_pTransformCom = nullptr;
	CRenderer*			m_pRendererCom = nullptr;
	CTexture*			m_pTextureCom = nullptr;
	_float				m_fX = 0.f , m_fY = 0.f;
	_float				m_fSizeX = 0.f , m_fSizeY = 0.f;
	_byte				m_byDrawID = 0;
private:
	HRESULT Ready_Component();
public:
	static CUI* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject* Clone_GameObject();
protected:
	virtual void Free();
};

_END


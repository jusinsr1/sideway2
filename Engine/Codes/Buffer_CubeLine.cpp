#include "..\Headers\Buffer_CubeLine.h"
#include "Transform.h"

CBuffer_CubeLine::CBuffer_CubeLine(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CVIBuffer(pGraphic_Device)
{

}

CBuffer_CubeLine::CBuffer_CubeLine(const CBuffer_CubeLine & rhs)
	: CVIBuffer(rhs)
{

}

HRESULT CBuffer_CubeLine::Ready_VIBuffer()
{
	m_iNumVertices = 8;
	m_iStride = sizeof(VTXNMCOL);
	m_dwFVF = D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_DIFFUSE;
	

	m_iNumPolygons = 12;
	m_iPolygonSize = sizeof(POLYGON16);
	m_eFormat = D3DFMT_INDEX16;

	m_pIndex = new POLYGON16[m_iNumPolygons];

	if (FAILED(CVIBuffer::Ready_VIBuffer()))
		return E_FAIL;

	VTXNMCOL*		pVertices = nullptr;

	m_pVB->Lock(0, 0, (void**)&pVertices, 0);

	pVertices[0].vPosition = _vec3(-0.5f, 0.5f, -0.5f);
	pVertices[1].vPosition = _vec3(0.5f, 0.5f, -0.5f);
	pVertices[2].vPosition = _vec3(0.5f, -0.5f, -0.5f);
	pVertices[3].vPosition = _vec3(-0.5f, -0.5f, -0.5f);
	pVertices[4].vPosition = _vec3(-0.5f, 0.5f, 0.5f);
	pVertices[5].vPosition = _vec3(0.5f, 0.5f, 0.5f);
	pVertices[6].vPosition = _vec3(0.5f, -0.5f, 0.5f);
	pVertices[7].vPosition = _vec3(-0.5f, -0.5f, 0.5f);


	for (size_t i = 0; i < m_iNumVertices; ++i)
	{
		pVertices[i].vNormal = _vec3(0.f, 0.f, 0.f);
		//pVertices[i].dwColor = D3DXCOLOR(0.835f, 0.835f, 0.835f, 1.f);
		pVertices[i].dwColor = D3DXCOLOR(1.f, 0.f, 0.f, 1.f);
	}

	m_pVB->Unlock();




	POLYGON16*		pIndices = nullptr;

	m_pIB->Lock(0, 0, (void**)&pIndices, 0);

	// +x
	pIndices[0]._0 = 1; pIndices[0]._1 = 5; pIndices[0]._2 = 6;
	pIndices[1]._0 = 1; pIndices[1]._1 = 6; pIndices[1]._2 = 2;

	// -x
	pIndices[2]._0 = 4; pIndices[2]._1 = 0; pIndices[2]._2 = 3;
	pIndices[3]._0 = 4; pIndices[3]._1 = 3; pIndices[3]._2 = 7;

	// +y
	pIndices[4]._0 = 4; pIndices[4]._1 = 5; pIndices[4]._2 = 1;
	pIndices[5]._0 = 4; pIndices[5]._1 = 1; pIndices[5]._2 = 0;

	// -y
	pIndices[6]._0 = 3; pIndices[6]._1 = 2; pIndices[6]._2 = 6;
	pIndices[7]._0 = 3; pIndices[7]._1 = 6; pIndices[7]._2 = 7;

	// +z
	pIndices[8]._0 = 7; pIndices[8]._1 = 6; pIndices[8]._2 = 5;
	pIndices[9]._0 = 7; pIndices[9]._1 = 5; pIndices[9]._2 = 4;

	// -z
	pIndices[10]._0 = 0; pIndices[10]._1 = 1; pIndices[10]._2 = 2;
	pIndices[11]._0 = 0; pIndices[11]._1 = 2; pIndices[11]._2 = 3;
	//////////////////////////////////////////////////////////////////////////////////
	// +x
	m_pIndex[0]._0 = 1; m_pIndex[0]._1 = 5; m_pIndex[0]._2 = 6;
	m_pIndex[1]._0 = 1; m_pIndex[1]._1 = 6; m_pIndex[1]._2 = 2;

	// -x
	m_pIndex[2]._0 = 4; m_pIndex[2]._1 = 0; m_pIndex[2]._2 = 3;
	m_pIndex[3]._0 = 4; m_pIndex[3]._1 = 3; m_pIndex[3]._2 = 7;

	// +y
	m_pIndex[4]._0 = 4; m_pIndex[4]._1 = 5; m_pIndex[4]._2 = 1;
	m_pIndex[5]._0 = 4; m_pIndex[5]._1 = 1; m_pIndex[5]._2 = 0;

	// -y
	m_pIndex[6]._0 = 3; m_pIndex[6]._1 = 2; m_pIndex[6]._2 = 6;
	m_pIndex[7]._0 = 3; m_pIndex[7]._1 = 6; m_pIndex[7]._2 = 7;

	// +z
	m_pIndex[8]._0 = 7; m_pIndex[8]._1 = 6; m_pIndex[8]._2 = 5;
	m_pIndex[9]._0 = 7; m_pIndex[9]._1 = 5; m_pIndex[9]._2 = 4;

	// -z
	m_pIndex[10]._0 = 0; m_pIndex[10]._1 = 1; m_pIndex[10]._2 = 2;
	m_pIndex[11]._0 = 0; m_pIndex[11]._1 = 2; m_pIndex[11]._2 = 3;
	/////////////////////////////////////////////////////////////////////////////

	//����
	/*pIndices[12]._0 = 0; pIndices[12]._1 = 1; pIndices[12]._2 = 2;
	pIndices[13]._0 = 3; pIndices[13]._1 = 7; pIndices[13]._2 = 4;
	pIndices[14]._0 = 5; pIndices[14]._1 = 6; pIndices[14]._2 = 7;
	pIndices[15]._0 = 0; pIndices[15]._1 = 4; pIndices[15]._2 = 1;
	pIndices[16]._0 = 5; pIndices[16]._1 = 2; pIndices[16]._2 = 6;*/
	////////////////////////////////////////////////////////////


	m_pIB->Unlock();

	UpdateNormal();


	return NOERROR;
}

void CBuffer_CubeLine::Render_VIBuffer()
{
	m_pGraphic_Device->SetStreamSource(0, m_pVB, 0, m_iStride);
	m_pGraphic_Device->SetFVF(m_dwFVF);
	m_pGraphic_Device->SetIndices(m_pIB);
	m_pGraphic_Device->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, m_iNumVertices, 0, m_iNumPolygons);
//	m_pGraphic_Device->DrawIndexedPrimitive(D3DPT_LINESTRIP, 36, 36, m_iNumVertices, 0, m_iNumPolygons);
}




CBuffer_CubeLine * CBuffer_CubeLine::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CBuffer_CubeLine*	pInstance = new CBuffer_CubeLine(pGraphic_Device);

	if (FAILED(pInstance->Ready_VIBuffer()))
	{
		MessageBox(0, L"CBuffer_CubeTex Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;

}


CComponent * CBuffer_CubeLine::Clone_Component()
{
	return new CBuffer_CubeLine(*this);
}

void CBuffer_CubeLine::Free()
{


	CVIBuffer::Free();
}

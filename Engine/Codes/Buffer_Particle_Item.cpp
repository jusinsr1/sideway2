#include "Buffer_Particle_Item.h"



CBuffer_Particle_Item::CBuffer_Particle_Item(LPDIRECT3DDEVICE9 pGraphic_Device)
	:CBuffer_Particle(pGraphic_Device)
{
}

CBuffer_Particle_Item::CBuffer_Particle_Item(const CBuffer_Particle_Item & rhs)
	:CBuffer_Particle(rhs)
{
}

HRESULT CBuffer_Particle_Item::Ready_ParticleItem(const _uint iNumParticle)
{
	if (FAILED(CVtxBuffer::Ready_CVtxBuffer(128)))
		return E_FAIL;

	m_fSize = 1.f;
	m_dwOffset = 0;
	m_dwBatchSize = 64;

	for (size_t i = 0; i < iNumParticle; i++)
		CBuffer_Particle::Add_Particle();


	return NOERROR;
}

HRESULT CBuffer_Particle_Item::SetUp_Particle(ATTIBUTE* pBute)
{

		pBute->bIsLive = true;

		int iTempNum[3] = {};
	
		//for (size_t i = 0; i < 3; i++)
		//{
		//	random_device random;
		//	mt19937 engine(random());
		//	uniform_int_distribution<int> distribution(0, 50);

		//	iTempNum[i] = distribution(engine);
		//}
		//
		// SetUp Bute
		
		// pBute->vPosition = {(_float)iTempNum[0],(_float)iTempNum[1],(_float)iTempNum[2] };

		pBute->vPosition = {0.f,0.f,0.f};
		pBute->fTimeLife = 3.f;
		pBute->dwColor = D3DXCOLOR(1.f,1.f,1.f,1.f);
		
		pBute->vVelocity = { (float)iTempNum[0],(float)iTempNum[1],-5.f};
		
	return NOERROR;
}

_int CBuffer_Particle_Item::Update(const _float & fTimeDelta)
{
	m_fSize += 0.01f;

	for (auto& pBute : m_Particle_Lst)
		pBute.fAge += fTimeDelta;

	for (auto& pBute : m_Particle_Lst)
	{
		pBute.vPosition += pBute.vVelocity * fTimeDelta;
		if (pBute.fAge >= pBute.fTimeLife)
			pBute.bIsLive = false;
	}

	CBuffer_Particle::Release_Dead_Particle();

	return _int();
}

void CBuffer_Particle_Item::Render_Particle(LPDIRECT3DTEXTURE9 pTexture)
{
	CBuffer_Particle::Render_Particle(pTexture);
}

CBuffer_Particle_Item * CBuffer_Particle_Item::Create(LPDIRECT3DDEVICE9 pGraphic_Device, const _uint iNumParticle)
{
	CBuffer_Particle_Item* pInstance = new CBuffer_Particle_Item(pGraphic_Device);

	if (FAILED(pInstance->Ready_ParticleItem(iNumParticle)))
		Safe_Release(pInstance);
	
	return pInstance;
}

CComponent * CBuffer_Particle_Item::Clone_Component()
{
	return new CBuffer_Particle_Item(*this);
}

void CBuffer_Particle_Item::Free()
{
	CBuffer_Particle::Free();
}

#include "..\Headers\Buffer_Terrain.h"

CBuffer_Terrain::CBuffer_Terrain(LPDIRECT3DDEVICE9 pGraphic_Device)
	:CVIBuffer(pGraphic_Device)
{
}

CBuffer_Terrain::CBuffer_Terrain(const CBuffer_Terrain & rhs)
	:CVIBuffer(rhs)
{
}

HRESULT CBuffer_Terrain::Ready_VIBuffer(const _uint & iNumVerticesX, const _uint & iNumVerticesZ, const _float & fInterval)
{
	m_iNumVerticesX = iNumVerticesX;
	m_iNumVerticesZ = iNumVerticesZ;
	m_fInterval = fInterval;

	m_iNumVertices = iNumVerticesX * iNumVerticesZ;
	m_iStride = sizeof(VTXTEX);
	m_dwFVF = D3DFVF_XYZ | D3DFVF_TEX1;
	m_iNumPolygons = (iNumVerticesX - 1) * (iNumVerticesZ - 1) * 2;

	m_iPolygonSize = sizeof(POLYGON32);
	m_eFormat = D3DFMT_INDEX32;

	if (FAILED(CVIBuffer::Ready_VIBuffer()))
		return E_FAIL;

	VTXTEX*		pVertices = nullptr;

	m_pVB->Lock(0, 0, (void**)&pVertices, 0);

	for (size_t i = 0; i < iNumVerticesZ; i++)
	{
		for (size_t j = 0; j < iNumVerticesX; j++)
		{
			_uint		iIndex = i * iNumVerticesX + j;

			pVertices[iIndex].vPosition = _vec3(j * m_fInterval, 0.0f, i * m_fInterval);
			pVertices[iIndex].vTexUV = _vec2(j / (iNumVerticesX - 1.f) * 30.f, i / (iNumVerticesZ - 1.f) * 30.f);
		}
	}

	m_pVB->Unlock();


	POLYGON32*		pIndices = nullptr;

	m_pIB->Lock(0, 0, (void**)&pIndices, 0);

	_uint			iPolygonIndex = 0;

	for (size_t i = 0; i < iNumVerticesZ - 1; i++)
	{
		for (size_t j = 0; j < iNumVerticesX - 1; j++)
		{
			size_t iIndex = i * iNumVerticesX + j;

			// ��.��
			pIndices[iPolygonIndex]._0 = iIndex + iNumVerticesX;
			pIndices[iPolygonIndex]._1 = iIndex + iNumVerticesX + 1;
			pIndices[iPolygonIndex]._2 = iIndex + 1;
			++iPolygonIndex;

			// ��.��
			pIndices[iPolygonIndex]._0 = iIndex + iNumVerticesX;
			pIndices[iPolygonIndex]._1 = iIndex + 1;
			pIndices[iPolygonIndex]._2 = iIndex;
			++iPolygonIndex;
		}
	}


	m_pIB->Unlock();


	return NOERROR;
}

void CBuffer_Terrain::Render_VIBuffer()
{
	m_pGraphic_Device->SetStreamSource(0, m_pVB, 0, m_iStride);
	m_pGraphic_Device->SetFVF(m_dwFVF);
	m_pGraphic_Device->SetIndices(m_pIB);
	m_pGraphic_Device->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, m_iNumVertices, 0, m_iNumPolygons);
}

CBuffer_Terrain * CBuffer_Terrain::Create(LPDIRECT3DDEVICE9 pGraphic_Device, const _uint iNumVerticesX, const _uint iNumVerticesZ, const _float & fInterval)
{
	CBuffer_Terrain* pInstance = new CBuffer_Terrain(pGraphic_Device);
	if (FAILED(pInstance->Ready_VIBuffer(iNumVerticesX, iNumVerticesZ,fInterval)))
	{
		_MSG_BOX("CBuffer_Terrain Created Failed")
		Safe_Release(pInstance);
	}
	return pInstance;
}

CComponent * CBuffer_Terrain::Clone_Component()
{
	return new CBuffer_Terrain(*this);
}

void CBuffer_Terrain::Free()
{
	CVIBuffer::Free();
}
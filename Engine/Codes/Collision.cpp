#include "..\Headers\Collision.h"
#include "GameObject.h"
#include "Management.h"
#include "Transform.h"

CCollision::CCollision(LPDIRECT3DDEVICE9 pGraphic_Device)
	:CComponent(pGraphic_Device)
{
}

HRESULT CCollision::Add_CollGroup(COLLGROUP eGroup, CGameObject * pGameObject)
{
	if (COLL_END <= eGroup)
		return E_FAIL;

	if (pGameObject == nullptr)
		return E_FAIL;

	m_CollList[eGroup].push_back(pGameObject);


	return NOERROR;
}

HRESULT CCollision::Coll_CollGroup()
{
	Coll_Player_Tire();

	return NOERROR;
}

void CCollision::Coll_Player_Tire()
{
	if (m_CollList[COLL_PLAYER].size() == 0)
 		return;

	CGameObject* player = m_CollList[COLL_PLAYER].front();
	CTransform* tPlayer = (CTransform*)player->Find_Component(L"Com_Transform");

	for (CGameObject* tire : m_CollList[COLL_TIRE])
	{
		CTransform* tTire =(CTransform*)tire->Find_Component(L"Com_Transform");

		if (Rect_Cube(tPlayer, tTire))
		{
			player->Coll_GameObject();
			tire->Coll_GameObject();

		}

	}
}


_bool CCollision::Rect_Cube(CTransform * rect, CTransform * cube)
{
	_vec3 rectPos = *rect->Get_StateInfo(CTransform::STATE_POSITION);
	_vec3 cubePos = *cube->Get_StateInfo(CTransform::STATE_POSITION);
	
	RC rc1,rc2;


	const _vec3* vLook= rect->Get_StateInfo(CTransform::STATE_LOOK);
	_vec3 vLookNormal;

	D3DXVec3Normalize(&vLookNormal, vLook);

	if (fabsf(vLookNormal.x-1.f)<= FLT_EPSILON) //���� xȤ�� -x����
	{
		if ((rectPos.x + 0.01f< cubePos.x - cube->Get_Width()*0.5f) ||
			(rectPos.x - 0.01f > cubePos.x + cube->Get_Width()*0.5f))
			return false;


		rc1.left = rectPos.z - rect->Get_Width()*0.5f;
		rc1.right = rectPos.z + rect->Get_Width()*0.5f;
		rc1.top = rectPos.y + rect->Get_Height()*0.5f;
		rc1.bot = rectPos.y - rect->Get_Height()*0.5f;
		
		rc2.left = cubePos.z - cube->Get_Width()*0.5f;
		rc2.right = cubePos.z + cube->Get_Width()*0.5f;
		rc2.top = cubePos.y + cube->Get_Height()*0.5f;
		rc2.bot = cubePos.y - cube->Get_Height()*0.5f;

		
	}
	else if (fabsf(vLookNormal.y - 1.f) <= FLT_EPSILON) //���� yȤ�� -y����
	{ 
		if ((rectPos.y + 0.01f< cubePos.y - cube->Get_Height()*0.5f) ||
			(rectPos.y - 0.01f> cubePos.y + cube->Get_Height()*0.5f))
			return false;

		rc1.left = rectPos.x - rect->Get_Width()*0.5f;
		rc1.right = rectPos.x + rect->Get_Width()*0.5f;
		rc1.top = rectPos.z + rect->Get_Height()*0.5f;
		rc1.bot = rectPos.z - rect->Get_Height()*0.5f;

		rc2.left = cubePos.x - cube->Get_Width()*0.5f;
		rc2.right = cubePos.x + cube->Get_Width()*0.5f;
		rc2.top = cubePos.z + cube->Get_Height()*0.5f;
		rc2.bot = cubePos.z - cube->Get_Height()*0.5f;
	}
	else if (fabsf(vLookNormal.z - 1.f) <= FLT_EPSILON) //���� zȤ�� -z����
	{
		if ((rectPos.z +0.01f< cubePos.z - cube->Get_Width()*0.5f) ||
			(rectPos.z - 0.01f> cubePos.z + cube->Get_Width()*0.5f))
			return false;

		rc1.left = rectPos.x - rect->Get_Width()*0.5f;
		rc1.right = rectPos.x + rect->Get_Width()*0.5f;
		rc1.top = rectPos.y + rect->Get_Height()*0.5f;
		rc1.bot = rectPos.y - rect->Get_Height()*0.5f;
	
		rc2.left = cubePos.x - cube->Get_Width()*0.5f;
		rc2.right = cubePos.x + cube->Get_Width()*0.5f;
		rc2.top = cubePos.y + cube->Get_Height()*0.5f;
		rc2.bot = cubePos.y  - cube->Get_Height()*0.5f;
	}
	else
	{
		return false;
	}

 	if (Coll_Rect(rc1, rc2))
	{
		return true;
	}
	
 	return false;
}



HRESULT CCollision::Ready_Collision()
{
	return NOERROR;
}







_bool CCollision::Coll_Rect(const RC & rc1, const RC & rc2)
{
	if ((rc1.right >= rc2.left) && (rc1.left <= rc2.right))
	{
		if ((rc1.bot <= rc2.top) && (rc1.top >= rc2.bot))
		{
			return true;
		}
	}
	return false;
}

CCollision * CCollision::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CCollision* pInstance = new CCollision(pGraphic_Device);
	if (FAILED(pInstance->Ready_Collision()))
	{
		_MSG_BOX("CCollision Created Failed")
			Safe_Release(pInstance);
	}
	return pInstance;
}

CComponent * CCollision::Clone_Component()
{
	AddRef();
	return this;
}

void CCollision::Free()
{
	CComponent::Free();
}

#include "..\Headers\Input_Device.h"

_IMPLEMENT_SINGLETON(CInput_Device)

CInput_Device::CInput_Device()
{
}

HRESULT CInput_Device::Ready_Input_Device(HINSTANCE hInst, HWND hWnd)
{
	if (FAILED(DirectInput8Create(hInst, DIRECTINPUT_VERSION, IID_IDirectInput8, (void**)&m_pSDK, nullptr)))
		return E_FAIL;

	if (FAILED(Ready_KeyBoard(hWnd)))
		return E_FAIL;

	if (FAILED(Ready_Mouse(hWnd)))
		return E_FAIL;

	return NOERROR;
}

void CInput_Device::SetUp_InputState()
{
	if (nullptr == m_pKeyBoard)
		return;
	if (nullptr == m_pMouse)
		return;

	memcpy_s(m_PreKeyState, sizeof(_byte) * 256, m_KeyState, sizeof(_byte) * 256);
	m_PreMouseState = m_MouseState;
	m_pKeyBoard->GetDeviceState(256, m_KeyState);
	m_pMouse->GetDeviceState(sizeof(DIMOUSESTATE), &m_MouseState);
}

_bool CInput_Device::IsKeyPressing(_ubyte byKeyID)
{
	if (Get_DIKeyState(byKeyID) & 0x80)
		return true;
	return false;
}

_bool CInput_Device::IsKeyDown(_ubyte byKeyID)
{
	if ((Get_DIKeyState(byKeyID) & 0x80) && !(Get_DIPreKeyState(byKeyID) & 0x80))
		return true;
	return false;
}

_bool CInput_Device::IsKeyUp(_ubyte byKeyID)
{
	if ((Get_DIPreKeyState(byKeyID) & 0x80) && !(Get_DIKeyState(byKeyID) & 0x80))
		return true;
	return false;
}

HRESULT CInput_Device::Ready_KeyBoard(HWND hWnd)
{
	if (FAILED(m_pSDK->CreateDevice(GUID_SysKeyboard, &m_pKeyBoard, nullptr)))
		return E_FAIL;

	if (FAILED(m_pKeyBoard->SetCooperativeLevel(hWnd, DISCL_FOREGROUND | DISCL_NONEXCLUSIVE | DISCL_NOWINKEY)))
		return E_FAIL;

	if (FAILED(m_pKeyBoard->SetDataFormat(&c_dfDIKeyboard)))
		return E_FAIL;

	m_pKeyBoard->Acquire();

	return NOERROR;
}

HRESULT CInput_Device::Ready_Mouse(HWND hWnd)
{
	if (FAILED(m_pSDK->CreateDevice(GUID_SysMouse, &m_pMouse, nullptr)))
		return E_FAIL;

	if (FAILED(m_pMouse->SetCooperativeLevel(hWnd, DISCL_FOREGROUND | DISCL_NONEXCLUSIVE)))
		return E_FAIL;

	if (FAILED(m_pMouse->SetDataFormat(&c_dfDIMouse)))
		return E_FAIL;

	m_pMouse->Acquire();

	return NOERROR;
}

void CInput_Device::Free()
{
	Safe_Release(m_pKeyBoard);
	Safe_Release(m_pMouse);

	Safe_Release(m_pSDK);
}

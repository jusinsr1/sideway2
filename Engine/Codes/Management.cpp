#include "..\Headers\Management.h"
#include "Scene.h"
#include "System.h"
#include "Timer_Manager.h"
#include "Frame_Manager.h"
#include "Graphic_Device.h"
#include "Input_Device.h"
#include "Object_Manager.h"
#include "Component_Manager.h"

_IMPLEMENT_SINGLETON(CManagement)

CManagement::CManagement()
	:m_pObject_Manager(GET_INSTANCE(CObject_Manager))
	,m_pComponent_Manager(GET_INSTANCE(CComponent_Manager))
{
	m_pObject_Manager->AddRef();
	m_pComponent_Manager->AddRef();
}
CComponent * CManagement::Get_ComponentPointer(const _uint & iSceneID, const _tchar * pLayerTag, const _tchar * pComponentTag, const _uint & iIndex)
{
	if (nullptr == m_pObject_Manager)
		return nullptr;

	return m_pObject_Manager->Get_ComponentPointer(iSceneID, pLayerTag, pComponentTag, iIndex);
}

HRESULT CManagement::Ready_Management(const _uint & iNumScene)
{
	if (m_pObject_Manager == nullptr||m_pComponent_Manager == nullptr)
		return E_FAIL;
	if (FAILED(m_pObject_Manager -> Reserve_Object_Manager(iNumScene)))
		return E_FAIL;
	if (FAILED(m_pComponent_Manager->Reserve_Component_Manager(iNumScene)))
		return E_FAIL;
	return NOERROR;
}

HRESULT CManagement::Add_Prototype_GameObject(const _tchar * pGameObjectTag, CGameObject * pGameObject)
{
	if (m_pObject_Manager == nullptr)
		return E_FAIL;
 	return m_pObject_Manager ->Add_Prototype_GameObject(pGameObjectTag, pGameObject);
}

HRESULT CManagement::Add_Prototype_Component(const _uint & iSceneID, const _tchar * pComponentTag, CComponent * pComponent)
{
	if (m_pComponent_Manager == nullptr)
		return E_FAIL;
	return m_pComponent_Manager->Add_Prototype_Component(iSceneID, pComponentTag, pComponent);
}

HRESULT CManagement::Add_GameObjectToLayer(const _tchar * pProtoTag, const _uint & iSceneID, const _tchar * pLayerTag, CGameObject** ppCloneObject)
{
	if (m_pObject_Manager == nullptr)
		return E_FAIL;
	return m_pObject_Manager->Add_GameObjectToLayer(pProtoTag, iSceneID, pLayerTag, ppCloneObject);
}

HRESULT CManagement::SetUp_ScenePointer(CScene * pNewScenePointer)
{
	if (pNewScenePointer == nullptr)
		return E_FAIL;
	if (Safe_Release(m_pScene) != 0)
		return E_FAIL;

	m_pScene = pNewScenePointer;

	m_pScene->AddRef();

	return NOERROR;
}

CComponent * CManagement::Clone_Component(const _uint & iSceneID, const _tchar * pComponentTag)
{
	if (nullptr == m_pComponent_Manager)
		return nullptr;

	return m_pComponent_Manager->Clone_Component(iSceneID, pComponentTag);
}

_int CManagement::Update_Management(const _float & fTimeDelta)
{
	if (m_pScene == nullptr)
		return -1;

	_int iProcessCodes = 0;


	iProcessCodes = m_pScene->Update_Scene(fTimeDelta);
	if (iProcessCodes & 0x80000000)
		return iProcessCodes;

	iProcessCodes = m_pScene->LastUpdate_Scene(fTimeDelta);
	if (iProcessCodes & 0x80000000)
		return iProcessCodes;

	return _int();
}

void CManagement::Render_Management()
{
	if (m_pScene == nullptr)
		return;
	
	m_pScene->Render_Scene();
}

HRESULT CManagement::Clear_Layers(const _uint & iSceneID)
{
	if (m_pObject_Manager == nullptr)
		return E_FAIL;
	return m_pObject_Manager->Clear_Layers(iSceneID);
}

void CManagement::Release_Engine()
{
	_ulong		dwRefCnt = 0;

	if (dwRefCnt = GET_INSTANCE(CManagement)->DestroyInstance())
		_MSG_BOX("CManagement Release Failed");
	if (dwRefCnt = GET_INSTANCE(CObject_Manager)->DestroyInstance())
		_MSG_BOX("CObject_Manager Release Failed");
	if (dwRefCnt = GET_INSTANCE(CComponent_Manager)->DestroyInstance())
		_MSG_BOX("CComponent_Manager Release Failed");
	if (dwRefCnt = GET_INSTANCE(CSystem)->DestroyInstance())
		_MSG_BOX("CSystem Release Failed");
	if (dwRefCnt = GET_INSTANCE(CTimer_Manager)->DestroyInstance())
		_MSG_BOX("CTimer_Manager Release Failed");
	if (dwRefCnt = GET_INSTANCE(CFrame_Manager)->DestroyInstance())
		_MSG_BOX("CFrame_Manager Release Failed");
	if (dwRefCnt = GET_INSTANCE(CGraphic_Device)->DestroyInstance())
		_MSG_BOX("CGraphic_Device Release Failed");
	if (dwRefCnt = GET_INSTANCE(CInput_Device)->DestroyInstance())
		_MSG_BOX("CGraphic_Device Release Failed");

}

void CManagement::Free()
{
	Safe_Release(m_pComponent_Manager);
	Safe_Release(m_pObject_Manager);
	Safe_Release(m_pScene);
}

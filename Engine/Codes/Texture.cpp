#include "..\Headers\Texture.h"


CTexture::CTexture(LPDIRECT3DDEVICE9 pGraphic_Device)
	:CComponent(pGraphic_Device)
{
}

CTexture::CTexture(const CTexture & rhs)
	:CComponent(rhs)
	,m_vecTexture(rhs.m_vecTexture)
{
	for (auto& pTexture : m_vecTexture)
		pTexture->AddRef();
}

HRESULT CTexture::Ready_Texture(const _tchar * pFilePath, const _uint & iNumTexture, const TYPE eTypeID)
{
	m_vecTexture.reserve(iNumTexture);
	for (size_t i = 0; i < iNumTexture; i++)
	{
		IDirect3DBaseTexture9*		pTexture = nullptr;
		_tchar					szFilePath[MAX_PATH] = L"";
		wsprintf(szFilePath, pFilePath, i);

		if (TYPE_GENERAL == eTypeID)
		{
			if (FAILED(D3DXCreateTextureFromFile(m_pGraphic_Device, szFilePath, (LPDIRECT3DTEXTURE9*)&pTexture)))
				return E_FAIL;
		}
		else if (TYPE_CUBE == eTypeID)
		{
			if (FAILED(D3DXCreateCubeTextureFromFile(m_pGraphic_Device, szFilePath, (LPDIRECT3DCUBETEXTURE9*)&pTexture)))
				return E_FAIL;
		}

		m_vecTexture.push_back(pTexture);

	}

	return NOERROR;

}

HRESULT CTexture::SetUp_OnGraphicDev(const _uint & iIndex)
{
	if (m_vecTexture.size() <= iIndex)
		return E_FAIL;

	if (FAILED(m_pGraphic_Device->SetTexture(0, m_vecTexture[iIndex])))
		return E_FAIL;

	return NOERROR;
}

CTexture * CTexture::Create(LPDIRECT3DDEVICE9 pGraphic_Device, const TYPE eTypeID , const _tchar * pFilePath, const _uint & iNumTexture)
{
	CTexture* pInstance = new CTexture(pGraphic_Device);

	if (FAILED(pInstance->Ready_Texture(pFilePath, iNumTexture, eTypeID)))
	{
		_MSG_BOX("CTexture Created Failed")
		Safe_Release(pInstance);
	}
	return pInstance;
}

CComponent * CTexture::Clone_Component()
{
	return new CTexture(*this);
}

void CTexture::Free()
{
	for (auto& pTexture : m_vecTexture)
		Safe_Release(pTexture);
	m_vecTexture.clear();

	CComponent::Free();
}

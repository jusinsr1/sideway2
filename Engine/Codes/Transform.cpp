#include "..\Headers\Transform.h"


CTransform::CTransform(LPDIRECT3DDEVICE9 pGraphic_Device)
	:CComponent(pGraphic_Device)
{
}

CTransform::CTransform(const CTransform & rhs)
	:CComponent(rhs)
	,m_matWorld(rhs.m_matWorld)
{
}

const _vec3 * CTransform::Get_StateInfo(STATE eState)
{
	return (_vec3*)&m_matWorld.m[eState][0];
}

_vec3 CTransform::Get_Scale()
{
	_float	fScaleX = D3DXVec3Length(Get_StateInfo(CTransform::STATE_RIGHT));
	_float	fScaleY = D3DXVec3Length(Get_StateInfo(CTransform::STATE_UP));
	_float	fScaleZ = D3DXVec3Length(Get_StateInfo(CTransform::STATE_LOOK));

	return _vec3(fScaleX, fScaleY, fScaleZ);
}

void CTransform::Set_StateInfo(STATE eState, const _vec3* pInfo)
{
	memcpy(&m_matWorld.m[eState][0], pInfo, sizeof(_vec3));
}

HRESULT CTransform::Ready_Transform()
{
	D3DXMatrixIdentity(&m_matWorld);
	return NOERROR;
}

HRESULT CTransform::SetUp_OnGraphicDev()
{
	if (m_pGraphic_Device == nullptr)
		return E_FAIL;

	m_pGraphic_Device->SetTransform(D3DTS_WORLD, &m_matWorld);
	return NOERROR;
}

void CTransform::SetUp_Speed(const _float & fMovePerSec, const _float & fRotationPerSec)
{
	m_fSpeed_Move = fMovePerSec;
	m_fSpeed_Rotation = fRotationPerSec;
}


void CTransform::Go_Straight(const _float & fTimeDelta)
{
	_vec3 vLook, vPosition;

	vLook = *Get_StateInfo(CTransform::STATE_LOOK);
	vPosition = *Get_StateInfo(CTransform::STATE_POSITION);

	vPosition += *D3DXVec3Normalize(&vLook, &vLook) * m_fSpeed_Move * fTimeDelta;

	Set_StateInfo(CTransform::STATE_POSITION, &vPosition);
}

void CTransform::Go_Left(const _float & fTimeDelta)
{
	_vec3 vRight, vPosition;

	vRight = *Get_StateInfo(CTransform::STATE_RIGHT);
	vPosition = *Get_StateInfo(CTransform::STATE_POSITION);

	vPosition -= *D3DXVec3Normalize(&vRight, &vRight) * m_fSpeed_Move * fTimeDelta;

	Set_StateInfo(CTransform::STATE_POSITION, &vPosition);
}

void CTransform::Go_Right(const _float & fTimeDelta)
{
	_vec3 vRight, vPosition;

	vRight = *Get_StateInfo(CTransform::STATE_RIGHT);
	vPosition = *Get_StateInfo(CTransform::STATE_POSITION);

	vPosition += *D3DXVec3Normalize(&vRight, &vRight) * m_fSpeed_Move * fTimeDelta;

	Set_StateInfo(CTransform::STATE_POSITION, &vPosition);
}

void CTransform::Go_Up(const _float & fTimeDelta)
{
	_vec3 vRight, vPosition;

	vRight = *Get_StateInfo(CTransform::STATE_UP);
	vPosition = *Get_StateInfo(CTransform::STATE_POSITION);

	vPosition += *D3DXVec3Normalize(&vRight, &vRight) * m_fSpeed_Move * fTimeDelta;

	Set_StateInfo(CTransform::STATE_POSITION, &vPosition);
}

void CTransform::Go_Down(const _float & fTimeDelta)
{
	_vec3 vRight, vPosition;

	vRight = *Get_StateInfo(CTransform::STATE_UP);
	vPosition = *Get_StateInfo(CTransform::STATE_POSITION);

	vPosition -= *D3DXVec3Normalize(&vRight, &vRight) * m_fSpeed_Move * fTimeDelta;

	Set_StateInfo(CTransform::STATE_POSITION, &vPosition);
}




void CTransform::BackWard(const _float & fTimeDelta)
{
	_vec3 vLook, vPosition;

	vLook = *Get_StateInfo(CTransform::STATE_LOOK);
	vPosition = *Get_StateInfo(CTransform::STATE_POSITION);

	vPosition -= *D3DXVec3Normalize(&vLook, &vLook) * m_fSpeed_Move * fTimeDelta;

	Set_StateInfo(CTransform::STATE_POSITION, &vPosition);
}

void CTransform::SetUp_RotationX(const _float & fRadian)
{
	_vec3 vRight(1.f, 0.f, 0.f), vUp(0.f, 1.f, 0.f), vLook(0.f, 0.f, 1.f);

	_matrix	matRot;
	D3DXMatrixRotationX(&matRot, fRadian);

	vRight *= Get_Scale().x;
	vUp *= Get_Scale().y;
	vLook *= Get_Scale().z;

	D3DXVec3TransformNormal(&vRight, &vRight, &matRot);
	D3DXVec3TransformNormal(&vUp, &vUp, &matRot);
	D3DXVec3TransformNormal(&vLook, &vLook, &matRot);

	Set_StateInfo(CTransform::STATE_RIGHT, &vRight);
	Set_StateInfo(CTransform::STATE_UP, &vUp);
	Set_StateInfo(CTransform::STATE_LOOK, &vLook);
}

void CTransform::SetUp_RotationY(const _float & fRadian)
{
	_vec3 vRight(1.f, 0.f, 0.f), vUp(0.f, 1.f, 0.f), vLook(0.f, 0.f, 1.f);

	_matrix	matRot;
	D3DXMatrixRotationY(&matRot, fRadian);

	vRight *= Get_Scale().x;
	vUp *= Get_Scale().y;
	vLook *= Get_Scale().z;

	D3DXVec3TransformNormal(&vRight, &vRight, &matRot);
	D3DXVec3TransformNormal(&vUp, &vUp, &matRot);
	D3DXVec3TransformNormal(&vLook, &vLook, &matRot);

	Set_StateInfo(CTransform::STATE_RIGHT, &vRight);
	Set_StateInfo(CTransform::STATE_UP, &vUp);
	Set_StateInfo(CTransform::STATE_LOOK, &vLook);
}

void CTransform::SetUp_RotationZ(const _float & fRadian)
{
	_vec3 vRight(1.f, 0.f, 0.f), vUp(0.f, 1.f, 0.f), vLook(0.f, 0.f, 1.f);

	_matrix	matRot;
	D3DXMatrixRotationZ(&matRot, fRadian);

	vRight *= Get_Scale().x;
	vUp *= Get_Scale().y;
	vLook *= Get_Scale().z;

	D3DXVec3TransformNormal(&vRight, &vRight, &matRot);
	D3DXVec3TransformNormal(&vUp, &vUp, &matRot);
	D3DXVec3TransformNormal(&vLook, &vLook, &matRot);

	Set_StateInfo(CTransform::STATE_RIGHT, &vRight);
	Set_StateInfo(CTransform::STATE_UP, &vUp);
	Set_StateInfo(CTransform::STATE_LOOK, &vLook);
}

void CTransform::SetUp_RotationRight(const _float & fRadian)
{
	_vec3	vDir[3], vAxisRight;
	for (size_t i = 0; i < 3; i++)
		vDir[i] = *Get_StateInfo(STATE(i));

	//D3DXVec3Cross(&vAxisRight, &vDir[1], &vDir[2]);

	_matrix	matRot;
	D3DXMatrixRotationAxis(&matRot, &vDir[0], fRadian);

	for (size_t i = 0; i < 3; i++)
		D3DXVec3TransformNormal(&vDir[i], &vDir[i], &matRot);


	Set_StateInfo(CTransform::STATE_RIGHT, &vDir[STATE_RIGHT]);
	Set_StateInfo(CTransform::STATE_UP, &vDir[STATE_UP]);
	Set_StateInfo(CTransform::STATE_LOOK, &vDir[STATE_LOOK]);
}

void CTransform::Rotation_X(const _float & fTimeDelta)
{
	_vec3	vDir[3];
	for (size_t i = 0; i < 3; i++)
		vDir[i] = *Get_StateInfo(STATE(i));

	_matrix	matRot;
	D3DXMatrixRotationX(&matRot, m_fSpeed_Rotation * fTimeDelta);

	for (size_t i = 0; i < 3; i++)
		D3DXVec3TransformNormal(&vDir[i], &vDir[i], &matRot);

	Set_StateInfo(CTransform::STATE_RIGHT, &vDir[STATE_RIGHT]);
	Set_StateInfo(CTransform::STATE_UP, &vDir[STATE_UP]);
	Set_StateInfo(CTransform::STATE_LOOK, &vDir[STATE_LOOK]);
}
void CTransform::Rotation_Y(const _float & fTimeDelta)
{
	_vec3	vDir[3];
	for (size_t i = 0; i < 3; i++)
		vDir[i] = *Get_StateInfo(STATE(i));

	_matrix	matRot;
	D3DXMatrixRotationY(&matRot, m_fSpeed_Rotation * fTimeDelta);

	for (size_t i = 0; i < 3; i++)
		D3DXVec3TransformNormal(&vDir[i], &vDir[i], &matRot);

	Set_StateInfo(CTransform::STATE_RIGHT, &vDir[STATE_RIGHT]);
	Set_StateInfo(CTransform::STATE_UP, &vDir[STATE_UP]);
	Set_StateInfo(CTransform::STATE_LOOK, &vDir[STATE_LOOK]);
}

void CTransform::Rotation_Z(const _float & fTimeDelta)
{
	_vec3	vDir[3];
	for (size_t i = 0; i < 3; i++)
		vDir[i] = *Get_StateInfo(STATE(i));

	_matrix	matRot;
	D3DXMatrixRotationZ(&matRot, m_fSpeed_Rotation * fTimeDelta);

	for (size_t i = 0; i < 3; i++)
		D3DXVec3TransformNormal(&vDir[i], &vDir[i], &matRot);

	Set_StateInfo(CTransform::STATE_RIGHT, &vDir[STATE_RIGHT]);
	Set_StateInfo(CTransform::STATE_UP, &vDir[STATE_UP]);
	Set_StateInfo(CTransform::STATE_LOOK, &vDir[STATE_LOOK]);
}

void CTransform::Rotation_AxisRight(const _float & fTimeDelta)
{
	_vec3	vDir[3], vAxisRight;
	for (size_t i = 0; i < 3; i++)
		vDir[i] = *Get_StateInfo(STATE(i));

	//D3DXVec3Cross(&vAxisRight, &vDir[1], &vDir[2]);

	_matrix	matRot;
	D3DXMatrixRotationAxis(&matRot, &vDir[0], m_fSpeed_Rotation * fTimeDelta);

	for (size_t i = 0; i < 3; i++)
		D3DXVec3TransformNormal(&vDir[i], &vDir[i], &matRot);

	Set_StateInfo(CTransform::STATE_RIGHT, &vDir[STATE_RIGHT]);
	Set_StateInfo(CTransform::STATE_UP, &vDir[STATE_UP]);
	Set_StateInfo(CTransform::STATE_LOOK, &vDir[STATE_LOOK]);
}

_matrix CTransform::Get_Matrix_Inverse()
{
	_matrix	matInverse;
	D3DXMatrixInverse(&matInverse, nullptr, &m_matWorld);
	return matInverse;
}

void CTransform::Scaling(const _float & fX, const _float & fY, const _float & fZ)
{
	_vec3 vDir[3];

	for (size_t i = 0; i < 3; i++)
	{
		vDir[i] = *Get_StateInfo(STATE(i));
		D3DXVec3Normalize(&vDir[i], &vDir[i]);
	}

	vDir[STATE_RIGHT] *= fX;
	vDir[STATE_UP] *= fY;
	vDir[STATE_LOOK] *= fZ;

	Set_StateInfo(CTransform::STATE_RIGHT, &vDir[STATE_RIGHT]);
	Set_StateInfo(CTransform::STATE_UP, &vDir[STATE_UP]);
	Set_StateInfo(CTransform::STATE_LOOK, &vDir[STATE_LOOK]);
}

void CTransform::Go_ToTarget(const _vec3 * pTargetPos, const _float & fTimeDelta)
{
	_vec3 vLook, vPosition;
	
	vPosition = *Get_StateInfo(CTransform::STATE_POSITION);
	vLook = *pTargetPos - vPosition;
	vPosition += *D3DXVec3Normalize(&vLook, &vLook) * m_fSpeed_Move * fTimeDelta;
	Set_StateInfo(CTransform::STATE_POSITION, &vPosition);
}

void CTransform::Move(const _float & fTimeDelta, const _vec2& vSpd)
{
	_vec3 vPos = *Get_StateInfo(CTransform::STATE_POSITION);

	vPos += *Get_StateInfo(CTransform::STATE_RIGHT)*vSpd.x*fTimeDelta;
	vPos += *Get_StateInfo(CTransform::STATE_UP)*vSpd.y*fTimeDelta;

	Set_StateInfo(CTransform::STATE_POSITION, &vPos);
}

_float CTransform::Get_Width()
{
	return D3DXVec3Length(Get_StateInfo(CTransform::STATE_RIGHT));
}

_float CTransform::Get_Height()
{
	return D3DXVec3Length(Get_StateInfo(CTransform::STATE_UP));
}


CTransform * CTransform::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CTransform* pInstance = new CTransform(pGraphic_Device);

	if (FAILED(pInstance->Ready_Transform()))
	{
		_MSG_BOX("CTransform Created Failed")
		Safe_Release(pInstance);
	}
	return pInstance;
}

CComponent * CTransform::Clone_Component()
{
	return new CTransform(*this);
}

void CTransform::Free()
{
	CComponent::Free();
}

#include "..\Headers\VIBuffer.h"

CVIBuffer::CVIBuffer(LPDIRECT3DDEVICE9 pGraphic_Device)
	:CComponent(pGraphic_Device)
	, m_isClone(false)
{
}

CVIBuffer::CVIBuffer(const CVIBuffer & rhs)
	:CComponent(rhs)
	,m_pVB(rhs.m_pVB)
	,m_pIB(rhs.m_pIB)
	,m_iStride(rhs.m_iStride)
	,m_iNumVertices(rhs.m_iNumVertices)
	,m_dwFVF(rhs.m_dwFVF)
	,m_iNumPolygons(rhs.m_iNumPolygons)
	,m_iPolygonSize(rhs.m_iPolygonSize)
	,m_eFormat(rhs.m_eFormat)
	, m_isClone(true)
{
	m_pVB->AddRef();
	m_pIB->AddRef();
}

HRESULT CVIBuffer::Ready_VIBuffer()
{
	if (FAILED(m_pGraphic_Device->CreateVertexBuffer(m_iStride * m_iNumVertices, 0, m_dwFVF, D3DPOOL_MANAGED, &m_pVB, nullptr)))
		return E_FAIL;
	if (FAILED(m_pGraphic_Device->CreateIndexBuffer(m_iPolygonSize * m_iNumPolygons, 0, m_eFormat, D3DPOOL_MANAGED, &m_pIB, nullptr)))
		return E_FAIL;


	return NOERROR;
}

//template<typename T>
void CVIBuffer::UpdateNormal()
{
	VTXNMCOL*	pVertices = nullptr;

	m_pVB->Lock(0, 0, (void**)&pVertices, 0);


	for (size_t i = 0; i < m_iPolygonSize; ++i)
	{
		_vec3 vCross;
 		ComputeNormal(&pVertices[m_pIndex[i]._0].vPosition, &pVertices[m_pIndex[i]._1].vPosition,
			&pVertices[m_pIndex[i]._2].vPosition, &vCross);

		pVertices[m_pIndex[i]._0].vNormal += vCross;
		pVertices[m_pIndex[i]._1].vNormal += vCross;
		pVertices[m_pIndex[i]._2].vNormal += vCross;
	}

	for (size_t i = 0; i < m_iNumVertices; ++i)
	{
		D3DXVec3Normalize(&pVertices[i].vNormal, &pVertices[i].vNormal);
	}

	m_pVB->Unlock();

}

void CVIBuffer::ComputeNormal(_vec3 * p0, _vec3 * p1, _vec3 * p2, _vec3 * out)
{
 	_vec3 u = *p1 - *p0;
	_vec3 v = *p2 - *p0;

	D3DXVec3Cross(out,&u,&v);
	D3DXVec3Normalize(out,out);
}

void CVIBuffer::Free()
{
	if (false!=m_isClone)
	{
		Safe_Delete_Array(m_pPosition);
		Safe_Delete_Array(m_pIndex);
	}
	Safe_Release(m_pIB);
	Safe_Release(m_pVB);

	CComponent::Free();
}

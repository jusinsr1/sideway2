#pragma once

#include "VIBuffer.h"

_BEGIN(Engine)

class _ENGINE_DLL CBuffer_CubeLine final : public CVIBuffer
{
private:
	explicit CBuffer_CubeLine(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CBuffer_CubeLine(const CBuffer_CubeLine& rhs);
	virtual ~CBuffer_CubeLine() = default;
public:
	HRESULT Ready_VIBuffer();
	void Render_VIBuffer();
public:
	//virtual _bool Picking_ToBuffer(_vec3* pOut, const CTransform* pTransformCom, const CPicking* pPickingCom);
public:
	static CBuffer_CubeLine* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CComponent* Clone_Component();
protected:
	virtual void Free();




};

_END
#pragma once

#include "VIBuffer.h"

_BEGIN(Engine)

class _ENGINE_DLL CBuffer_CubeTex final : public CVIBuffer
{
private:
	explicit CBuffer_CubeTex(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CBuffer_CubeTex(const CBuffer_CubeTex& rhs);
	virtual ~CBuffer_CubeTex() = default;
public:
	HRESULT Ready_VIBuffer();
	void Render_VIBuffer();
public:
	static CBuffer_CubeTex* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CComponent* Clone_Component();
protected:
	virtual void Free();


};

_END
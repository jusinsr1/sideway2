#pragma once
#include "VIBuffer.h"

_BEGIN(Engine)

class _ENGINE_DLL CBuffer_RcTex final: public CVIBuffer
{
private:
	explicit CBuffer_RcTex(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CBuffer_RcTex(const CBuffer_RcTex& rhs);
	virtual ~CBuffer_RcTex() = default;
public:
	HRESULT Ready_VIBuffer();
	void Render_VIBuffer();
public:
	static CBuffer_RcTex* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CComponent* Clone_Component();
protected:
	virtual void Free();
};

_END
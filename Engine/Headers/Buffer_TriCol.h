#pragma once

#include "VIBuffer.h"

_BEGIN(Engine)

class _ENGINE_DLL CBuffer_TriCol final : public CVIBuffer
{
private:
	explicit CBuffer_TriCol(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CBuffer_TriCol(const CBuffer_TriCol& rhs);
	virtual ~CBuffer_TriCol() = default;
public:
	HRESULT Ready_VIBuffer();
	void Render_VIBuffer();
public:
	static CBuffer_TriCol* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CComponent* Clone_Component();
protected:
	virtual void Free();
};

_END
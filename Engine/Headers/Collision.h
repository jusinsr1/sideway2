#pragma once
#include "Component.h"

_BEGIN(Engine)
class CTransform;
class CGameObject;
class _ENGINE_DLL CCollision final : public CComponent
{
public:
	enum COLLGROUP {COLL_PLAYER,COLL_TIRE, COLL_END};
private:
	explicit CCollision(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual ~CCollision() = default;

public:
	HRESULT Ready_Collision();
	HRESULT Add_CollGroup(COLLGROUP eGroup, CGameObject* pGameObject);
	HRESULT Coll_CollGroup();
private:
	list<CGameObject*>	m_CollList[COLL_END];
	typedef list<CGameObject*>	OBJECTLIST;

private:
	void Coll_Player_Tire();

private:
	_bool Rect_Cube(CTransform* left, CTransform* right);

private:
	_bool Coll_Rect(const RC& rc1, const RC& rc2);
public:
	static CCollision* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CComponent* Clone_Component();
protected:
	virtual void Free();
};

_END
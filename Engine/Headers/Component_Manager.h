#pragma once
#include "Base.h"
#include "Transform.h"
#include "Renderer.h"
#include "Buffer_TriCol.h"
#include "Buffer_RcTex.h"
#include "Buffer_Cube.h"
#include "Buffer_CubeTex.h"
#include "Buffer_Particle_Item.h"
#include "Buffer_ViewPort.h"
#include "Buffer_CarTex.h"
#include "Texture.h"
#include "Buffer_Terrain.h"


_BEGIN(Engine)

class CComponent_Manager final : public CBase
{
	_DECLARE_SINGLETON(CComponent_Manager)
private:
	explicit CComponent_Manager();
	virtual ~CComponent_Manager() = default;
public:
	HRESULT Reserve_Component_Manager(const _uint& iNumScene);
	HRESULT Add_Prototype_Component(const _uint& iSceneID, const _tchar* pComponentTag, CComponent* pComponent);
	CComponent* Clone_Component(const _uint& iSceneID, const _tchar* pComponentTag);
private:
	map<const _tchar*, CComponent*>*			m_pMapPrototype = nullptr;
	typedef map<const _tchar*, CComponent*>		MAPPROTOTYPE;
private:
	_uint		m_iNumScene = 0;
private:
	CComponent* Find_Component(const _uint& iSceneID, const _tchar* pComponentTag);
protected:
	virtual void Free() override;
};

_END
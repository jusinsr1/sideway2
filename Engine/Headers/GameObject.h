#pragma once
#include "Base.h"

_BEGIN(Engine)

class CComponent;
class _ENGINE_DLL CGameObject abstract : public CBase
{
protected:
	explicit CGameObject(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CGameObject(const CGameObject& rhs);
	virtual ~CGameObject() = default;
public:
	CComponent* Get_ComponentPointer(const _tchar* pComponentTag);
public:
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& fTImeDelta);
	virtual _int LastUpdate_GameObject(const _float& fTImeDelta);
	virtual void Render_GameObject();
	virtual void Coll_GameObject();
protected:
	LPDIRECT3DDEVICE9		m_pGraphic_Device = nullptr;
	_float					m_fTime = 0.f;
protected:
	map<const _tchar*, CComponent*>			m_mapComponent;
	typedef map<const _tchar*, CComponent*>	MAPCOMPONENT;
protected:
	HRESULT Add_Component(const _tchar* pComponentTag, CComponent* pComponent);
	virtual _bool Time_Permit(const _float& fTimePermit, const _float& fTimeDelta);
public:
	CComponent*  Find_Component(const _tchar* pComponentTag);
	

public:
	virtual CGameObject* Clone_GameObject() = 0;
protected:
	virtual void Free();
};

_END
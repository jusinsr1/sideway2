#pragma once
#include "Base.h"

_BEGIN(Engine)

class _ENGINE_DLL CInput_Device final: public CBase
{
	_DECLARE_SINGLETON(CInput_Device)
public:
	enum MOUSEBUTTON {DIM_LBUTTON, DIM_RBUTTON, DIM_WHEEL, DIMXBUTTON};
	enum MOUSEMOVE {DIM_X, DIM_Y, DIM_Z};

private:
	explicit CInput_Device();
	virtual ~CInput_Device() = default;
public:
	HRESULT Ready_Input_Device(HINSTANCE hInst, HWND hWnd);
	void SetUp_InputState();
public:
	_byte Get_DIKeyState(_ubyte byKeyID) { return m_KeyState[byKeyID]; }
	_byte Get_DIPreKeyState(_ubyte byKeyID) { return m_PreKeyState[byKeyID]; }
	_byte Get_DIMouseState(_ubyte byKeyID) { return m_MouseState.rgbButtons[byKeyID]; }
	LONG Get_DIMouseMove(_ubyte byKeyID) { return *((long*)&m_MouseState + byKeyID); }
	_bool IsKeyPressing(_ubyte byKeyID);
	_bool IsKeyDown(_ubyte byKeyID);
	_bool IsKeyUp(_ubyte byKeyID);
private:
	LPDIRECTINPUT8			m_pSDK = nullptr;
	LPDIRECTINPUTDEVICE8	m_pKeyBoard = nullptr;
	LPDIRECTINPUTDEVICE8	m_pMouse = nullptr;
private:
	_byte					m_KeyState[256];
	_byte					m_PreKeyState[256];
	DIMOUSESTATE			m_MouseState;
	DIMOUSESTATE			m_PreMouseState;
private:
	HRESULT Ready_KeyBoard(HWND hWnd);
	HRESULT Ready_Mouse(HWND hWnd);
protected:
	virtual void Free();
};

_END
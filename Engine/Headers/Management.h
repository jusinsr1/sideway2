#pragma once
#include "Base.h"
#include "Component_Manager.h"
#include "Object_Manager.h"

_BEGIN(Engine)

class CScene;
class CComponent;
class CGameObject;
class _ENGINE_DLL CManagement final : public CBase
{
	_DECLARE_SINGLETON(CManagement)
private:
	explicit CManagement();
	virtual ~CManagement() = default;
public:
	CComponent* Get_ComponentPointer(const _uint& iSceneID, const _tchar* pLayerTag, const _tchar* pComponentTag, const _uint& iIndex = 0);
public:
	HRESULT Ready_Management(const _uint& iNumScene);
	HRESULT Add_Prototype_GameObject(const _tchar* pGameObjectTag, CGameObject* pGameObject);
	HRESULT Add_Prototype_Component(const _uint& iSceneID, const _tchar* pComponentTag, CComponent* pComponent);
	HRESULT Add_GameObjectToLayer(const _tchar* pProtoTag, const _uint& iSceneID, const _tchar* pLayerTag, CGameObject** ppCloneObject = nullptr);
	CComponent* Clone_Component(const _uint& iSceneID, const _tchar* pComponentTag);
	HRESULT SetUp_ScenePointer(CScene* pNewScenePointer);
	void	Render_Management();
	_int	Update_Management(const _float& fTimeDelta);
	HRESULT Clear_Layers(const _uint& iSceneID);
	static void Release_Engine();
private:
	CScene*				m_pScene = nullptr;
	CObject_Manager*	m_pObject_Manager = nullptr;
	CComponent_Manager*	m_pComponent_Manager = nullptr;
protected:
	virtual void Free();
};

_END
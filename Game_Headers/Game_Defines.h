#pragma once

const unsigned int g_iBackCX = 800;
const unsigned int g_iBackCY = 600;

enum SCENEID {SCENE_STATIC, SCENE_LOADING, SCENE_LOGO, SCENE_STAGE, SCENE_END};
enum Direction { DOWN,LEFT, UP, RIGHT,DIR_END };
enum CubeID { CUBE_NOMAL, CUBE_BUILDING, CUBE_JUMP, CUBE_BOX };

extern HINSTANCE g_hInst;
extern HWND g_hWnd;


const D3DXCOLOR      WHITE(D3DCOLOR_XRGB(255, 255, 255));
const D3DXCOLOR      BLACK(D3DCOLOR_XRGB(0, 0, 0));
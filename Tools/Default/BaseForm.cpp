// BaseForm.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Tools.h"
#include "BaseForm.h"
#include "Camera.h"


// CBaseForm
IMPLEMENT_DYNCREATE(CBaseForm, CFormView)

CBaseForm::CBaseForm()
	: CFormView(IDD_BASEFORM)
	, m_CameraPos(_T(""))
	, m_MousePos(_T(""))
	, m_IsLock(_T(""))
{

}

CBaseForm::~CBaseForm()
{
}

void CBaseForm::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_RADIO1, m_CameraState[0]);
	DDX_Control(pDX, IDC_RADIO2, m_CameraState[1]);
	DDX_Control(pDX, IDC_RADIO3, m_CameraState[2]);
	DDX_Control(pDX, IDC_RADIO4, m_CameraState[3]);
	DDX_Control(pDX, IDC_RADIO5, m_CameraState[4]);
	DDX_Control(pDX, IDC_RADIO6, m_CameraState[5]);
	DDX_Text(pDX, IDC_EDIT1, m_CameraPos);
	DDX_Control(pDX, IDC_CHECK1, m_FrameMode);
	DDX_Text(pDX, IDC_EDIT2, m_MousePos);
	DDX_Text(pDX, IDC_EDIT4, m_IsLock);
}

void CBaseForm::MousePos(POINT mouse)
{
	m_vecMousePos = GET_INSTANCE(CSaveLoad_Manager)->PickCheck(mouse);
	{
		TCHAR MousePos[50];
		swprintf_s(MousePos, L"MousePos x:%0.1f,y:%0.1f, z:%0.1f", m_vecMousePos.x, m_vecMousePos.y, m_vecMousePos.z);
		m_MousePos.SetString(MousePos);
	}
	_matrix matView, m;
	GET_INSTANCE(CDevice)->GetDevice()->GetTransform(D3DTS_VIEW, &matView);

	D3DXMatrixInverse(&m, NULL, &matView);

	
}

void CBaseForm::GetCameraPos(POINT mouse)
{
	if (this != nullptr)
	{
		_vec3 vecCamera = m_pCamera->GetCameraPos();
		TCHAR CameraPos[50];
		swprintf_s(CameraPos, L"CameraPos  x:%d, y:%d, z:%d", (int)vecCamera.x, (int)vecCamera.y, (int)vecCamera.z);
		m_CameraPos.SetString(CameraPos);
		UpdateData(FALSE);


		MousePos(mouse);
		if (GET_INSTANCE(CKeyMgr)->KeyDown(KEY_SPACE))
		{
			if (m_pCamera->SetLock())
				m_IsLock.SetString(L"Lock");
			else
				m_IsLock.SetString(L"UnLock");
		}
	}
}

BEGIN_MESSAGE_MAP(CBaseForm, CFormView)
	ON_BN_CLICKED(IDC_RADIO1, &CBaseForm::OnBnClickedRadioCameraState)
	ON_BN_CLICKED(IDC_RADIO2, &CBaseForm::OnBnClickedRadioCameraState)
	ON_BN_CLICKED(IDC_RADIO3, &CBaseForm::OnBnClickedRadioCameraState)
	ON_BN_CLICKED(IDC_RADIO4, &CBaseForm::OnBnClickedRadioCameraState)
	ON_BN_CLICKED(IDC_RADIO5, &CBaseForm::OnBnClickedRadioCameraState)
	ON_BN_CLICKED(IDC_RADIO6, &CBaseForm::OnBnClickedRadioCameraState)
	ON_BN_CLICKED(IDC_CHECK1, &CBaseForm::OnBnClickedFrameMode)
	ON_BN_CLICKED(IDC_BUTTON1, &CBaseForm::OnBnClickedResetCamera)
	ON_BN_CLICKED(IDC_BUTTON3, &CBaseForm::OnBnClickedBTexturePath)
END_MESSAGE_MAP()


// CBaseForm 진단입니다.

#ifdef _DEBUG
void CBaseForm::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CBaseForm::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CBaseForm 메시지 처리기입니다.


void CBaseForm::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();
	m_pCamera = GET_INSTANCE(CSaveLoad_Manager)->GetCamera();
	m_CameraState[0].SetCheck(1);
	m_IsLock.SetString(L"UnLock");
}


void CBaseForm::OnBnClickedRadioCameraState()
{
	for (size_t i = 0; i < 6; i++)
	{
		if (m_CameraState[i].GetCheck())
			m_pCamera->SetCameraState(i);
	}
}

void CBaseForm::OnBnClickedFrameMode()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if (m_FrameMode.GetCheck())
		GET_INSTANCE(CDevice)->GetDevice()->SetRenderState(D3DRS_FILLMODE, D3DFILL_WIREFRAME);
	else
		GET_INSTANCE(CDevice)->GetDevice()->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);
}


void CBaseForm::OnBnClickedResetCamera()
{
	_vec3 Position = { 0,0,0 };
	_vec3 Right = { 1,0,0 };
	_vec3 Up = { 0,1,0 };
	_vec3 Look = { 0,0,1 };
	m_pCamera->Set_StateInfo(CObj::STATE_POSITION, &Position);
	m_pCamera->Set_StateInfo(CObj::STATE_RIGHT, &Right);
	m_pCamera->Set_StateInfo(CObj::STATE_UP, &Up);
	m_pCamera->Set_StateInfo(CObj::STATE_LOOK, &Look);
}


void CBaseForm::OnBnClickedBTexturePath()
{
	if (nullptr == m_TexturePath.GetSafeHwnd())
	{
		// 해당 ID에 맞는 다이얼로그 생성.
		m_TexturePath.Create(IDD_TEXTUREPATH);
	}
	m_TexturePath.ShowWindow(SW_SHOW);
}

#pragma once
#include "afxwin.h"

#include "TexturePath.h"

// CBaseForm 폼 뷰입니다.
class CCamera;
class CBaseForm : public CFormView
{
	DECLARE_DYNCREATE(CBaseForm)

protected:
	CBaseForm();           // 동적 만들기에 사용되는 protected 생성자입니다.
	virtual ~CBaseForm();

public:
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_BASEFORM };
#endif
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif
	void MousePos(POINT mouse);
	void GetCameraPos(POINT mouse);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	CTexturePath	m_TexturePath;
private:
	CCamera* m_pCamera = nullptr;
public:
	virtual void OnInitialUpdate();
	afx_msg void OnBnClickedResetCamera();
	afx_msg void OnBnClickedFrameMode();
	afx_msg void OnBnClickedRadioCameraState();
	afx_msg void OnBnClickedBTexturePath();
	_vec3 GetMousePos() { return m_vecMousePos; }
	CButton m_CameraState[6];

public:
	CString m_CameraPos;
	CButton m_FrameMode;
	CString m_MousePos;
	CString m_IsLock;
	_vec3 m_vecMousePos;
};



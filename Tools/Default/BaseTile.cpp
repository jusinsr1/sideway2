#include "stdafx.h"
#include "BaseTile.h"

CBaseTile::CBaseTile(LPDIRECT3DDEVICE9 pGraphic_Device)
	:CMapObject(pGraphic_Device)
{
}

CBaseTile::~CBaseTile()
{
	Release();
}

void CBaseTile::Ready_BaseTile(const _uint iNumVerticesX, const _uint iNumVerticesZ, const _float & fInterval)
{
	D3DXCreateTextureFromFile(m_pGraphic_Device, L"../../Resources/Textures/2DMap/Asphalt/asphalt0.png", &m_pTexture);

	D3DXMatrixIdentity(&m_matWorld);
	m_iNumVerticesX = iNumVerticesX;
	m_iNumVerticesZ = iNumVerticesZ;
	m_fInterval = fInterval;

	m_iNumVertices = iNumVerticesX * iNumVerticesZ;
	m_pPosition = new _vec3[m_iNumVertices];
	m_iStride = sizeof(VTXTEX);
	m_dwFVF = D3DFVF_XYZ | D3DFVF_TEX1;
	m_iNumPolygons = (iNumVerticesX - 1) * (iNumVerticesZ - 1) * 2;

	m_iPolygonSize = sizeof(POLYGON32);
	m_eFormat = D3DFMT_INDEX32;

	CMapObject::Ready_Object();

	VTXTEX*		pVertices = nullptr;

	m_pVB->Lock(0, 0, (void**)&pVertices, 0);

	for (size_t i = 0; i < iNumVerticesZ; i++)
	{
		for (size_t j = 0; j < iNumVerticesX; j++)
		{
			_uint		iIndex = i * iNumVerticesX + j;

			pVertices[iIndex].vPosition = _vec3(j * m_fInterval, 0.0f, i * m_fInterval);
			pVertices[iIndex].vTexUV = _vec2(j / (iNumVerticesX - 1.f) * 30.f, i / (iNumVerticesZ - 1.f) * 30.f);
		}
	}
	for (size_t i = 0; i < m_iNumVertices; i++)
	{
		m_pPosition[i] = pVertices[i].vPosition;
	}
	m_pVB->Unlock();


	POLYGON32*		pIndices = nullptr;

	m_pIB->Lock(0, 0, (void**)&pIndices, 0);

	_uint			iPolygonIndex = 0;

	for (size_t i = 0; i < iNumVerticesZ - 1; i++)
	{
		for (size_t j = 0; j < iNumVerticesX - 1; j++)
		{
			size_t iIndex = i * iNumVerticesX + j;

			pIndices[iPolygonIndex]._0 = iIndex + iNumVerticesX;
			pIndices[iPolygonIndex]._1 = iIndex + iNumVerticesX + 1;
			pIndices[iPolygonIndex]._2 = iIndex + 1;
			++iPolygonIndex;

			pIndices[iPolygonIndex]._0 = iIndex + iNumVerticesX;
			pIndices[iPolygonIndex]._1 = iIndex + 1;
			pIndices[iPolygonIndex]._2 = iIndex;
			++iPolygonIndex;
		}
	}


	m_pIB->Unlock();

}

CBaseTile * CBaseTile::Create(LPDIRECT3DDEVICE9 pGraphic_Device, const _uint iNumVerticesX, const _uint iNumVerticesZ, const _float & fInterval)
{
	CBaseTile* pInstance = new CBaseTile(pGraphic_Device);
	pInstance->Ready_BaseTile(iNumVerticesX, iNumVerticesZ, fInterval);
	
	return pInstance;
}

void CBaseTile::Update()
{
}

void CBaseTile::Render()
{
	m_pGraphic_Device->SetTexture(0, m_pTexture);
	m_pGraphic_Device->SetTransform(D3DTS_WORLD, &m_matWorld);
	m_pGraphic_Device->SetStreamSource(0, m_pVB, 0, m_iStride);
	m_pGraphic_Device->SetFVF(m_dwFVF);
	m_pGraphic_Device->SetIndices(m_pIB);
	m_pGraphic_Device->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, m_iNumVertices, 0, m_iNumPolygons);
}

void CBaseTile::Release()
{
}

_vec3 CBaseTile::UpdateRay(const _vec3 * vMouseRay, const _vec3 * vCameraPos, _float * dist)
{
	_vec3	vRayPivot = *vCameraPos;
	_vec3	vRay = *vMouseRay;

	_matrix	matWorldInv = Get_Matrix_Inverse();

	D3DXVec3TransformCoord(&vRayPivot, &vRayPivot, &matWorldInv);
	D3DXVec3TransformNormal(&vRay, &vRay, &matWorldInv);

	_float		fU, fV, fDist;

	_uint			iPolygonIndex = 0;
	for (size_t i = 0; i < m_iNumVerticesZ - 1; i++)
	{
		for (size_t j = 0; j < m_iNumVerticesX - 1; j++)
		{
			size_t iIndex = i * m_iNumVerticesX + j;

			if (TRUE == D3DXIntersectTri(&m_pPosition[iIndex + m_iNumVerticesX + 1], &m_pPosition[iIndex + m_iNumVerticesX], &m_pPosition[iIndex + 1], &vRayPivot, &vRay, &fU, &fV, &fDist))
			{
				//D3DXVec3Normalize(&vRay, &vRay);
				_vec3 Pos = vRayPivot + (vRay* fDist);
				return Pos;
			}
			if (TRUE == D3DXIntersectTri(&m_pPosition[iIndex + 1], &m_pPosition[iIndex + m_iNumVerticesX], &m_pPosition[iIndex], &vRayPivot, &vRay, &fU, &fV, &fDist))
			{
				//D3DXVec3Normalize(&vRay, &vRay);
				_vec3 Pos = vRayPivot + (vRay* fDist);
				return Pos;
			}
		}
	}


	return _vec3(-1, -1, -1);
}

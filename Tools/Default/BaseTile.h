#pragma once
#include "MapObject.h"

class CBaseTile :	public CMapObject
{
private:
	explicit CBaseTile(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual ~CBaseTile();

private:
	void Ready_BaseTile(const _uint iNumVerticesX, const _uint iNumVerticesZ, const _float& fInterval = 1.f);
	virtual void Update() override;
	virtual void Render() override;
	virtual void Release() override;
	virtual _vec3 UpdateRay(const _vec3* vMouseRay, const _vec3* vCameraPos, _float* dist);

public:
	static CBaseTile* Create(LPDIRECT3DDEVICE9 pGraphic_Device, const _uint iNumVerticesX, const _uint iNumVerticesZ, const _float& fInterval = 1.f);


private:
	_uint		m_iNumVerticesX = 0;
	_uint		m_iNumVerticesZ = 0;
	_float		m_fInterval = 0.f;
protected:
	LPDIRECT3DTEXTURE9			m_pTexture = nullptr;

};


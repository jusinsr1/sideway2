#include "stdafx.h"
#include "Camera.h"


CCamera::CCamera(LPDIRECT3DDEVICE9 pGraphic_Device)
	:CObj(pGraphic_Device)
{
}


CCamera::~CCamera()
{
}

void CCamera::Ready_Camera()
{
	D3DXMatrixIdentity(&m_matWorld);
	D3DXMatrixIdentity(&m_matView);
	D3DXMatrixIdentity(&m_matProj);

	ZeroMemory(&m_ProjDesc, sizeof(PROJDESC));
	m_ProjDesc.fFovY = D3DXToRadian(60.f);
	m_ProjDesc.fAspect = _float(WINCX)/_float(WINCY);
	m_ProjDesc.fNear = 0.1f;
	m_ProjDesc.fFar = 500.f;

	ZeroMemory(&m_CameraDesc, sizeof(CAMERADESC));
	m_CameraDesc.vEye = _vec3(0.f, 20.f, -10.f);
	m_CameraDesc.vAt = _vec3(0.f, 0.f, 0.f);
	m_CameraDesc.vAxisY = _vec3(0.f, 1.f, 0.f);

	m_matProj._11 = (1.f / tan(m_ProjDesc.fFovY * 0.5f)) / m_ProjDesc.fAspect;
	m_matProj._22 = 1.f / tan(m_ProjDesc.fFovY * 0.5f);
	m_matProj._33 = m_ProjDesc.fFar / (m_ProjDesc.fFar - m_ProjDesc.fNear);
	m_matProj._43 = (m_ProjDesc.fFar * m_ProjDesc.fNear) / (m_ProjDesc.fFar - m_ProjDesc.fNear) * -1.f;
	m_matProj._34 = 1.f;
	m_matProj._44 = 0.f;

	_vec3 vLook, vRight, vUp;

	vLook = m_CameraDesc.vAt - m_CameraDesc.vEye;
	D3DXVec3Normalize(&vLook, &vLook);
	D3DXVec3Cross(&vRight, &m_CameraDesc.vAxisY, &vLook);
	D3DXVec3Normalize(&vRight, &vRight);
	D3DXVec3Cross(&vUp, &vLook, &vRight);
	D3DXVec3Normalize(&vUp, &vUp);

	memcpy(&m_matWorld.m[0][0], &vRight, sizeof(_vec3));
	memcpy(&m_matWorld.m[1][0], &vUp, sizeof(_vec3));
	memcpy(&m_matWorld.m[2][0], &vLook, sizeof(_vec3));
	memcpy(&m_matWorld.m[3][0], &m_CameraDesc.vEye, sizeof(_vec3));
	Invalidate_ViewProjMatrix();
}

void CCamera::Update()
{
	if (!m_CameraLock)
	{
		if (GET_INSTANCE(CKeyMgr)->KeyPressing(KEY_D))
		{
			CObj::Go_Right();
		}
		if (GET_INSTANCE(CKeyMgr)->KeyPressing(KEY_A))
		{
			CObj::Go_Left();
		}

		if (m_CameraState == 0)
		{
			if (GET_INSTANCE(CKeyMgr)->KeyPressing(KEY_W))
			{
				CObj::Go_Straight();
			}
			if (GET_INSTANCE(CKeyMgr)->KeyPressing(KEY_S))
			{
				CObj::BackWard();
			}
			_long MouseMove = 0;
			if (MouseMove = GET_INSTANCE(CInputDevice)->Get_DIMouseMove(CInputDevice::DIM_X))
				CObj::Rotation_Y(0.005f*MouseMove);
			if (MouseMove = GET_INSTANCE(CInputDevice)->Get_DIMouseMove(CInputDevice::DIM_Y))
				CObj::Rotation_AxisRight(0.005f*MouseMove);
		}
		else
		{
			if (GET_INSTANCE(CKeyMgr)->KeyPressing(KEY_W))
			{
				CObj::Go_Up();
			}
			if (GET_INSTANCE(CKeyMgr)->KeyPressing(KEY_S))
			{
				CObj::Go_Down();
			}
		}
	}
	Invalidate_ViewProjMatrix();
}

void CCamera::Render()
{
}

void CCamera::Release()
{
}

void CCamera::SetCameraState(_uint State)
{
	m_CameraState = State;

	if (m_CameraState == 1)
	{
		SetUp_RotationY(D3DXToRadian(0.f));
	}
	else if (m_CameraState == 2)
	{
		SetUp_RotationY(D3DXToRadian(270.f));
	}
	else if (m_CameraState == 3)
	{
		SetUp_RotationY(D3DXToRadian(180.f));
	}
	else if (m_CameraState == 4)
	{
		SetUp_RotationY(D3DXToRadian(90.f));
	}
	else if (m_CameraState == 5)
	{
		SetUp_RotationX(D3DXToRadian(90.f));
	}
}

void CCamera::Invalidate_ViewProjMatrix()
{
	D3DXMatrixInverse(&m_matView, nullptr, &m_matWorld);

	m_pGraphic_Device->SetTransform(D3DTS_VIEW, &m_matView);

	m_pGraphic_Device->SetTransform(D3DTS_PROJECTION, &m_matProj);
}

_vec3 CCamera::GetCameraPos()
{
	return _vec3(m_matWorld._41, m_matWorld._42, m_matWorld._43);
}

CCamera * CCamera::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CCamera* pInstance = new CCamera(pGraphic_Device);
	pInstance->Ready_Camera();

	return pInstance;
}
#pragma once
class CCamera final : public CObj
{
public:
	explicit CCamera(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual ~CCamera();

	// CObj을(를) 통해 상속됨
public:
	void Ready_Camera();
	virtual void Update() override;
	virtual void Render() override;
	virtual void Release() override;
	void SetCameraState(_uint State);
	void Invalidate_ViewProjMatrix();
	_vec3 GetCameraPos();
	_bool SetLock() { m_CameraLock = !m_CameraLock; return m_CameraLock; }
	
private:
	_matrix			m_matView;
	CAMERADESC		m_CameraDesc;
	_matrix			m_matProj;
	PROJDESC		m_ProjDesc;

	_uint			m_CameraState = 0;
	_bool			m_CameraLock = false;
public:
	static CCamera* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
};


#include "stdafx.h"
#include "Cube.h"


CCube::CCube(LPDIRECT3DDEVICE9 pGraphic_Device)
	:CMapObject(pGraphic_Device)
{
}


CCube::~CCube()
{
	
}

void CCube::Ready_Cube(_vec3 & Pos, const _vec3 & Scale, const wstring& Path, const wstring& ImgTag)
{
	m_wstrImageTag = ImgTag;
	_tchar chPath[MAX_STR] = L"";
	m_Position = Pos;
	m_Scale = Scale;
	swprintf(chPath, Path.c_str(), 0);
	D3DXCreateCubeTextureFromFile(m_pGraphic_Device, chPath, &m_pCubeTexture);
	
	D3DXMatrixIdentity(&m_matWorld);
	m_matWorld._11 = Scale.x;
	m_matWorld._22 = Scale.y;
	m_matWorld._33 = Scale.z;
	m_matWorld._41 = Pos.x;
	m_matWorld._42 = Pos.y;
	m_matWorld._43 = Pos.z;
	
	m_iStride = sizeof(VTXCUBE);
	m_iNumVertices = 8;
	m_dwFVF = D3DFVF_XYZ | D3DFVF_TEX1 | D3DFVF_TEXCOORDSIZE3(0);

	m_pPosition = new _vec3[m_iNumVertices];

	m_iNumPolygons = 12;
	m_iPolygonSize = sizeof(POLYGON16);
	m_eFormat = D3DFMT_INDEX16;
	
	CMapObject::Ready_Object();

	VTXCUBE*		pVertices = nullptr;

	m_pVB->Lock(0, 0, (void**)&pVertices, 0);
	pVertices[0].vTexture = pVertices[0].vPosition = m_pPosition[0] = _vec3(-0.5f, 0.5f, -0.5f);
	pVertices[1].vTexture = pVertices[1].vPosition = m_pPosition[1] = _vec3(0.5f, 0.5f, -0.5f);
	pVertices[2].vTexture = pVertices[2].vPosition = m_pPosition[2] = _vec3(0.5f, -0.5f, -0.5f);
	pVertices[3].vTexture = pVertices[3].vPosition = m_pPosition[3] = _vec3(-0.5f, -0.5f, -0.5f);

	pVertices[4].vTexture = pVertices[4].vPosition = m_pPosition[4] = _vec3(-0.5f, 0.5f, 0.5f);
	pVertices[5].vTexture = pVertices[5].vPosition = m_pPosition[5] = _vec3(0.5f, 0.5f, 0.5f);
	pVertices[6].vTexture = pVertices[6].vPosition = m_pPosition[6] = _vec3(0.5f, -0.5f, 0.5f);
	pVertices[7].vTexture = pVertices[7].vPosition = m_pPosition[7] = _vec3(-0.5f, -0.5f, 0.5f);
	m_pVB->Unlock();

	POLYGON16*	pIndices = nullptr;
	m_pIB->Lock(0, 0, (void**)&pIndices, 0);

	pIndices[0]._0 = 1;	pIndices[0]._1 = 5;	pIndices[0]._2 = 6;
	pIndices[1]._0 = 1;	pIndices[1]._1 = 6;	pIndices[1]._2 = 2;
	pIndices[2]._0 = 4;	pIndices[2]._1 = 0;	pIndices[2]._2 = 3;
	pIndices[3]._0 = 4;	pIndices[3]._1 = 3;	pIndices[3]._2 = 7;
	pIndices[4]._0 = 4;	pIndices[4]._1 = 5;	pIndices[4]._2 = 1;
	pIndices[5]._0 = 4;	pIndices[5]._1 = 1;	pIndices[5]._2 = 0;
	pIndices[6]._0 = 3;	pIndices[6]._1 = 2;	pIndices[6]._2 = 6;
	pIndices[7]._0 = 3;	pIndices[7]._1 = 6;	pIndices[7]._2 = 7;
	pIndices[8]._0 = 7;	pIndices[8]._1 = 6;	pIndices[8]._2 = 5;
	pIndices[9]._0 = 7;	pIndices[9]._1 = 5;	pIndices[9]._2 = 4;
	pIndices[10]._0 = 0; pIndices[10]._1 = 1; pIndices[10]._2 = 2;
	pIndices[11]._0 = 0; pIndices[11]._1 = 2; pIndices[11]._2 = 3;

	m_pIB->Unlock();
}

void CCube::Update()
{
}

void CCube::Render()
{
	//m_pGraphic_Device->SetRenderState(D3DRS_LIGHTING, FALSE);
	//m_pGraphic_Device->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);

	m_pGraphic_Device->SetTransform(D3DTS_WORLD, &m_matWorld);
	m_pGraphic_Device->SetTexture(0, m_pCubeTexture);

	m_pGraphic_Device->SetStreamSource(0, m_pVB, 0, m_iStride);
	m_pGraphic_Device->SetFVF(m_dwFVF);
	m_pGraphic_Device->SetIndices(m_pIB);
	m_pGraphic_Device->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, m_iNumVertices, 0, m_iNumPolygons);


	//m_pGraphic_Device->SetRenderState(D3DRS_LIGHTING, TRUE);
	//m_pGraphic_Device->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);
}

void CCube::Release()
{
}

_vec3 CCube::UpdateRay(const _vec3 * vMouseRay, const _vec3 * vCameraPos, _float * dist)
{
	_vec3	vRayPivot = *vCameraPos;
	_vec3	vRay = *vMouseRay;

	_matrix	matWorldInv = Get_Matrix_Inverse();

	D3DXVec3TransformCoord(&vRayPivot, &vRayPivot, &matWorldInv);
	D3DXVec3TransformNormal(&vRay, &vRay, &matWorldInv);

	_float		fU, fV, fDist, fDistMin = -1;
	if (D3DXIntersectTri(&m_pPosition[1], &m_pPosition[5], &m_pPosition[6], &vRayPivot, &vRay, &fU, &fV, &fDist))
		if (fDistMin == -1   || fDist < fDistMin)
			fDistMin = fDist;
	if (D3DXIntersectTri(&m_pPosition[1], &m_pPosition[6], &m_pPosition[2], &vRayPivot, &vRay, &fU, &fV, &fDist))
		if (fDistMin == -1 || fDist < fDistMin)
			fDistMin = fDist;
	if (D3DXIntersectTri(&m_pPosition[4], &m_pPosition[0], &m_pPosition[3], &vRayPivot, &vRay, &fU, &fV, &fDist))
		if (fDistMin == -1 || fDist < fDistMin)
			fDistMin = fDist;
	if (D3DXIntersectTri(&m_pPosition[4], &m_pPosition[3], &m_pPosition[7], &vRayPivot, &vRay, &fU, &fV, &fDist))
		if (fDistMin == -1 || fDist < fDistMin)
			fDistMin = fDist;
	if (D3DXIntersectTri(&m_pPosition[4], &m_pPosition[5], &m_pPosition[1], &vRayPivot, &vRay, &fU, &fV, &fDist))
		if (fDistMin == -1 || fDist < fDistMin)
			fDistMin = fDist;
	if (D3DXIntersectTri(&m_pPosition[4], &m_pPosition[1], &m_pPosition[0], &vRayPivot, &vRay, &fU, &fV, &fDist))
		if (fDistMin == -1 || fDist < fDistMin)
			fDistMin = fDist;
	if (D3DXIntersectTri(&m_pPosition[3], &m_pPosition[2], &m_pPosition[6], &vRayPivot, &vRay, &fU, &fV, &fDist))
		if (fDistMin == -1 || fDist < fDistMin)
			fDistMin = fDist;
	if (D3DXIntersectTri(&m_pPosition[3], &m_pPosition[6], &m_pPosition[7], &vRayPivot, &vRay, &fU, &fV, &fDist))
		if (fDistMin == -1 || fDist < fDistMin)
			fDistMin = fDist;
	if (D3DXIntersectTri(&m_pPosition[7], &m_pPosition[6], &m_pPosition[5], &vRayPivot, &vRay, &fU, &fV, &fDist))
		if (fDistMin == -1 || fDist < fDistMin)
			fDistMin = fDist;
	if (D3DXIntersectTri(&m_pPosition[7], &m_pPosition[5], &m_pPosition[4], &vRayPivot, &vRay, &fU, &fV, &fDist))
		if (fDistMin == -1 || fDist < fDistMin)
			fDistMin = fDist;
	if (D3DXIntersectTri(&m_pPosition[0], &m_pPosition[1], &m_pPosition[2], &vRayPivot, &vRay, &fU, &fV, &fDist))
		if (fDistMin == -1 || fDist < fDistMin)
			fDistMin = fDist;
	if (D3DXIntersectTri(&m_pPosition[0], &m_pPosition[2], &m_pPosition[3], &vRayPivot, &vRay, &fU, &fV, &fDist))
		if (fDistMin == -1 || fDist < fDistMin)
			fDistMin = fDist;

	if (fDistMin != -1 && *dist< fDistMin)
	{
		*dist = fDistMin;
		_vec3 Pos = vRayPivot + (vRay* fDistMin);
		return Pos;
	}

	return _vec3(-1, -1, -1);
}

void CCube::SetWorld()
{
	CObj::Set_StateInfo(CObj::STATE_POSITION, &m_Position);
	CObj::Scaling(m_Scale);
}

void CCube::SetTexture(const wstring & Path, const wstring & ImgTag)
{

	if (m_pCubeTexture != nullptr)
	{
		m_pCubeTexture->Release();
		m_pCubeTexture = nullptr;
	}
	
	_tchar chPath[MAX_STR] = L"";
	swprintf(chPath, Path.c_str(), 0);
	D3DXCreateCubeTextureFromFile(m_pGraphic_Device, chPath, &m_pCubeTexture);
}

CCube * CCube::Create(LPDIRECT3DDEVICE9 pGraphic_Device, _vec3 & Pos, _vec3 & Scale, const wstring& Path, const wstring& ImgTag)
{
	CCube* pInstance = new CCube(pGraphic_Device);
	pInstance->Ready_Cube(Pos, Scale, Path, ImgTag);

	return pInstance;
}
#pragma once
#include "MapObject.h"
class CCube final:	public CMapObject
{
public:
	explicit CCube(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual ~CCube();

	void Ready_Cube(_vec3 & Pos, const _vec3 & Scale, const wstring& Path, const wstring& ImgTag);
	virtual void Update() override;
	virtual void Render() override;
	virtual void Release() override;
	virtual _vec3 UpdateRay(const _vec3* vMouseRay, const _vec3* vCameraPos, _float* dist);
	void SetWorld();
	virtual void SetTexture(const wstring& Path, const wstring& ImgTag);
public:
	static CCube* Create(LPDIRECT3DDEVICE9 pGraphic_Device, _vec3& Pos, _vec3& Scale, const wstring& Path, const wstring& ImgTag);
private:

	LPDIRECT3DCUBETEXTURE9 m_pCubeTexture;

};


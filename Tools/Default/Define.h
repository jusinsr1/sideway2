#pragma once
#define NULL_CHECK_VOID(PTR) if(nullptr == PTR) return;
#define NULL_CHECK_VAL(PTR, RETURN_VAL) if(nullptr == PTR) return RETURN_VAL;

#define ERR_MSG(message)						\
MessageBox(g_hWnd, message, L"Error!", MB_OK);

#define NO_COPY(ClassName)						\
private:										\
	ClassName(const ClassName& _Obj);			\
	ClassName& operator=(const ClassName& _Obj);

#define DECLARE_SINGLETON(ClassName)			\
		NO_COPY(ClassName)						\
public:											\
	static ClassName* GetInstance()				\
	{											\
		if(nullptr == m_pInstance)				\
			m_pInstance = new ClassName;		\
		return m_pInstance;						\
	}											\
	void DestroyInstance()						\
	{											\
		if(m_pInstance)							\
		{										\
			delete m_pInstance;					\
			m_pInstance = nullptr;				\
		}										\
	}											\
private:										\
	static ClassName*	m_pInstance;			

#define IMPLEMENT_SINGLETON(ClassName)			\
ClassName* ClassName::m_pInstance = nullptr;

#define GET_INSTANCE(ClassName)					\
ClassName::GetInstance()

#define WINCX 1280
#define WINCY 720
#define MAX_STR 256
#define MIN_STR 64
#define ERR_MSG(message)						\
MessageBox(g_hWnd, message, L"Error!", MB_OK);

typedef struct tagCamera_Desc
{
	D3DXVECTOR3		vEye;
	D3DXVECTOR3		vAt;
	D3DXVECTOR3		vAxisY;
}CAMERADESC;

typedef struct tagProjection_Camera_Desc
{
	float			fFovY;
	float			fAspect;
	float			fNear;
	float			fFar;
}PROJDESC;

typedef struct tagVertex_Texture
{
	D3DXVECTOR3			vPosition;
	D3DXVECTOR2			vTexUV;
}VTXTEX;

typedef struct tagVertex_Cube
{
	D3DXVECTOR3 vPosition;
	D3DXVECTOR3 vTexture;
}VTXCUBE;


typedef struct tagVertex_Color
{
	D3DXVECTOR3			vPosition;
	DWORD				dwColor;
}VTXCOL;

typedef struct tagPolygon16
{
	unsigned short		_0, _1, _2;
}POLYGON16;

typedef struct tagPolygon32
{
	unsigned long		_0, _1, _2;
}POLYGON32;

typedef struct tagImgPathInfo
{
	wstring wstrObjKey;
	wstring wstrStateKey;
	wstring wstrPath;
	int		iCount;
}IMGPATHINFO;
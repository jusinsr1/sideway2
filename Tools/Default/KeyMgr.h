#pragma once
class CKeyMgr
{
	DECLARE_SINGLETON(CKeyMgr)

private:
	CKeyMgr();
	~CKeyMgr();

public:
	void Update();
	_bool KeyUp(DWORD dwKey);
	_bool KeyDown(DWORD dwKey);
	_bool KeyPressing(DWORD dwKey);
	_bool KeyCombine(DWORD dwFirstKey, DWORD dwSecondKey);
	_bool GetOnView() { return m_IsOnView; }
	void IsMouseOnView(POINT pt);
private:
	DWORD m_dwKey;
	DWORD m_dwKeyPressed;
	DWORD m_dwKeyDown;
	_bool m_IsOnView = false;
};


#pragma once
#include "Obj.h"
class CMapObject :	public CObj
{
public:
	explicit CMapObject(LPDIRECT3DDEVICE9 m_pGraphic_Device);
	virtual ~CMapObject();

	virtual void Ready_Object();
	virtual void Update() override;
	virtual void Render() override;
	virtual void Release() override;
	virtual _vec3 UpdateRay(const _vec3* vMouseRay, const _vec3* vCameraPos, _float* dist);
	_vec3& Get_Scale() { return m_Scale; }
	_vec3& Get_Position() { return m_Position; }
protected:
	LPDIRECT3DVERTEXBUFFER9		m_pVB = nullptr;
	LPDIRECT3DINDEXBUFFER9		m_pIB = nullptr;

	_uint						m_iStride = 0;
	_uint						m_iNumVertices;
	_ulong						m_dwFVF = 0;
	_uint						m_iPolygonSize = 0;
	_uint						m_iNumPolygons = 0;
	D3DFORMAT					m_eFormat = D3DFORMAT(0);

	
	_vec3*			m_pPosition = nullptr;

};


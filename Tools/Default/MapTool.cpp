// MapTool.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Tools.h"
#include "MapTool.h"
#include "afxdialogex.h"
#include "MainFrm.h"
#include "Cube.h"


// CMapTool 대화 상자입니다.

IMPLEMENT_DYNAMIC(CMapTool, CPropertyPage)

CMapTool::CMapTool()
	: CPropertyPage(IDD_MAPTOOL)
	, m_ToolState(_T(""))
	, m_iHeight(0)
{

}

CMapTool::~CMapTool()
{
}

void CMapTool::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COMBO2, m_Tex3DCombo);
	DDX_Text(pDX, IDC_EDIT1, m_3DPosition.x);
	DDX_Text(pDX, IDC_EDIT3, m_3DPosition.y);
	DDX_Text(pDX, IDC_EDIT4, m_3DPosition.z);
	DDX_Text(pDX, IDC_EDIT5, m_3DScale.x);
	DDX_Text(pDX, IDC_EDIT6, m_3DScale.y);
	DDX_Text(pDX, IDC_EDIT7, m_3DScale.z);
	DDX_Control(pDX, IDC_RADIO1, m_Type3DRadio[0]);
	DDX_Control(pDX, IDC_RADIO2, m_Type3DRadio[1]);
	DDX_Control(pDX, IDC_RADIO3, m_Type3DRadio[2]);
	DDX_Control(pDX, IDC_RADIO6, m_Type3DRadio[3]);
	DDX_Control(pDX, IDC_RADIO4, m_2DRadio);
	DDX_Text(pDX, IDC_EDIT8, m_ToolState);
	DDX_Control(pDX, IDC_COMBO3, m_Tex2DCombo);
}


BEGIN_MESSAGE_MAP(CMapTool, CPropertyPage)
	ON_BN_CLICKED(IDC_RADIO1, &CMapTool::OnBnClicked3DTypeChange)
	ON_BN_CLICKED(IDC_RADIO2, &CMapTool::OnBnClicked3DTypeChange)
	ON_BN_CLICKED(IDC_RADIO3, &CMapTool::OnBnClicked3DTypeChange)
	ON_BN_CLICKED(IDC_RADIO6, &CMapTool::OnBnClicked3DTypeChange)
	ON_WM_MOUSEMOVE()
	ON_BN_CLICKED(IDC_BUTTON1, &CMapTool::OnBnClickedDraw)
	ON_BN_CLICKED(IDC_BUTTON2, &CMapTool::OnBnClickedChange)
	ON_BN_CLICKED(IDC_BUTTON4, &CMapTool::OnBnClickedSaveData)
	ON_BN_CLICKED(IDC_BUTTON3, &CMapTool::OnBnClickedLoad)
END_MESSAGE_MAP()


// CMapTool 메시지 처리기입니다.



BOOL CMapTool::OnInitDialog()
{
	CPropertyPage::OnInitDialog();

	CMainFrame* pMainFrm = dynamic_cast<CMainFrame*>(AfxGetMainWnd());
	m_pBaseForm = dynamic_cast<CBaseForm*>(pMainFrm->m_SecondSplt.GetPane(0, 0));
	

	if (FAILED(LoadTexture(L"../../Data/2DMapTexInfo.txt", 0)))
		return false;
	if(FAILED(LoadTexture(L"../../Data/3DMapTexInfo.txt", 1)))
		return false;
	m_Tex2DCombo.SetCurSel(0);
	m_Tex3DCombo.SetCurSel(0);
	
	m_pSaveLoad = GET_INSTANCE(CSaveLoad_Manager);
	m_Type3DRadio[0].SetCheck(true);
	m_2DRadio.SetCheck(true);

	m_ToolState.SetString(L"STATE_NORMAL");
	UpdateData(false);

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.

}

void CMapTool::OnBnClicked3DTypeChange()
{
	if (m_Type3DRadio[0].GetCheck())
		m_3DType = Building;
	else if (m_Type3DRadio[1].GetCheck())
		m_3DType = Box;
	else if (m_Type3DRadio[2].GetCheck())
		m_3DType = Spring;
	else if (m_Type3DRadio[3].GetCheck())
		m_3DType = Others;
}




void CMapTool::OnBnClickedDraw()
{
	if (m_iMapToolState == STATE_NORMAL)
	{
		if (m_2DRadio.GetCheck())
			m_iMapToolState = STATE_DRAW2D;
		else
			m_iMapToolState = STATE_DRAW3D;
	}
	StateCheck();
	UpdateData(FALSE);
}

void CMapTool::Update()
{
	if (this == nullptr)
		return;
	UpdateData(TRUE);
		
	KeyCheck();



	UpdateData(FALSE);
}

void CMapTool::KeyCheck()
{
	if (m_iMapToolState == STATE_DRAW3D)
	{
		_vec3 MousePos = m_pBaseForm->GetMousePos();
		wstring ImgTag, Path;

		if (GET_INSTANCE(CKeyMgr)->KeyDown(KEY_LBUTTON))
		{
			ImgTag = GetComboCurSel(true);
			auto& iter = m_map3D.find(ImgTag);
			if (iter == m_map3D.end())
				return;
			Path = iter->second;

			m_DrawPosition = MousePos;
			GET_INSTANCE(CSaveLoad_Manager)->Add3DCube(m_3DType, m_pSelObj = CCube::Create(GET_INSTANCE(CDevice)->GetDevice(), m_DrawPosition, m_3DScale, Path, ImgTag));
			m_3DPosition = *m_pSelObj->Get_StateInfo(CObj::STATE_POSITION);
			wstring  ImgTag= m_pSelObj->Get_ImgTag();
			int i = 0;
			for (auto& iter : m_map3D)
			{
				if (iter.second == ImgTag)
					break;
				i++;
			}
			m_Tex3DCombo.SetCurSel(i);
			m_iMapToolState = STATE_3DPICKING;
			StateCheck();
		}
	}

	if (GET_INSTANCE(CKeyMgr)->KeyDown(KEY_RBUTTON))
	{
		m_3DPosition = { 0.f,0.f,0.f };
		m_3DScale = { 1.f,1.f,1.f };
		m_pSelObj = nullptr;
		m_iMapToolState = STATE_NORMAL;
	}
	if (m_iMapToolState == STATE_NORMAL)
	{
		if (GET_INSTANCE(CKeyMgr)->KeyDown(KEY_F1))
			OnBnClickedDraw();
		if (GET_INSTANCE(CKeyMgr)->GetOnView() && GET_INSTANCE(CKeyMgr)->KeyDown(KEY_LBUTTON))
		{
			m_pSelObj = GET_INSTANCE(CSaveLoad_Manager)->GetSelObj();
			if (m_pSelObj != nullptr)
			{
				m_iMapToolState = STATE_3DPICKING;
				m_3DPosition = *m_pSelObj->Get_StateInfo(CObj::STATE_POSITION);
				m_3DScale = m_pSelObj->Get_Scale();
			}
		}
	}
	if (m_iMapToolState == STATE_3DPICKING)
		if (GET_INSTANCE(CKeyMgr)->KeyDown(KEY_F2))
		{
			GET_INSTANCE(CSaveLoad_Manager)->DeleteObj(m_pSelObj);
			m_pSelObj = nullptr;
			m_iMapToolState = STATE_NORMAL;
		}


	StateCheck();
}

void CMapTool::StateCheck()
{
	if (m_iMapToolState == STATE_NORMAL)
		m_ToolState.SetString(L"STATE_NORMAL");
	else if (m_iMapToolState == STATE_DRAW2D)
		m_ToolState.SetString(L"STATE_DRAW2D");
	else if (m_iMapToolState == STATE_DRAW3D)
		m_ToolState.SetString(L"STATE_DRAW3D");
	else if (m_iMapToolState == STATE_2DPICKING)
		m_ToolState.SetString(L"STATE_2DPICKING");
	else if (m_iMapToolState == STATE_3DPICKING)
		m_ToolState.SetString(L"STATE_3DPICKING");
	UpdateData(FALSE);
}

CString CMapTool::GetComboCurSel(_bool Is2D3D)
{
	CString StateKey;
	_uint index;
	if (Is2D3D)
	{
		index = m_Tex3DCombo.GetCurSel();
		m_Tex3DCombo.GetLBText(index, StateKey);
	}
	else
	{
		index = m_Tex2DCombo.GetCurSel();
		m_Tex2DCombo.GetLBText(index, StateKey);
	}return StateKey;
}

HRESULT CMapTool::LoadTexture(const wstring & wstrPath, _bool State)
{
	wifstream fin;

	fin.open(wstrPath);

	if (fin.fail())
		return E_FAIL;

	TCHAR szBuf[MAX_STR] = L"";
	IMGPATHINFO tImgPath;

	while (true)
	{
		fin.getline(szBuf, MAX_STR, '|');
		tImgPath.wstrObjKey = szBuf;

		fin.getline(szBuf, MAX_STR, '|');
		tImgPath.wstrStateKey = szBuf;

		fin.getline(szBuf, MAX_STR, '|');
		tImgPath.iCount = _wtoi(szBuf);

		fin.getline(szBuf, MAX_STR);
		tImgPath.wstrPath = szBuf;

		if (fin.eof())
			break;

		if (State == 0)
		{
			m_map2D.insert({ tImgPath.wstrStateKey, tImgPath.wstrPath });
			m_Tex2DCombo.AddString(tImgPath.wstrStateKey.c_str());
		}
		if (State == 1)
		{
			m_map3D.insert({ tImgPath.wstrStateKey, tImgPath.wstrPath });
			m_Tex3DCombo.AddString(tImgPath.wstrStateKey.c_str());
		}
		GET_INSTANCE(CSaveLoad_Manager)->AddTexData(tImgPath.wstrStateKey, tImgPath.wstrPath);
	}

	fin.close();

	return S_OK;
}

void CMapTool::OnBnClickedChange()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if (m_iMapToolState == STATE_3DPICKING)
	{
		if (m_pSelObj == nullptr)
			return;
		m_pSelObj->Set_Scale(m_3DScale);
		m_pSelObj->Set_Position(m_3DPosition);
		
		wstring ImgTag = GetComboCurSel(true);
		auto& iter = m_map3D.find(ImgTag);
		if (iter == m_map3D.end())
			return;
		wstring Path = iter->second;


		dynamic_cast<CCube*>(m_pSelObj)->SetTexture(Path,ImgTag);
	}
	
}


void CMapTool::OnBnClickedSaveData()
{
	UpdateData(TRUE);

	CFileDialog Dlg(FALSE, L"txt", L"제목없음.txt", OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,
		L"Txt Files(*.txt)|*.txt||", this);

	TCHAR szCurPath[MAX_STR] = L"";

	GetCurrentDirectory(MAX_STR, szCurPath);

	PathRemoveFileSpec(szCurPath);
	PathRemoveFileSpec(szCurPath);
	lstrcat(szCurPath, L"\\Data");
	Dlg.m_ofn.lpstrInitialDir = szCurPath;
	if (IDOK == Dlg.DoModal())
	{
		wstring  Path = Dlg.GetPathName().GetString();
		GET_INSTANCE(CSaveLoad_Manager)->Save3DObject(Path, !(m_2DRadio.GetCheck()));
	}
}





void CMapTool::OnBnClickedLoad()
{
	CFileDialog Dlg(TRUE, L"txt", L"제목없음.txt", OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,
		L"Txt Files(*.txt)|*.txt||", this);
	TCHAR szCurPath[MAX_STR] = L"";

	GetCurrentDirectory(MAX_STR, szCurPath);
	PathRemoveFileSpec(szCurPath);
	lstrcat(szCurPath, L"\\Data");
	Dlg.m_ofn.lpstrInitialDir = szCurPath;	// 절대경로!
	if (IDOK == Dlg.DoModal())
	{
		wstring  Path = Dlg.GetPathName().GetString();
		GET_INSTANCE(CSaveLoad_Manager)->Load3DObject(Path, !(m_2DRadio.GetCheck()));
	}
}

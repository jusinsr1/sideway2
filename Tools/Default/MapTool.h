#pragma once
#include "afxwin.h"
#include "BaseForm.h"

// CMapTool 대화 상자입니다.

class CMapTool : public CPropertyPage
{
	DECLARE_DYNAMIC(CMapTool)

public:
	CMapTool();
	virtual ~CMapTool();

	// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_MAPTOOL };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	
	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	enum ToolState { STATE_NORMAL, STATE_DRAW2D, STATE_DRAW3D, STATE_2DPICKING, STATE_3DPICKING, STATE_END };
private:
	CSaveLoad_Manager* m_pSaveLoad = nullptr;
public:
	afx_msg void OnBnClickedSaveData();
	afx_msg void OnBnClicked3DTypeChange();
	afx_msg void OnBnClickedChange();
	afx_msg void OnBnClickedDraw();
	afx_msg void OnBnClickedLoad();
	void Update();
	void KeyCheck();
	void StateCheck();
	CString GetComboCurSel(_bool Is2D3D);
public:

	HRESULT LoadTexture(const wstring& wstrPath, _bool State);
	map<wstring, wstring> m_map2D;
	map<wstring, wstring> m_map3D;
	
	CButton m_Type3DRadio[4];
	Object3D m_3DType = Building;
	CComboBox m_Tex3DCombo;
	
	CButton m_2DRadio;
	CComboBox m_Tex2DCombo;
	ToolState m_iMapToolState = STATE_NORMAL;
	CString m_ToolState;
	
	_vec3 m_3DPosition = { 0,0,0 };
	_vec3 m_3DScale = { 1,1,1 };
	
	_vec3 m_DrawPosition;
	int m_iHeight;
private:
	CBaseForm* m_pBaseForm = nullptr;
	CObj* m_pSelObj = nullptr;
};

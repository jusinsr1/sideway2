#pragma once



// CMySheet
#include "MapTool.h"
#include "ObjectTool.h"
#include "PlaneTool.h"
class CMySheet : public CPropertySheet
{
	DECLARE_DYNAMIC(CMySheet)

public:
	CMySheet();
	CMySheet(UINT nIDCaption, CWnd* pParentWnd = NULL, UINT iSelectPage = 0);
	CMySheet(LPCTSTR pszCaption, CWnd* pParentWnd = NULL, UINT iSelectPage = 0);
	virtual ~CMySheet();

protected:
	DECLARE_MESSAGE_MAP()
public:
	CMapTool	m_MapTool;
	CPlaneTool	m_PlaneTool;
	CObjectTool m_ObjectTool;
};
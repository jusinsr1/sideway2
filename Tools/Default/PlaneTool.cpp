// PlaneTool.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Tools.h"
#include "PlaneTool.h"
#include "afxdialogex.h"


// CPlaneTool 대화 상자입니다.

IMPLEMENT_DYNAMIC(CPlaneTool, CPropertyPage)

CPlaneTool::CPlaneTool()
	: CPropertyPage(IDD_PLANETOOL)
{

}

CPlaneTool::~CPlaneTool()
{
}

void CPlaneTool::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT12, m_PlaneInfo.arrMoveablePlane[0]);
	DDX_Text(pDX, IDC_EDIT9, m_PlaneInfo.arrMoveablePlane[1]);
	DDX_Text(pDX, IDC_EDIT11, m_PlaneInfo.arrMoveablePlane[2]);
	DDX_Text(pDX, IDC_EDIT10, m_PlaneInfo.arrMoveablePlane[3]);
	DDX_Control(pDX, IDC_LIST1, m_AllPlaneListBox);
	DDX_Text(pDX, IDC_EDIT6, m_PlaneInfo.Pos.x);
	DDX_Text(pDX, IDC_EDIT7, m_PlaneInfo.Pos.y);
	DDX_Text(pDX, IDC_EDIT8, m_PlaneInfo.Pos.z);
	DDX_Text(pDX, IDC_EDIT1, m_PlaneInfo.fWidth);
	DDX_Text(pDX, IDC_EDIT3, m_PlaneInfo.fHeight);
	DDX_Control(pDX, IDC_LIST3, m_GamePlaneListBox);
}


BEGIN_MESSAGE_MAP(CPlaneTool, CPropertyPage)
	ON_BN_CLICKED(IDC_BUTTON2, &CPlaneTool::OnBnClickedSetData)
	ON_LBN_SELCHANGE(IDC_LIST1, &CPlaneTool::OnLbnSelchangeAllPlaneListBox)
	ON_BN_CLICKED(IDC_BUTTON7, &CPlaneTool::OnBnClickedChange)
	ON_BN_CLICKED(IDC_BUTTON1, &CPlaneTool::OnBnClickedMoveToGameList)
	ON_BN_CLICKED(IDC_BUTTON8, &CPlaneTool::OnBnClickedDelete)
	ON_BN_CLICKED(IDC_BUTTON5, &CPlaneTool::OnBnClickedSave)
	ON_BN_CLICKED(IDC_BUTTON6, &CPlaneTool::OnBnClickedLoad)
END_MESSAGE_MAP()


// CPlaneTool 메시지 처리기입니다.


void CPlaneTool::OnBnClickedSetData()
{
	GET_INSTANCE(CSaveLoad_Manager)->SetAllPlane(&m_mapAllPlane);
	for (auto& iter : m_mapAllPlane)
	{
		wstring Plane = to_wstring(iter.first / 4) + L"-" + to_wstring(iter.first % 4)+ L"(" + to_wstring(iter.first) + L")";
		m_AllPlaneListBox.AddString(Plane.c_str());
	}
	if (m_mapAllPlane.size() != 0)
	{
		m_AllPlaneListBox.SetCurSel(0);
		OnLbnSelchangeAllPlaneListBox();
	}
}


void CPlaneTool::OnLbnSelchangeAllPlaneListBox()
{
	int SelNum = m_AllPlaneListBox.GetCurSel();
	auto& iter = m_mapAllPlane.find(SelNum);
	m_PlaneInfo = iter->second;
	UpdateData(false);
}


BOOL CPlaneTool::OnInitDialog()
{
	CPropertyPage::OnInitDialog();
	for (size_t i = 0; i < 4; i++)
	{
		m_PlaneInfo.arrMoveablePlane[i] = -1;
	}
	m_PlaneInfo.fHeight = 0;
	m_PlaneInfo.fWidth = 0;
	m_PlaneInfo.iPlaneNum = -1;
	m_PlaneInfo.Pos = { 0, 0, 0 };
	UpdateData(false);
	return TRUE;
}


void CPlaneTool::OnBnClickedChange()
{
	if (m_mapAllPlane.size() == 0)
		return;
	UpdateData(true);
	int SelNum = m_AllPlaneListBox.GetCurSel();
	auto& iter = m_mapAllPlane.find(SelNum);

	for (size_t i = 0; i < 4; i++)
		iter->second.arrMoveablePlane[i] = m_PlaneInfo.arrMoveablePlane[i];
}


void CPlaneTool::OnBnClickedMoveToGameList()
{
	if (m_mapAllPlane.size() == 0)
		return;

	int SelNum = m_AllPlaneListBox.GetCurSel();
	auto& iter = m_mapAllPlane.find(SelNum);
	m_mapGamePlane.insert({ iter->first,iter->second });
	wstring Plane = to_wstring(iter->first / 4)+ L"-" + to_wstring(iter->first % 4) + L"(" + to_wstring(iter->first) + L")";
	m_GamePlaneListBox.AddString(Plane.c_str());
}


void CPlaneTool::OnBnClickedDelete()
{
	int SelNum = m_GamePlaneListBox.GetCurSel();
	
	if (SelNum < 0)
		return;
	auto& iter = m_mapGamePlane.begin();
	for (int i = 0; i < SelNum; i++)
		iter = iter++;
	m_mapGamePlane.erase(iter);
	m_GamePlaneListBox.DeleteString(SelNum);
}


void CPlaneTool::OnBnClickedSave()
{
	CFileDialog Dlg(FALSE, L"txt", L"제목없음.txt", OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,
		L"Txt Files(*.txt)|*.txt||", this);

	TCHAR szCurPath[MAX_STR] = L"";

	GetCurrentDirectory(MAX_STR, szCurPath);

	PathRemoveFileSpec(szCurPath);
	PathRemoveFileSpec(szCurPath);
	lstrcat(szCurPath, L"\\Data\\Plane");
	Dlg.m_ofn.lpstrInitialDir = szCurPath;

	if (IDOK == Dlg.DoModal())
	{
		wofstream fout;
		fout.open(Dlg.GetPathName().GetString());

		if (fout.fail())
			return;

		wstring wstrCombined = L"";
		TCHAR szBuffer[MIN_STR] = L"";

		for (auto& iter : m_mapGamePlane)
		{
			wstrCombined = to_wstring(iter.first);
			wstrCombined = wstrCombined + L"|" + to_wstring(iter.second.fWidth);
			wstrCombined = wstrCombined + L"|" + to_wstring(iter.second.fHeight);
			wstrCombined = wstrCombined + L"|" + to_wstring(iter.second.Pos.x);
			wstrCombined = wstrCombined + L"|" + to_wstring(iter.second.Pos.y);
			wstrCombined = wstrCombined + L"|" + to_wstring(iter.second.Pos.z);
			wstrCombined = wstrCombined + L"|" + to_wstring(iter.second.arrMoveablePlane[0]);
			wstrCombined = wstrCombined + L"|" + to_wstring(iter.second.arrMoveablePlane[1]);
			wstrCombined = wstrCombined + L"|" + to_wstring(iter.second.arrMoveablePlane[2]);
			wstrCombined = wstrCombined + L"|" + to_wstring(iter.second.arrMoveablePlane[3]);
			fout << wstrCombined << endl;
		}

		fout.close();
	}
}


void CPlaneTool::OnBnClickedLoad()
{
	CFileDialog Dlg(TRUE, L"txt", L"제목없음.txt", OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,
		L"Txt Files(*.txt)|*.txt||", this);
	TCHAR szCurPath[MAX_STR] = L"";

	GetCurrentDirectory(MAX_STR, szCurPath);
	PathRemoveFileSpec(szCurPath);
	lstrcat(szCurPath, L"\\Data\\Plane");
	Dlg.m_ofn.lpstrInitialDir = szCurPath;	// 절대경로!
	if (IDOK == Dlg.DoModal())
	{
		wifstream fin;

		fin.open(Dlg.GetPathName().GetString());
		if (fin.fail())
			return;
		TCHAR szBuf[MAX_STR] = L"";

		while (true)
		{
			PLANEINFO PlaneInfo;

			fin.getline(szBuf, MAX_STR, '|');
			PlaneInfo.iPlaneNum = _wtoi(szBuf);
			fin.getline(szBuf, MAX_STR, '|');
			PlaneInfo.fWidth = (float)_wtof(szBuf);
			fin.getline(szBuf, MAX_STR, '|');
			PlaneInfo.fHeight = (float)_wtof(szBuf);
			fin.getline(szBuf, MAX_STR, '|');
			PlaneInfo.Pos.x = (float)_wtof(szBuf);
			fin.getline(szBuf, MAX_STR, '|');
			PlaneInfo.Pos.y = (float)_wtof(szBuf);
			fin.getline(szBuf, MAX_STR, '|');
			PlaneInfo.Pos.z = (float)_wtof(szBuf);
			fin.getline(szBuf, MAX_STR, '|');
			PlaneInfo.arrMoveablePlane[0] = _wtoi(szBuf);
			fin.getline(szBuf, MAX_STR, '|');
			PlaneInfo.arrMoveablePlane[1] = _wtoi(szBuf);
			fin.getline(szBuf, MAX_STR, '|');
			PlaneInfo.arrMoveablePlane[2] = _wtoi(szBuf);
			fin.getline(szBuf, MAX_STR);
			PlaneInfo.arrMoveablePlane[3] = _wtoi(szBuf);

			if (fin.eof())
				break;
			m_mapGamePlane.insert({ PlaneInfo.iPlaneNum, PlaneInfo });
			wstring Num = to_wstring(PlaneInfo.iPlaneNum / 4) + L"-" + to_wstring(PlaneInfo.iPlaneNum % 4) + L"(" + to_wstring(PlaneInfo.iPlaneNum) + L")";
			m_GamePlaneListBox.AddString(Num.c_str());
		}
		fin.close();
	}
}

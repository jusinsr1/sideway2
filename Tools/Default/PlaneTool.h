#pragma once
#include "afxwin.h"


// CPlaneTool 대화 상자입니다.

class CPlaneTool : public CPropertyPage
{
	DECLARE_DYNAMIC(CPlaneTool)

public:
	CPlaneTool();
	virtual ~CPlaneTool();

// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_PLANETOOL };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedChange();
	afx_msg void OnBnClickedMoveToGameList();
	afx_msg void OnBnClickedSetData();
	afx_msg void OnLbnSelchangeAllPlaneListBox();
	afx_msg void OnBnClickedDelete();
	afx_msg void OnBnClickedSave();
public:
	CListBox m_AllPlaneListBox;
	map<_int, PLANEINFO> m_mapAllPlane;
	map<_int, PLANEINFO> m_mapGamePlane;
	
	PLANEINFO m_PlaneInfo;
	virtual BOOL OnInitDialog();
	CListBox m_GamePlaneListBox;
	afx_msg void OnBnClickedLoad();
};

#include "stdafx.h"
#include "SaveLoad_Manager.h"
#include "Cube.h"

IMPLEMENT_SINGLETON(CSaveLoad_Manager);

CSaveLoad_Manager::CSaveLoad_Manager()
{
}

CSaveLoad_Manager::~CSaveLoad_Manager()
{
	Release();
}

void CSaveLoad_Manager::Add3DCube(Object3D ID, CObj * CObj)
{
	m_vec3D[ID].push_back(CObj);
}

void CSaveLoad_Manager::AddBaseTile(CObj* CObj)
{
	m_BaseTile = CObj;
}

void CSaveLoad_Manager::AddCamera(CCamera * CCamera)
{
	m_Camera = CCamera;
}

void CSaveLoad_Manager::AddSquare(Object2D ID, CObj * CObj)
{
	m_vec2D[ID].push_back(CObj);
}

void CSaveLoad_Manager::AddTexData(const wstring ImgTag, const wstring ImgPath)
{
	m_TextureData.insert({ ImgTag, ImgPath });
}

wstring CSaveLoad_Manager::Find_Path(const wstring & ImgTag)
{
	auto iter = m_TextureData.find(ImgTag);
	if (iter == m_TextureData.end())
		return wstring();
	return iter->second;
}

void CSaveLoad_Manager::Update()
{
	if (m_BaseTile != nullptr)
		m_BaseTile->Update();
	if (m_Camera != nullptr)
		m_Camera->Update();
	for (int i = 0; i<Object3D::_3D_END; i++)
	{
		for (auto& Object : m_vec3D[i])
		{
			Object->Update();
		}
	}

	for (int i = 0; i<Object2D::_2D_END; i++)
	{
		for (auto& Object : m_vec2D[i])
		{
			Object->Update();
		}
	}
}

_vec3 CSaveLoad_Manager::PickCheck(POINT mouse)
{
	D3DVIEWPORT9		ViewPort;
	GET_INSTANCE(CDevice)->GetDevice()->GetViewport(&ViewPort);
	GET_INSTANCE(CKeyMgr)->IsMouseOnView(mouse);
	_vec4	vMousePos;
	vMousePos.x = mouse.x / (ViewPort.Width * 0.5f) - 1.f;
	vMousePos.y = mouse.y / ((ViewPort.Height * 0.5f) * -1.f) + 1.f;
	vMousePos.z = 0.0f;
	vMousePos.w = 1.f;

	_matrix	matProj;
	GET_INSTANCE(CDevice)->GetDevice()->GetTransform(D3DTS_PROJECTION, &matProj);
	D3DXMatrixInverse(&matProj, nullptr, &matProj);
	D3DXVec4Transform(&vMousePos, &vMousePos, &matProj);
	

	_vec3		vCameraPos(0.f, 0.f, 0.f);

	_vec3		vMouseRay = _vec3(vMousePos.x, vMousePos.y, vMousePos.z) - vCameraPos;

	_matrix	matView;
	GET_INSTANCE(CDevice)->GetDevice()->GetTransform(D3DTS_VIEW, &matView);

	D3DXMatrixInverse(&matView, nullptr, &matView);
	D3DXVec3TransformCoord(&vCameraPos, &vCameraPos, &matView);
	D3DXVec3TransformNormal(&vMouseRay, &vMouseRay, &matView);

	_vec3 Picking = { -1, -1, -1 };
	_float dist = 0;

	CObj* PickObj = nullptr;
	Picking = m_BaseTile->UpdateRay(&vMouseRay, &vCameraPos, &dist);
	
	for (int i = 0; i<Object3D::_3D_END; i++)
	{
		for (auto& Object : m_vec3D[i])
		{
			_vec3 TempPicking = { -1,-1,-1 };
			TempPicking = Object->UpdateRay(&vMouseRay, &vCameraPos, &dist);
			if (TempPicking.x != -1)
			{
				PickObj = Object;
				Picking = TempPicking;
			}
		}
	}
	m_SelectObj = PickObj;
	return Picking;
}

void CSaveLoad_Manager::Render()
{
	if (m_BaseTile != nullptr)
		m_BaseTile->Render();
	if (m_Camera != nullptr)
		m_Camera->Render();
	for (int i = 0; i<Object3D::_3D_END; i++)
		for (auto& Object : m_vec3D[i])
			Object->Render();

	for (int i = 0; i<Object2D::_2D_END; i++)
		for (auto& Object : m_vec2D[i])
			Object->Render();
}

void CSaveLoad_Manager::Release()
{
	SafeDelete(m_BaseTile);
	SafeDelete(m_Camera);

	for (int i = 0; i < Object3D::_3D_END; i++)
	{
		for (auto& Object : m_vec3D[i])
			SafeDelete(Object);
		m_vec3D[i].clear();
	}
	for (int i = 0; i < Object2D::_2D_END; i++)
	{
		for (auto& Object : m_vec2D[i])
			SafeDelete(Object);
		m_vec2D[i].clear();
	}
}

void CSaveLoad_Manager::DeleteObj(CObj * CObj)
{
	for (int i = 0; i < Object3D::_3D_END; i++)
	{
		int j = 0;
		for (auto& Object : m_vec3D[i])
		{
			if (Object == CObj)
			{
				SafeDelete(Object);
				m_vec3D[i].erase(m_vec3D->begin() + j);
				return;
			}
			j++;
		}
	}
}

void CSaveLoad_Manager::Save3DObject(const wstring &wstrPath, _bool State)
{
		wofstream fout;
		fout.open(wstrPath);
		if (fout.fail())
			return;

		for (int i = 0; i < Object3D::_3D_END; i++)
		{
			for (auto& Object : m_vec3D[i])
			{
				wstring SaveData = L"";
				SaveData = Object->Save3DObj() + L"|" + to_wstring(i);
				fout << SaveData << endl;
			}
		}
	fout.close();
}

void CSaveLoad_Manager::Load3DObject(const wstring &wstrPath, _bool State)
{
	wifstream fin;

	fin.open(wstrPath);

	if (fin.fail())
		return;

	TCHAR szBuf[MAX_STR] = L"";
	

	while (true)
	{
		if (State == true)
		{
			_vec3 Position;
			_vec3 Scale;
			wstring wstrImgTag;
			wstring wstrImgPath;
			Object3D Type3D;
			fin.getline(szBuf, MAX_STR, '|');
			wstrImgTag = szBuf;

			wstrImgPath = Find_Path(wstrImgTag);

			fin.getline(szBuf, MAX_STR, '|');
			Position.x = (float)_wtof(szBuf);
			fin.getline(szBuf, MAX_STR, '|');
			Position.y = (float)_wtof(szBuf);
			fin.getline(szBuf, MAX_STR, '|');
			Position.z = (float)_wtof(szBuf);

			fin.getline(szBuf, MAX_STR, '|');
			Scale.x = (float)_wtof(szBuf);
			fin.getline(szBuf, MAX_STR, '|');
			Scale.y = (float)_wtof(szBuf);
			fin.getline(szBuf, MAX_STR, '|');
			Scale.z = (float)_wtof(szBuf);

			fin.getline(szBuf, MAX_STR);
			Type3D = (Object3D)_wtoi(szBuf);

			if (fin.eof())
				break;

			Add3DCube(Type3D, CCube::Create(GET_INSTANCE(CDevice)->GetDevice(), Position, Scale, wstrImgPath, wstrImgTag));
		}
		if (State == false)
		{
			//2D Load
		}
	}

	fin.close();
}

void CSaveLoad_Manager::SetAllPlane(map<_int, PLANEINFO>* m_mapPlaneData)
{
	m_mapPlaneData->clear();
	int j = 0;
	for (auto& Object : m_vec3D[Building])
	{
		PLANEINFO PlaneData;
		for (size_t i = 0; i < 4; i++)
		{
			PlaneData = Object->Get_PlaneInfo(i, j);
			m_mapPlaneData->insert({ PlaneData.iPlaneNum, PlaneData });
		}
		j++;
	}
}
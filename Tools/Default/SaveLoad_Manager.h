#pragma once

#include "Obj.h"
#include "Camera.h"

class CObj;
class CCamera;
class CSaveLoad_Manager final
{
	DECLARE_SINGLETON(CSaveLoad_Manager);
	

private:
	explicit CSaveLoad_Manager();
	virtual ~CSaveLoad_Manager();
public:
	void AddBaseTile(CObj* CObj);
	void AddCamera(CCamera* CObj);
	void Add3DCube(Object3D ID, CObj* CObj);
	void AddSquare(Object2D ID, CObj * CObj);
	void AddTexData(const wstring ImgTag, const wstring ImgPath);
	wstring Find_Path(const wstring& ImgTag);
	CObj* GetSelObj() { return m_SelectObj; }
	CCamera* GetCamera() { return m_Camera; }
	void Update();
	_vec3 PickCheck(POINT mouse);
	void Render();
	void Release();

	void DeleteObj(CObj* CObj);

	void Save3DObject(const wstring &wstrPath, _bool State);
	void Load3DObject(const wstring &wstrPath, _bool State);

	void SetAllPlane(map<_int, PLANEINFO>* m_mapPlaneData);
private:
	CObj* m_BaseTile = nullptr;
	CCamera* m_Camera = nullptr;

	vector<CObj*> m_vec3D[_3D_END];
	vector<CObj*> m_vec2D[_2D_END];
	map<wstring, wstring> m_TextureData;
	CObj* m_SelectObj = nullptr;
};


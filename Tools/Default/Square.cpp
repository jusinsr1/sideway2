#include "stdafx.h"
#include "Square.h"


CSquare::CSquare(LPDIRECT3DDEVICE9 pGraphic_Device)
	:CMapObject(pGraphic_Device)
{
}


CSquare::~CSquare()
{
}

void CSquare::Ready_Square(const _vec3 Scale, const _vec3 Pos, wstring* Path)
{
	if (Path != nullptr)
	{
//		D3DXCreateTextureFromFile(m_pGraphic_Device, m_TexturePath.c_str(), &m_pTexture);
	}
	D3DXMatrixIdentity(&m_matWorld);

	m_iNumVertices = 4;
	m_iStride = sizeof(VTXTEX);
	m_dwFVF = D3DFVF_XYZ | D3DFVF_TEX1;

	m_iNumPolygons = 2;
	//m_pPosition[m_iNumVertices] = new 
	m_iPolygonSize = sizeof(POLYGON16);
	m_eFormat = D3DFMT_INDEX16;

	CMapObject::Ready_Object();

	VTXTEX*		pVertices = nullptr;

	m_pVB->Lock(0, 0, (void**)&pVertices, 0);
	pVertices[0].vPosition = _vec3(-0.5f, 0.5f, 0.f);
	pVertices[0].vTexUV = _vec2(0.f, 0.f);

	pVertices[1].vPosition = _vec3(0.5f, 0.5f, 0.f);
	pVertices[1].vTexUV = _vec2(1.f, 0.f);

	pVertices[2].vPosition = _vec3(0.5f, -0.5f, 0.f);
	pVertices[2].vTexUV = _vec2(1.f, 1.f);

	pVertices[3].vPosition = _vec3(-0.5f, -0.5f, 0.f);
	pVertices[3].vTexUV = _vec2(0.f, 1.f);

	m_pVB->Unlock();

	POLYGON16*	pIndices = nullptr;
	m_pIB->Lock(0, 0, (void**)&pIndices, 0);

	pIndices[0]._0 = 0;
	pIndices[0]._1 = 1;
	pIndices[0]._2 = 2;

	pIndices[1]._0 = 0;
	pIndices[1]._1 = 2;
	pIndices[1]._2 = 3;

	m_pIB->Unlock();
}

void CSquare::Update()
{
}

void CSquare::Render()
{
	m_pGraphic_Device->SetTexture(0, m_pTexture);
	m_pGraphic_Device->SetTransform(D3DTS_WORLD, &m_matWorld);
	m_pGraphic_Device->SetStreamSource(0, m_pVB, 0, m_iStride);
	m_pGraphic_Device->SetFVF(m_dwFVF);
	m_pGraphic_Device->SetIndices(m_pIB);
	m_pGraphic_Device->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, m_iNumVertices, 0, m_iNumPolygons);
}

void CSquare::Release()
{
}

_vec3 CSquare::UpdateRay(const _vec3 * vMouseRay, const _vec3 * vCameraPos, _float * dist)
{
	return _vec3();
}

void CSquare::matWorldChange()
{
	CObj::Set_StateInfo(CObj::STATE_POSITION, &m_vecPos);
	CObj::Scaling(m_vecScale);
}

CSquare * CSquare::Create(LPDIRECT3DDEVICE9 pGraphic_Device, const _float Width, const _float Height, const _vec3 Pos, wstring* Path)
{
	CSquare* pInstance = new CSquare(pGraphic_Device);
	//pInstance->Ready_Square(Width, Height, Pos, Path);

	return pInstance;
}

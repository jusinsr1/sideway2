// TexturePath.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Tools.h"
#include "TexturePath.h"
#include "afxdialogex.h"
#include "FileInfo.h"

// CTexturePath 대화 상자입니다.

IMPLEMENT_DYNAMIC(CTexturePath, CDialog)

CTexturePath::CTexturePath(CWnd* pParent /*=NULL*/)
	: CDialog(IDD_TEXTUREPATH, pParent)
{
}

CTexturePath::~CTexturePath()
{
	for_each(m_ImgInfoLst.begin(), m_ImgInfoLst.end(), SafeDelete<IMGPATHINFO*>);
	m_ImgInfoLst.clear();
}

void CTexturePath::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST2, m_ListBox);
	DDX_Control(pDX, IDC_CHECK1, m_IsPng);
}


BEGIN_MESSAGE_MAP(CTexturePath, CDialog)
	ON_WM_DROPFILES()
	ON_BN_CLICKED(IDC_BUTTON1, &CTexturePath::OnBnClickedSaveFiles)
END_MESSAGE_MAP()


// CTexturePath 메시지 처리기입니다.


void CTexturePath::OnDropFiles(HDROP hDropInfo)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	CDialog::OnDropFiles(hDropInfo);

	UpdateData(TRUE);

	int iCount = DragQueryFile(hDropInfo, -1, nullptr, 0);

	TCHAR szFullPath[MAX_STR] = L"";

	_bool TF = (_bool)m_IsPng.GetCheck();
	for (int i = 0; i < iCount; ++i)
	{
		DragQueryFile(hDropInfo, i, szFullPath, MAX_STR);
		CFileInfo::DirInfoExtraction(szFullPath, m_ImgInfoLst, TF);
	}

	wstring wstrCombined = L"";

	for (auto& pImgPathInfo : m_ImgInfoLst)
	{
		wstrCombined = pImgPathInfo->wstrStateKey + L"|"
			+ to_wstring(pImgPathInfo->iCount) + L"|" + pImgPathInfo->wstrPath;

		m_ListBox.AddString(wstrCombined.c_str());
	}

	SetHorizontalScroll();

	UpdateData(FALSE);

}

void CTexturePath::SetHorizontalScroll()
{
	CString strName = L"";
	CSize	size;

	int iCX = 0;

	CDC* pDC = m_ListBox.GetDC();

	for (int i = 0; i < m_ListBox.GetCount(); ++i)
	{
		m_ListBox.GetText(i, strName);

		size = pDC->GetTextExtent(strName);

		if (size.cx > iCX)
			iCX = size.cx;
	}

	m_ListBox.ReleaseDC(pDC);

	if (iCX > m_ListBox.GetHorizontalExtent())
		m_ListBox.SetHorizontalExtent(iCX);
}




BOOL CTexturePath::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	m_IsPng.SetCheck(1);
	UpdateData(false);
	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


void CTexturePath::OnBnClickedSaveFiles()
{
	UpdateData(TRUE);

	CFileDialog Dlg(FALSE, L"txt", L"제목없음.txt", OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,
		L"Txt Files(*.txt)|*.txt||", this);

	TCHAR szCurPath[MAX_STR] = L"";

	GetCurrentDirectory(MAX_STR, szCurPath);

	PathRemoveFileSpec(szCurPath);
	PathRemoveFileSpec(szCurPath);
	lstrcat(szCurPath, L"\\Data");
	Dlg.m_ofn.lpstrInitialDir = szCurPath;

	if (IDOK == Dlg.DoModal())
	{
		wofstream fout;
		fout.open(Dlg.GetPathName().GetString());

		if (fout.fail())
			return;

		wstring wstrCombined = L"";

		for (auto& pImgPathInfo : m_ImgInfoLst)
		{
			wstrCombined = pImgPathInfo->wstrObjKey + L"|" + pImgPathInfo->wstrStateKey + L"|"
				+ to_wstring(pImgPathInfo->iCount) + L"|" + pImgPathInfo->wstrPath;

			fout << wstrCombined << endl;
		}

		fout.close();
	}

	UpdateData(FALSE);
}

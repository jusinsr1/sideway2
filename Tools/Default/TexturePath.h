#pragma once
#include "afxwin.h"


// CTexturePath 대화 상자입니다.

class CTexturePath : public CDialog
{
	DECLARE_DYNAMIC(CTexturePath)

public:
	CTexturePath(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CTexturePath();

// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_TEXTUREPATH };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnDropFiles(HDROP hDropInfo);
private:
	void SetHorizontalScroll();


public:
	list<IMGPATHINFO*> m_ImgInfoLst;

	CListBox m_ListBox;
	virtual BOOL OnInitDialog();
	CButton m_IsPng;
	afx_msg void OnBnClickedSaveFiles();
};

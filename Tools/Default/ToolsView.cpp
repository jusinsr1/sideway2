
// ToolsView.cpp : CToolsView 클래스의 구현
//

#include "stdafx.h"
// SHARED_HANDLERS는 미리 보기, 축소판 그림 및 검색 필터 처리기를 구현하는 ATL 프로젝트에서 정의할 수 있으며
// 해당 프로젝트와 문서 코드를 공유하도록 해 줍니다.
#ifndef SHARED_HANDLERS
#include "Tools.h"
#endif

#include "MainFrm.h"
#include "ToolsDoc.h"
#include "ToolsView.h"
#include "BaseTile.h"
#include "Camera.h"
#include "Square.h"
#include "BaseForm.h"
#include "Cube.h"
#include "MapTool.h"
#include "MySheet.h"
#include "PopupWnd.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CToolsView

IMPLEMENT_DYNCREATE(CToolsView, CView)

BEGIN_MESSAGE_MAP(CToolsView, CView)
	// 표준 인쇄 명령입니다.
	ON_COMMAND(ID_FILE_PRINT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CView::OnFilePrintPreview)
	ON_WM_MOUSEMOVE()
END_MESSAGE_MAP()

HWND g_hWnd;

// CToolsView 생성/소멸

CToolsView::CToolsView()
{
	// TODO: 여기에 생성 코드를 추가합니다.

}

CToolsView::~CToolsView()
{
	GET_INSTANCE(CInputDevice)->DestroyInstance();
	GET_INSTANCE(CDevice)->DestroyInstance();
	GET_INSTANCE(CSaveLoad_Manager)->DestroyInstance();
	GET_INSTANCE(CKeyMgr)->DestroyInstance();
}

BOOL CToolsView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: CREATESTRUCT cs를 수정하여 여기에서
	//  Window 클래스 또는 스타일을 수정합니다.

	return CView::PreCreateWindow(cs);
}

// CToolsView 그리기

void CToolsView::OnDraw(CDC* /*pDC*/)
{
	GET_INSTANCE(CInputDevice)->SetUp_InputState();
	GET_INSTANCE(CKeyMgr)->Update();
	FormUpdate();

	CToolsDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;
	
	GET_INSTANCE(CSaveLoad_Manager)->Update();
	CDevice::GetInstance()->Render_Begin();

	GET_INSTANCE(CSaveLoad_Manager)->Render();
	CDevice::GetInstance()->Render_End();
}


// CToolsView 인쇄

BOOL CToolsView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// 기본적인 준비
	return DoPreparePrinting(pInfo);
}

void CToolsView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 인쇄하기 전에 추가 초기화 작업을 추가합니다.
}

void CToolsView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 인쇄 후 정리 작업을 추가합니다.
}


// CToolsView 진단

#ifdef _DEBUG
void CToolsView::AssertValid() const
{
	CView::AssertValid();
}

void CToolsView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CToolsDoc* CToolsView::GetDocument() const // 디버그되지 않은 버전은 인라인으로 지정됩니다.
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CToolsDoc)));
	return (CToolsDoc*)m_pDocument;
}
#endif //_DEBUG


// CToolsView 메시지 처리기


void CToolsView::OnInitialUpdate()
{
	CView::OnInitialUpdate();
	g_hWnd = m_hWnd;
	// CWinApp::GetMainWnd함수: MainFrame 윈도우를 얻어옴.
	CMainFrame* pMainFrm = dynamic_cast<CMainFrame*>(AfxGetApp()->GetMainWnd());


	m_pBaseForm = dynamic_cast<CBaseForm*>(pMainFrm->m_SecondSplt.GetPane(0, 0));

	// GetWindowRect: 자신의 윈도우 크기를 RECT구조체로 얻어오는 함수.
	RECT rcMainFrm = {};
	pMainFrm->GetWindowRect(&rcMainFrm);

	// SetRect: RECT구조체를 설정하는 함수
	SetRect(&rcMainFrm, 0, 0,
		rcMainFrm.right - rcMainFrm.left, rcMainFrm.bottom - rcMainFrm.top);

	RECT rcView = {};
	GetClientRect(&rcView); // 현재 View윈도우의 RECT를 얻어옴.

							// Frame윈도우와 View윈도우 간의 가로, 세로 갭을 구한다.
	int iRowFrm = rcMainFrm.right - rcView.right;
	int iColFrm = rcMainFrm.bottom - rcView.bottom;

	pMainFrm->SetWindowPos(nullptr, 0, 0,
		WINCX + iRowFrm, WINCY + iColFrm, SWP_NOZORDER);

	GET_INSTANCE(CInputDevice)->Ready_Input_Device(g_hInst, AfxGetMainWnd()->m_hWnd);



	if (FAILED(CDevice::GetInstance()->InitDevice()))
	{
		AfxMessageBox(L"InitDevice Failed");
		return;
	}
	m_pGraphic_Device = GET_INSTANCE(CDevice)->GetDevice();
	m_pGraphic_Device->SetRenderState(D3DRS_LIGHTING, FALSE);
	GET_INSTANCE(CSaveLoad_Manager)->AddBaseTile(CBaseTile::Create(m_pGraphic_Device, 100, 100));
	//GET_INSTANCE(CSaveLoad_Manager)->AddBaseTile(CCube::Create(m_pGraphic_Device, _vec3(0,0,0), _vec3(100,100,100)));
	
	GET_INSTANCE(CSaveLoad_Manager)->AddCamera(CCamera::Create(m_pGraphic_Device));

}

void CToolsView::FormUpdate()
{
	CMainFrame* pMainFrm = dynamic_cast<CMainFrame*>(AfxGetMainWnd());
	CPopupWnd* pPopUpWnd = dynamic_cast<CPopupWnd*>(pMainFrm->m_SecondSplt.GetPane(1, 0));
	CMySheet* pMySheet = dynamic_cast<CMySheet*>(pPopUpWnd->m_pMySheet);
	m_pMapTool = dynamic_cast<CMapTool*>(&pMySheet->m_MapTool);
	
	_tchar		m_szFPS[MAX_PATH] = L"";
	GetCursorPos(&m_mouse);
	ScreenToClient(&m_mouse);
	wsprintf(m_szFPS, L"FPS:%d,%d", m_mouse.x, m_mouse.y);
	
	pMainFrm->SetWindowTextW(m_szFPS);
	if (m_pBaseForm != nullptr)
		m_pBaseForm->GetCameraPos(m_mouse);
	if (m_pMapTool != nullptr)
		m_pMapTool->Update();
}
